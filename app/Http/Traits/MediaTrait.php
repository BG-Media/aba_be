<?php

namespace App\Http\Traits;

use App\Models\Media;
use DB;

trait MediaTrait{

    public function uploadFile($request,$id,$essence,$method)
    {
        if ($request->hasFile('media_id'))
        {
            $destinationPath = 'upload/media/'.date('Y').'/'.date('m').'/'.date('d');

            if ($request->file('media_id')->isValid())
            {
                // move file
                $request->file('media_id')->move($destinationPath,$request->file('media_id')->getClientOriginalName());

                // insert to Media DB
                $media = Media::create([
                    'source' => '/'.$destinationPath.'/'.$request->file('media_id')->getClientOriginalName(),
                    'type' => $request->file('media_id')->getClientOriginalExtension(),
                ]);

                // insert to Essence DB
                if($method == 'store')
                {
                    DB::table($essence.'_medias')->insert([
                        $essence.'_id' => $id,
                        'media_id' => $media->id
                    ]);
                }
                else
                {
                    $count = DB::table($essence.'_medias')->where($essence.'_id',$id)->get();
                    if(count($count)>0)
                    {
                        DB::table($essence.'_medias')
                            ->where($essence.'_id',$id)
                            ->update(['media_id' => $media->id]);
                    }
                    else
                    {
                        DB::table($essence.'_medias')->insert([
                            $essence.'_id' => $id,
                            'media_id' => $media->id
                        ]);
                    }
                }
            }
        }
    }
}