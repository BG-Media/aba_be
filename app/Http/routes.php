<?php


/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'PageController@mainPage');


Route::auth();
Route::get('/home', 'HomeController@dashboard');
Route::get('/about-system', 'HomeController@about');
Route::resource('category', 'CategoryController');
Route::resource('comment', 'CommentController');
Route::resource('media', 'MediaController');
Route::resource('page', 'PageController');
Route::get('post/create-resume','PostController@create_resume');
Route::resource('post', 'PostController');
Route::resource('card','CardController');
Route::resource('store','StoreController');
Route::resource('banner','AdvertisingController');
Route::resource('b2b','B2bController');
Route::resource('subscription','SubscriptionController');
Route::resource('application','ApplicationController');
Route::post('send','CommentController@send');
Route::post('send_email','CommentController@send_email');
Route::get('b2b/show','B2bController@show');
Route::get('b2b/publish/{id}','B2bController@publish');
Route::get('/','AdvertisingController@brand');
Route::get('sales','StoreController@sales');
Route::get('catalog','StoreController@catalog');
Route::get('catalog/{id}','StoreController@show');
Route::get('sales/{id}','StoreController@show');
Route::get('getcard','CardController@getcard');
Route::get('getajax','StoreController@getajax');

Route::post('register', 'Auth\AuthController@create');


Route::group(['middleware' => 'auth'], function()
{
    Route::resource('user', 'UserController');
    Route::resource('role', 'RoleController');
});

Route::group(['middleware' => 'admin'], function()
{
    Route::get('admin', function() {
        return 'You are Admin!';
    });
});


//API content
Route::group(['prefix' => 'mobile'], function () {
    Route::get('category','ApiController@category');
    Route::post('news','ApiController@news');
    Route::get('brand','ApiController@brand');
    Route::get('events','ApiController@events');
    Route::get('sales','ApiController@sales');
    Route::post('send','ApiController@send');
    Route::post('send_email','ApiController@send_email');
    Route::get('catalog/{id?}','ApiController@catalog');    
    Route::get('cats_catalog','ApiController@cats_catalog');

    Route::get('projects','ApiController@projects');
    Route::get('governing','ApiController@governing');
    Route::get('reviews','ApiController@reviews');
    Route::get('jobs','ApiController@jobs');
    Route::get('summary','ApiController@summary');
    Route::get('b2b','ApiController@getb2b');
    Route::post('b2b','ApiController@postb2b');

});