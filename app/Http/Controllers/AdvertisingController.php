<?php

namespace App\Http\Controllers;

use App\Models\Advertising;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Input;
use App\Http\Requests;
use Illuminate\Support\Facades\File;

class AdvertisingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth',['only' =>[
            'index','create','update','destroy','store','edit'
        ]]);
    }

    
    public function index()
    {
        $banner = Advertising::all();
        return view('banner.index',compact('banner'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $banner = Advertising::all();
        return view('banner.create',compact('banner'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'link' => 'required'
        ]);
        $banner = Advertising::create($request->all());
        if(Input::hasFile('image')){
            $image = Input::file('image');
            $urlput = 'upload/media/banner';
            $image->move($urlput, $image->getClientOriginalName());
            $banner->image = '/'. $urlput.'/'.$image->getClientOriginalName();
            $banner->save();
        }
        return redirect('banner');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $banner = Advertising::all();
        return view('layouts.brand',compact('banner'));
    }
    
    public function brand(){
        $banner = Advertising::all();
        return view('welcome',compact('banner'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $banner = DB::table('advertisings')
            ->where('advertisings.id','=',$id)
            ->first();
        return view('banner.edit',compact('banner'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $store = Advertising::FindOrFail($id);
//        $this->validate($request, [
//            'link' => 'required',
//        ]);
        $data = Input::all();
        $store->update($data);

        $image = $request['image'];
        $sources = DB::table('advertisings')
            ->where('advertisings.id','=',$id)
            ->select('image')
            ->get();

        if ($image){
            File::delete($sources);
            $urlput = 'upload/media/banner'.date('Y').'/'.date('m').'/'.date('d');
            if(isset($image)){
                $image->move($urlput,$image->getClientOriginalName());
                $store->image = '/'. $urlput.'/'.$image->getClientOriginalName();
                $store->save();
            }
        }
        return redirect('/banner');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Advertising::find($id)->delete();
        return redirect('banner');
    }
}
