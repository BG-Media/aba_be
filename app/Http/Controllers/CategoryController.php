<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Advertising;
use App\Http\Requests;
use App\Models\Category;
use App\Models\Media;
use App\Http\Traits\MediaTrait;
use Carbon\Carbon;
use DB;
use Illuminate\Support\Facades\Input;

class CategoryController extends Controller
{

    use MediaTrait;

    public function __construct()
    {
        $this->middleware('auth',['only' =>[
            'index','create','update','destroy','store','edit'
        ]]);
    }

    public function index()
    {
        $categories = Category::all();
        return view('category.index',compact('categories'));
    }

    public function create()
    {
        $categories = Category::all();
        return view('category.create',compact('categories'));
    }

    public function store(Request $request)
    {
       
        $this->validate($request, [
            'title' => 'required|max:255',
            'slug' => 'required|unique:categories|max:50',
        ]);

        $category = Category::create($request->all());
        
        $this->uploadFile($request,$category->id,'category','store');

        return redirect('/category');
    }
    
    
    public function show($id,Request $request)
    {
        setlocale(LC_TIME, "ru_RU");
        Carbon::setLocale('ru'); 
        $count_show = $request['count_show'];

        $show = $request['show'];
        $category = DB::table('categories')
            ->where('categories.id','=',$id)
            ->leftJoin('category_medias','category_medias.category_id','=','categories.id')
            ->leftJoin('medias','medias.id','=','category_medias.media_id')
            ->whereNull('categories.deleted_at')
            ->get(['categories.*','medias.source as media_id'])[0];

        $items = DB::table('post_categories')
            ->where('post_categories.category_id',$id)
            ->join('posts','post_categories.post_id','=','posts.id')
            ->leftJoin('post_medias','post_medias.post_id','=','posts.id')
            ->leftJoin('medias','medias.id','=','post_medias.media_id')
            ->select('posts.*','medias.source')
            ->orderBy('created_at','desk')
            ->whereNull('posts.deleted_at')
            ->simplePaginate(9);

        $items1 = DB::table('post_categories')
            ->where('post_categories.category_id',$id)
            ->join('posts','post_categories.post_id','=','posts.id')
            ->leftJoin('post_medias','post_medias.post_id','=','posts.id')
            ->leftJoin('medias','medias.id','=','post_medias.media_id')
            ->select('posts.*','medias.source')
            ->orderBy('created_at','desk')
            ->whereNull('posts.deleted_at')
            ->get();


        $banner = Advertising::all();
        $data= [
            'category'=>$category,
            'items' =>$items,
            'items1' =>$items1,
            'banner' =>$banner,
        ];

        if($category->id == 1)
        {

            return view('category.showme',$data);
        }
        elseif ($category->id == 2)
        {
            return view('category.showbi',$data);
        }
        elseif($category->id == 7){
            return view('category.business',$data);
        }
        elseif($category->id == 8){
            return view('category.vacanci',$data);
        }
        elseif ($category->id == 10){
            return view('category.vacanci',$data);
        }
            return view('category.show',$data);

    }

    public function edit($id)
    {
        $categories = Category::all();

        $category = DB::table('categories')
            ->where('categories.id','=',$id)
            ->leftJoin('category_medias','category_medias.category_id','=','categories.id')
            ->leftJoin('medias','medias.id','=','category_medias.media_id')
            ->get(['categories.*','medias.source as media_id'])[0];

        return view('category.edit',compact('category','categories'));
    }
    
    public function update(Request $request, $id)
    {
        
        $this->validate($request, [
            'title' => 'required|max:255',
            'slug' => 'required|max:50',
        ]);

        $input = Input::except(['_method', '_token','media_id']);
        Category::whereId($id)->update($input);
        $this->uploadFile($request,$id,'category','update');
        return redirect('/category');
    }

    public function destroy($id)
    {

        Category::find($id)->delete();
        return redirect('/category');
    }

    public function restore(){}

    public function treashed(){}

}
