<?php

namespace App\Http\Controllers;

use App\Models\NewsPictures;
use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\Post;
use App\Models\Media;
use App\Models\Category;
use App\Http\Traits\MediaTrait;
use File;
use Illuminate\Support\Facades\Mail;
use DB;
use Illuminate\Support\Facades\Input;

class PostController extends Controller
{

    use MediaTrait;
    
    public function __construct()
    {
        $this->middleware('auth',['only' =>[
            'index','create','update','destroy','store','edit'
        ]]);
    }

    
    public function index()
    {
        $posts = Post::all();
        return view('post.index',compact('posts'));
    }

    public function create()
    {
        $categories = Category::all();
        return view('post.create',compact('categories'));
    }

    public function create_resume()
    {
        $categories = Category::all();
        return view('post.create_resume',compact('categories'));
    }

    public function store(Request $request)
    {
//        dd($request->all());
//        $this->validate($request, [
//            'title' => 'required',
//            'slug' => 'required',
//        ]);
        $post = Post::create($request->all());


        if(isset($request->category_id))
        {
            DB::table('post_categories')->insert([
                'post_id' => $post->id,
                'category_id' => $request->category_id
            ]);
        }

        $images = $request['image'];
        for($i=0; $i<count($images);$i++){
            $urlput = 'upload/media/slider';
            if(isset($images[$i])){
                $images[$i]->move($urlput,$images[$i]->getClientOriginalName());
                DB::table('news_pictures')->insert([
                    'source' => '/'.$urlput.'/'.$images[$i]->getClientOriginalName(),
                    'post_id' => $post->id
                ]);
            }
        }
        $this->uploadFile($request,$post->id,'post','store');

        if($request->category_id ==1){
            $news = DB::table('subscription')->where('value',1)->get();
            $data = $post;
            $count = count($news);
            for($i=0; $i<$count; $i++){
                Mail::send('mail.index', ['data' => $data], function($m) use ($news,$i) {
                    $m->from(env('MAIL_USERNAME'), 'АБА');
                    $m->to($news[$i]->email, 'Nuraga')->subject('Ассоциация Бизнеса Алматы');
                });
            }
        }


        return redirect('/post');
    }

    public function show($id)
    {
        $post = DB::table('posts')
            ->where('posts.id','=',$id)
            ->leftJoin('post_medias','post_medias.post_id','=','posts.id')
            ->leftJoin('medias','medias.id','=','post_medias.media_id')
            ->leftJoin('post_categories','post_categories.post_id','=','posts.id')
            ->leftJoin('categories','categories.id','=','post_categories.category_id')
            ->whereNull('posts.deleted_at')
            ->orderBy('id','desk')
            ->get(['posts.*','medias.source as media_id','categories.id as category_id'])[0];
        $pictures = DB::table('news_pictures')
            ->where('post_id',$id)
            ->select('source')
            ->get();
        $data =[
            'post'=>$post,
            'pictures'=>$pictures,
        ];
        if ($post->category_id == 1)
        {
            return view('post.showme',$data);
        }
        elseif ($post->category_id == 2){
            return view ('post.showbi',$data);
        }
        elseif($post->category_id == 7){
            return view('post.business',compact('post'));
        }
        return view('post.show',compact('post'));
    }

    public function edit($id)
    {
        $post = DB::table('posts')
            ->where('posts.id','=',$id)
            ->leftJoin('post_medias','post_medias.post_id','=','posts.id')
            ->leftJoin('medias','medias.id','=','post_medias.media_id')
            ->leftJoin('post_categories','post_categories.post_id','=','posts.id')
            ->leftJoin('categories','categories.id','=','post_categories.category_id')
            ->get(['posts.*','medias.source as media_id','categories.id as category_id'])[0];

        $pictures = DB::table('news_pictures')
            ->where('post_id',$id)
            ->get();

        $categories = Category::all();

        return view('post.edit',compact('post','categories','pictures'));
    }

    public function update(Request $request, $id)
    {

//        $this->validate($request, [
//            'title' => 'required',
//            'slug' => 'required',
//        ]);



        $input = Input::except(['_method', '_token','media_id','category_id']);
        $task = Post::findOrFail($id);
        $task->fill($input)->update();
        if(isset($request->category_id))
        {
            $post = DB::table('post_categories')->where('post_id',$id);

            if($post->count()>0)
            {
                DB::table('post_categories')->where('post_id',$id)->update
                    ([
                        'category_id' => $request->category_id
                    ]);
            }
            else
            {
                DB::table('post_categories')->insert
                    ([
                        'post_id' => $id,
                        'category_id' => $request->category_id
                    ]);
            }
        }

      //  dd($post->id);

        $images = $request['image'];
        $post = $post->first();

        $pictures = NewsPictures::where('post_id',$id)->get();
        for($i=0; $i<count($images);$i++){
            $urlput = 'upload/media/slider';
            if(isset($images[$i])){
                $images[$i]->move($urlput,$images[$i]->getClientOriginalName());
                $pictures[$i]->source = '/'.$urlput.'/'.$images[$i]->getClientOriginalName();
                $pictures[$i]->save();
            }
        }

        $this->uploadFile($request,$id,'post','update');

        return redirect('/post');
    }
    
    public function destroy($id)
    {

        Post::find($id)->delete();
        return redirect('/post');
    }

    public function restore(){}

    public function treashed()
    {
        $posts = Post::onlyTrashed()
            ->get();
    }

    public function media($media,$post_id,$method = 'store')
    {
        if ($media->hasFile('media_id'))
        {
            $destinationPath = 'upload/media/'.date('Y').'/'.date('m').'/'.date('d');

            if ($media->file('media_id')->isValid())
            {
                $media->file('media_id')->move($destinationPath,$media->file('media_id')->getClientOriginalName());
                $media = Media::create([
                    'source' => '/'.$destinationPath.'/'.$media->file('media_id')->getClientOriginalName(),
                    'type' => $media->file('media_id')->getClientOriginalExtension(),
                ]);

                if($method == 'store')
                {
                    DB::table('post_medias')->insert([
                        'post_id' => $post_id,
                        'media_id' => $media->id
                    ]);
                }
                else
                {
                    DB::table('post_medias')
                        ->where('post_id',$post_id)
                        ->update(['media_id' => $media->id]);
                }

            }
        }
    }
}
