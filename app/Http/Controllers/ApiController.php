<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Post;
use App\Models\Media;
use App\Models\Category;
use App\Http\Traits\MediaTrait;
use App\Models\Advertising;
use File;
use App\Models\B2b;
use DB;
use Illuminate\Support\Facades\Input;
use App\Http\Requests;


class ApiController extends Controller
{
    public function category(){
        $category = Category::all();
        return response()->json($category);
    }

    public function news(){

        $news = DB::table('posts')
            ->where('categories.id','=',1)
            ->leftJoin('post_medias','post_medias.post_id','=','posts.id')
            ->leftJoin('medias','medias.id','=','post_medias.media_id')
            ->leftJoin('post_categories','post_categories.post_id','=','posts.id')
            ->leftJoin('categories','categories.id','=','post_categories.category_id')
            ->whereNull('posts.deleted_at')
            ->select(['posts.*','medias.source as media_id','categories.id as category_id'])
            ->orderBy('created_at', 'desc')
            ->paginate(20);

        $list = array();
        foreach ($news as $item)
        {
            $item->content = strip_tags($item->content);
            $item->images = DB::table('news_pictures')
                ->where('post_id','=',$item->id)
                ->select('source','post_id')
                ->get();
            $list[] = $item;
        }
            $data = [
                'post'=>$list,
            ];
        return response()->json($data);
    }

    public function brand(){
        $brand = Advertising::all();
        $data = [
            'brand' => $brand
        ];
        return response()->json($data);
    }

    public function  events(){
        $events = DB::table('posts')
            ->where('categories.id','=',2)
            ->leftJoin('post_medias','post_medias.post_id','=','posts.id')
            ->leftJoin('medias','medias.id','=','post_medias.media_id')
            ->leftJoin('post_categories','post_categories.post_id','=','posts.id')
            ->leftJoin('categories','categories.id','=','post_categories.category_id')
            ->whereNull('posts.deleted_at')
            ->get(['posts.*','medias.source as media_id','categories.id as category_id']);
        $list = [];
        foreach ($events as $event)
        {
            //$event->content = strip_tags(str_replace("<br>", "\n", $event->content));
            $list[] = $event;
        }
        $data = [
            'events'=>$list,
        ];

        return response()->json($data);
    }

//    public function send_email(Request $request){
////        dd($request);
////        $this ->validate($request,[
////            'email-p'=>'required',
////        ]);
//        $data = $request->all();
//
//        Mail::send('mail.index', ['data' => $data], function($m) {
//            $m->from(env('MAIL_USERNAME'), 'Ассоциация Бизнеса Алматы');
//            $m->to('nursultankalibayev@gmail.com', 'Nuraga')->subject('Заявка на рассылку');
//        });
////        return redirect('/' . '#success');
//    }

    public function send_email(Request $request){
        $this ->validate($request,[
            'fio'=>'required|min:3|max:50',
            'phone'=>'required',
            'email-v' =>'required',
            'company' =>'required',
        ]);
        $data = $request->all();
        $mail = \Mail::send('mail.index-1', ['data' => $data], function($m) {
            $m->from(env('MAIL_USERNAME'), 'Ассоциация Бизнеса Алматы');
            $m->to('nursultankalibayev@gmail.com', 'Nuraga')->subject('Вступление в Ассоциацию');
        });
        return response()->json($mail);
    }

    public function sales(){
        $sales = DB::table('stores')
            ->select('*')
            ->get();
        $data =['store' => $sales];
        return response()->json($data);
    }

    public function catalog($id = 0){
        if ($id)
        {
            
            $catalog = DB::table('stores')
                ->where('value_cat', $id)
                ->get();
        }
        else 
        {
            $catalog = DB::table('stores')
                ->select('*')
                ->get();
            
        }

        $data = ['catalog'=> $catalog];

        return response()->json($data);
    }
    
    public function cats_catalog() 
    {
        $list = [ 
            ['id' => 0, 'name' => "Все члены ассоциации"],
            ['id' => 1, 'name' => "Авто, Спецтехника, автозапчасти"],
            ['id' => 2, 'name' => "Издательства, полиграфия, типографии"],
            ['id' => 3, 'name' => "Медицина"],
            ['id' => 4, 'name' => "Недвижимость  "],
            ['id' => 5, 'name' => "Оборудование"],
            ['id' => 6, 'name' => "Образование"],
            ['id' => 7, 'name' => "Строительство и проектирование"],
            ['id' => 8, 'name' => "Строительные материалы: производство, продажа"],
            ['id' => 9, 'name' => "Электротовары, бытовая техника"],
            ['id' => 10, 'name' => "Мебель, интерьер, фурнитура"],
            ['id' => 11, 'name' => "IT услуги"],
            ['id' => 12, 'name' => "Бухгалтерские и Юридические услуги"],
            ['id' => 13, 'name' => "Одежда, обувь, галантерея"],
            ['id' => 14, 'name' => "Рестораны, кафе, гостиницы, кейтеринг"],
            ['id' => 15, 'name' => "Транспорт и перевозки"],
            ['id' => 16, 'name' => "Телекоммуникации и связь"],
            ['id' => 17, 'name' => "Продукты питания"],
            ['id' => 18, 'name' => "Ювелирные изделия"],
            ['id' => 19, 'name' => "Прочее услуги"],
        ];
        $data = ['categories'=> $list];

        return response()->json($data);
    }

    public function projects(){
        $projects = DB::table('posts')
            ->where('categories.id','=',10)
            ->leftJoin('post_medias','post_medias.post_id','=','posts.id')
            ->leftJoin('medias','medias.id','=','post_medias.media_id')
            ->leftJoin('post_categories','post_categories.post_id','=','posts.id')
            ->leftJoin('categories','categories.id','=','post_categories.category_id')
            ->whereNull('posts.deleted_at')
            ->get(['posts.*','medias.source as media_id','categories.id as category_id']);
        $data = ['projects',$projects];
        return response()->json($data);
    }

    public function governing(){

        $governing = DB::table('posts')
            ->where('categories.id','=',5)
            ->leftJoin('post_medias','post_medias.post_id','=','posts.id')
            ->leftJoin('medias','medias.id','=','post_medias.media_id')
            ->leftJoin('post_categories','post_categories.post_id','=','posts.id')
            ->leftJoin('categories','categories.id','=','post_categories.category_id')
            ->whereNull('posts.deleted_at')
            ->get(['posts.*','medias.source as media_id','categories.id as category_id']);
        $data = ['projects',$governing];
        return response()->json($data);
    }
    public function reviews(){

        $reviews = DB::table('posts')
            ->where('categories.id','=',6)
            ->leftJoin('post_medias','post_medias.post_id','=','posts.id')
            ->leftJoin('medias','medias.id','=','post_medias.media_id')
            ->leftJoin('post_categories','post_categories.post_id','=','posts.id')
            ->leftJoin('categories','categories.id','=','post_categories.category_id')
            ->whereNull('posts.deleted_at')
            ->get(['posts.*','medias.source as media_id','categories.id as category_id']);
        $data = ['reviews',$reviews];
        return response()->json($data);
    }

    public function jobs(){

        $jobs = DB::table('posts')
            ->where('categories.id','=',8)
            ->leftJoin('post_medias','post_medias.post_id','=','posts.id')
            ->leftJoin('medias','medias.id','=','post_medias.media_id')
            ->leftJoin('post_categories','post_categories.post_id','=','posts.id')
            ->leftJoin('categories','categories.id','=','post_categories.category_id')
            ->whereNull('posts.deleted_at')
            ->get(['posts.*','medias.source as media_id','categories.id as category_id']);
        $data = ['jobs',$jobs];
        return response()->json($data);
    }

    public function summary(){

        $summary = DB::table('posts')
            ->where('categories.id','=',9)
            ->leftJoin('post_medias','post_medias.post_id','=','posts.id')
            ->leftJoin('medias','medias.id','=','post_medias.media_id')
            ->leftJoin('post_categories','post_categories.post_id','=','posts.id')
            ->leftJoin('categories','categories.id','=','post_categories.category_id')
            ->whereNull('posts.deleted_at')
            ->get(['posts.*','medias.source as media_id','categories.id as category_id']);
        $data = ['summary',$summary];
        return response()->json($data);
    }

    public function getb2b (){
        $content = B2b::all();
        $data = ['content' => $content];

        return response()->json($data);
    }

    public function postb2b(Request $request){
        $this->validate($request, [
            'title' => 'required',
            'description' => 'required',
            'email'=>'required'

        ]);
        if($request['title']) {
            if($request['description'])
            $content = B2b::create($request->all());
            if (Input::hasFile('image_1')) {
                $image = Input::file('image_1');
                $urlput = 'upload/media/b2b';
                $image->move($urlput, $image->getClientOriginalName());
                $content->image_1 = '/' . $urlput . '/' . $image->getClientOriginalName();
                $content->save();
            } else {
                $content->image_1 = '/upload/media/2016/09/26/25285969062_986422ad49_k.jpg';
                $content->save();
            }
            if (Input::hasFile('file')) {
                $image = Input::file('file');
                $urlput = 'upload/media/b2b';
                $image->move($urlput, $image->getClientOriginalName());
                $content->file = '/' . $urlput . '/' . $image->getClientOriginalName();
                $content->save();
            }
            return response()->json('success');
        } else{
            return response()->json('false');
        }
    }
}
