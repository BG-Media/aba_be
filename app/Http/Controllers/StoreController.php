<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Store;
use App\Http\Requests;
use DB;
use App\Models\Advertising;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;

class StoreController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth',['only' =>[
            'index','create','update','destroy','store','edit'
        ]]);
    }

    
    public function index()
    {
        $store = Store::all();
        return view('store.index',compact('store'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('store.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
//        dd($request->all());
//        $this->validate($request,[
//            'title' => 'required',
//            'value' => 'required',
//            'view' =>'required',
//            'images' => 'required',
//        ]);
        $store = Store::create($request->all());
        $images = $request['images'];
            $urlput = 'upload/media/sales'.date('Y').'/'.date('m').'/'.date('d');
            if(isset($images)){
                $images->move($urlput,$images->getClientOriginalName());
                $store->images =  '/'.$urlput.'/'.$images->getClientOriginalName();
                $store->save();
            }
        return redirect('/store');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $store = Store::findOrFail($id);
        return view('store.show',compact('store'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $store = DB::table('stores')
            ->where('stores.id','=',$id)
            ->first();
        
        return view('store.edit',compact('store'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

        $store = Store::FindOrFail($id);

//        $this->validate($request,[
//            'title' => 'required',
//            'value' => 'required',
//            'view' =>'required',
//            'images' => 'required',
//        ]);

        $data = Input::all();
        $store->update($data);

        $sources = DB::table('stores')
                    ->where('stores.id','=',$id)
                    ->select('images')
                    ->first();
        if ($request->images){
            File::delete($sources);
            $images = $request['images'];
            $urlput = 'upload/media/sales'.date('Y').'/'.date('m').'/'.date('d');
            if(isset($images)){
                $images->move($urlput,$images->getClientOriginalName());
                $store->images =  '/'.$urlput.'/'.$images->getClientOriginalName();
                $store->save();
            }
        }
        return redirect('/store');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Store::find($id)->delete();
        return redirect('/store');
    }

    public function sales(){
        $banner = Advertising::all();
        $store = DB::table('stores')
            ->where('role','=',1)
            ->get();
        return view('store.sales',compact('store','banner'));
    }
    public function catalog(){
        $banner = Advertising::all();
        $store = DB::table('stores')
            ->where('role','=',2)
            ->get();
        return view('store.catalog',compact('store','banner'));

    }
}
