<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use DB;
use App\Models\Category;
use App\Models\B2b;
use Illuminate\Support\Facades\Input;
use Symfony\Component\HttpFoundation\Response;

class B2bController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth', ['only' => [
            'index', 'update', 'destroy', 'edit'
        ]]);
    }

    public function index()
    {
        $content = B2b::all();
        return view('b2b.index', compact('content'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('b2b.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'description' => 'required',
        ]);

        $content = B2b::create($request->all());
        if (Input::hasFile('image_1')) {
            $image = Input::file('image_1');
            $urlput = 'upload/media/b2b';
            $image->move($urlput, $image->getClientOriginalName());
            $content->image_1 = '/' . $urlput . '/' . $image->getClientOriginalName();
            $content->save();
        }else{
            $content->image_1 = '/upload/media/2016/09/26/25285969062_986422ad49_k.jpg';
            $content->save();
        }
        if (Input::hasFile('file')) {
            $image = Input::file('file');
            $urlput = 'upload/media/b2b';
            $image->move($urlput, $image->getClientOriginalName());
            $content->file = '/' . $urlput . '/' . $image->getClientOriginalName();
            $content->save();
        }
        return redirect('b2b/show');

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $content = B2b::all();
        return view('b2b.show', compact('content'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        B2b::find($id)->delete();
        return redirect('b2b');
    }

    public function publish(Request $request, $id)
    {
        $publish = DB::table('btwob')->where('id', '=', $id)->first();
        if ($publish->is_admin == 0) {
            DB::table('btwob')->where('id', '=', $id)->update
            ([
                'is_admin' => 1
            ]);
        } else if ($publish->is_admin == 1) {
            DB::table('btwob')->where('id', '=', $id)->update
            ([
                'is_admin' => 0
            ]);
        }

        return redirect('/b2b');
    }
}
     