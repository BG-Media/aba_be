<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\Page;
use App\Models\Media;
use App\Http\Traits\MediaTrait;
use App\Helpers\Helper;
use App\Models\Advertising;
use DB;
use Illuminate\Support\Facades\Input;

class PageController extends Controller
{

    use MediaTrait;

    public function __construct()
    {
        $this->middleware('auth',['only' =>[
            'index','create','update','destroy','store','edit'
        ]]);
    }

    
    public function index()
    {
        $pages = Page::all();
        return view('page.index',compact('pages'));
    }

    public function create()
    {
        return view('page.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required|max:255',
            'slug' => 'required|unique:pages|max:50',
        ]);

        $page = Page::create($request->all());
        
        $this->uploadFile($request,$page->id,'page','store');

        return redirect('/page');
    }

    public function show($id)
    {
        $page = DB::table('pages')
            ->where('pages.id','=',$id)
            ->leftJoin('page_medias','page_medias.page_id','=','pages.id')
            ->leftJoin('medias','medias.id','=','page_medias.media_id')
            ->get(['pages.*','medias.source as media_id'])[0];

        $banner = Advertising::all();

        return view('page.show',compact('page','banner'));
    }

    public function edit($id)
    {
        $page = DB::table('pages')
            ->where('pages.id','=',$id)
            ->leftJoin('page_medias','page_medias.page_id','=','pages.id')
            ->leftJoin('medias','medias.id','=','page_medias.media_id')
            ->get(['pages.*','medias.source as media_id'])[0];

        return view('page.edit',compact('page'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'required|max:255',
            'slug' => 'required|max:50',
        ]);

        $input = Input::except(['_method', '_token','media_id']);
        
        Page::whereId($id)->update($input);

        $this->uploadFile($request,$id,'page','update');
        
        return redirect('/page');
    }

    public function destroy($id)
    {

        Page::find($id)->delete();
        return redirect('/page');
    }

    public function restore(){}

    public function treashed(){}

    public function media($media,$page_id,$method = 'store')
    {
        if ($media->hasFile('media_id'))
        {
            $destinationPath = 'upload/media/'.date('Y').'/'.date('m').'/'.date('d');

            if ($media->file('media_id')->isValid())
            {
                $media->file('media_id')->move($destinationPath,$media->file('media_id')->getClientOriginalName());
                $media = Media::create([
                    'source' => '/'.$destinationPath.'/'.$media->file('media_id')->getClientOriginalName(),
                    'type' => $media->file('media_id')->getClientOriginalExtension(),
                ]);

                if($method == 'store')
                {
                    DB::table('page_medias')->insert([
                        'page_id' => $page_id,
                        'media_id' => $media->id
                    ]);
                }
                else
                {
                    DB::table('page_medias')
                        ->where('page_id',$page_id)
                        ->update(['media_id' => $media->id]);
                }

            }
        }
    }

    public function mainPage()
    {
        return view('welcome');
    }
}

