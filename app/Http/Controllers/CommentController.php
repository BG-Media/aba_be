<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Mail;
use App\Models\comment;
use App\Models\Media;

use DB;
use Illuminate\Support\Facades\Input;

class CommentController extends Controller
{
    public function __construct()
    {
//        $this->middleware('auth',['only' =>[
//            'index','create','update','destroy','store','edit'
//        ]]);
    }


    public function index()
    {
        $comments = Comment::all();
        return view('comment.index',compact('comments'));
    }

    public function create()
    {
        return view('comment.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'content' => 'required',
            'post_id' => 'required',
            'user_id' => 'required',
        ]);

        $comment = Comment::create($request->all());

        DB::table('post_comments')->insert([
            'post_id' => $request->post_id,
            'comment_id' => $comment->id
        ]);

        return redirect('/comment');
    }

    public function show($id)
    {
        $comment = DB::table('comments')
            ->where('comments.id','=',$id)
            ->leftJoin('comment_medias','comment_medias.comment_id','=','comments.id')
            ->leftJoin('medias','medias.id','=','comment_medias.media_id')
            ->get(['comments.*','medias.source as media_id'])[0];

        return view('comment.show',compact('comment'));
    }

    public function edit($id)
    {
        $comment = DB::table('comments')
            ->where('comments.id','=',$id)
            ->leftJoin('comment_medias','comment_medias.comment_id','=','comments.id')
            ->leftJoin('medias','medias.id','=','comment_medias.media_id')
            ->get(['comments.*','medias.source as media_id'])[0];

        return view('comment.edit',compact('comment'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'required|max:255',
            'slug' => 'required|max:50',
        ]);


        $input = Input::except(['_method', '_token','media_id']);
        Comment::whereId($id)->update($input);
        $this->media($request,$id,'update');
        return redirect('/comment');
    }

    public function destroy($id)
    {

        Comment::find($id)->delete();
        return redirect('/comment');
    }

    public function restore(){}

    public function treashed(){}

    public function send_email(Request $request){
//        dd($request);
        $this ->validate($request,[
            'email-p'=>'required',
        ]);
        $data = $request->all();

        Mail::send('mail.index', ['data' => $data], function($m) {
            $m->from(env('MAIL_USERNAME'), 'Ассоциация Бизнеса Алматы');
            $m->to('nursultankalibayev@gmail.com', 'Nuraga')->subject('Заявка на рассылку');
        });
        return redirect('/' . '#success');
    }

    public function send(Request $request){
//        dd($request);
        $this ->validate($request,[
            'fio'=>'required|min:3|max:50',
            'phone'=>'required',
            'email-v' =>'required',
            'company' =>'required',
        ]);
        $data = $request->all();

        Mail::send('mail.index-1', ['data' => $data], function($m) {
            $m->from(env('MAIL_USERNAME'), 'Ассоциация Бизнеса Алматы');
            $m->to('nursultankalibayev@gmail.com', 'Nuraga')->subject('Вступление в Ассоциацию');
        });
        return redirect('/' . '#success');
    }

}
