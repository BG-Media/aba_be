<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use DB;
use App\Models\Card;
use App\Models\Category;
use App\Models\Advertising;
use Illuminate\Support\Facades\Input;
use Symfony\Component\HttpFoundation\Response;

class CardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth',['only' =>[
            'index','create','update','destroy','store','edit'
        ]]);
    }

    public function index()
    {
        $cards = Card::all();
        return view('card.index',compact('cards'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('card.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {
        $this->validate($request, [
            'number' => 'required|max:50',
            'fio' => 'required',
            'company' =>'required',
            'iin'=>'required|max:20'
        ]);

        $card = Card::create($request->all());
        return redirect('/card');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $banner = Advertising::all();
        return view('page.card',compact('banner'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $card = DB::table('cards')
            ->where('cards.id','=',$id)
            ->first();
        return view('card.edit',compact('card'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $dbcard =Card::findOrFail($id);
        $this->validate($request, [
            'number' => 'required|max:50',
            'fio' => 'required',
            'company' =>'required',
            'iin'=>'required|max:20'
        ]);
        $data = Input::all();
        $dbcard->update($data);


//        $input = Input::except([ '_method','_token','number','fio','iin','company']);
//        Card::whereId($id)->update($input);
        return redirect('/card');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Card::find($id)->delete();
        return redirect('card');
    }
    public function getcard(Request $request){
        if ($request->number){
            if($request->ajax())
            {
                $output ="";
                $customers_number = DB::table('cards') ->where('number','LIKE','%'.$request->number.'%')
                    ->get();
                if($customers_number )
                {
                    foreach ($customers_number  as $item) {
                        $output .=  '<tr>'.
                            '<td>'.$item->number.'</td>'.
                            '<td>'.$item->fio.'</td>'.
                            '<td>'.$item->company.'</td>'.
//                            '<td>'.$item->iin.'</td>'.
                            '</tr>';
                    }
                    return Response($output);
                }
            }
        }elseif ($request->fio)
        {
            if($request->ajax())
            {
                $output ="";
                $customers_fio = DB::table('cards') ->where('fio','LIKE','%'.$request->fio.'%')
                    ->get();
                if($customers_fio)
                {
                    foreach ($customers_fio  as $item) {
                        $output .=  '<tr>'.
                            '<td>'.$item->number.'</td>'.
                            '<td>'.$item->fio.'</td>'.
                            '<td>'.$item->company.'</td>'.
//                            '<td>'.$item->iin.'</td>'.
                            '</tr>';
                    }
                    return Response($output);
                }
            }
        }
        elseif($request->company)
        {
            if($request->ajax())
            {
                $output ="";
                $customers_company = DB::table('cards') ->where('company','LIKE','%'.$request->company.'%')
                    ->get();
                if($customers_company)
                {
                    foreach ($customers_company  as $item) {
                        $output .=  '<tr>'.
                            '<td>'.$item->number.'</td>'.
                            '<td>'.$item->fio.'</td>'.
                            '<td>'.$item->company.'</td>'.
//                            '<td>'.$item->iin.'</td>'.
                            '</tr>';
                    }
                    return Response($output);
                }
            }
        }
        else
        {
            if($request->ajax())
            {
                $output ="";
                $customers_iin = DB::table('cards') ->where('iin','LIKE','%'.$request->iin.'%')
                    ->get();
                if($customers_iin)
                {
                    foreach ($customers_iin  as $item) {
                        $output .=  '<tr>'.
                            '<td>'.$item->number.'</td>'.
                            '<td>'.$item->fio.'</td>'.
                            '<td>'.$item->company.'</td>'.
//                            '<td>'.$item->iin.'</td>'.
                            '</tr>';
                    }
                    return Response($output);
                }
            }
        }
    }
}
