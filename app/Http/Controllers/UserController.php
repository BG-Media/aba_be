<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\Role;

use Auth;

use App\Models\User;

use Illuminate\Support\Facades\Input;

use DB;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth',['only' =>[
            'index','create','update','destroy','store','edit'
        ]]);
    }

    
    public function index()
    {
        $users = User::all();
        $i=0;
        foreach($users as $user) {
            $role_id = DB::table('user_roles')->where('user_id','=',$user->id)->first();
            $role = Role::where('id','=',$role_id->role_id)->first();
            $roles[$i] = $role->role;
            $i++;
        }

        $role_id = DB::table('user_roles')->where('user_id','=',Auth::user()->id)->first();
        $role = Role::where('id','=',$role_id->role_id)->first();

        $all_roles = Role::orderBy('id')->get();

        $data = [
            'role' => $role,
            'users' => $users,
            'roles' => $roles,
            'all_roles' => $all_roles
        ];

        return view('user.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('user.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
        ]);

        $user = User::create([
            'name' => $request['name'],
            'email' => $request['email'],
            'password' => bcrypt($request['password']),
        ]);

        if($user) {
            DB::table('user_roles')->insert([
                'user_id' => $user->id,
                'role_id' => 1
            ]);
        }

        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::whereId($id)->first();

        $roles = Role::all();

        $role =DB::table('user_roles')->where('user_id','=',$user->id)->first();

        $data = [
                'user' => $user,
                'roles' => $roles,
                'role_id' => $role->role_id
        ];

        return view('user.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
                'name' => 'required',
                'password' => 'min:6|confirmed'
        ]);

        $input = Input::except(['_method', '_token','password', 'password_confirmation','email','role']);
        $user = User::whereId($id)->firstOrFail();
        $user->update($input);

        $role = $request->input('role');
        $role_id = Role::where('role','=',$role)->first()->id;
        $user_role = DB::table('user_roles')->where('user_id','=',$id);
        $user_role->update(['role_id' => $role_id]);

        if($request->input('password') != '') {
            $password = bcrypt($request->input('password'));
            $user->update(['password' => $password]);
            //return redirect('/logout');
        }
        $user->save();

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::whereId($id)->delete();
        DB::table('user_roles')->where('user_id','=',$id)->delete();
        return back();
    }
}
