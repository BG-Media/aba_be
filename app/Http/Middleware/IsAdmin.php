<?php

namespace App\Http\Middleware;

use Closure;

use Auth;

use DB;

class IsAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!Auth::guest()) {
            $role = DB::table('user_roles')->where('user_id','=',Auth::user()->id)->first();
            if($role->role_id == 2) {
                return $next($request);
            }
        }

        return redirect('/');
    }
}