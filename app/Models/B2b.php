<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class B2b extends Model
{
    //
    protected $table ='btwob';

    protected $fillable =[
        'title',
        'description',
        'image_1',
        'image_2',
        'file',
        'type',
        'author',
        'phone',
        'email',
        'is_admin',
        'title_content'
    ];
}
