<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NewsPictures extends Model
{
    //
    protected $table = 'news_pictures';

    protected $fillable = [
        'post_id','source',
    ];
}
