<?php
class Helper
{

    public static function getCategoryItems($id)
    {

        $items = DB::table('post_categories')
            ->where('post_categories.category_id', $id)
            ->join('posts', 'post_categories.post_id', '=', 'posts.id')
            ->leftJoin('post_medias', 'post_medias.post_id', '=', 'posts.id')
            ->leftJoin('medias', 'medias.id', '=', 'post_medias.media_id')
            ->whereNull('posts.deleted_at')
            ->get(['posts.*', 'medias.source']);
        return $items;
    }

    public static function getCategoryPagination($id, $count)
    {

        $items = DB::table('post_categories')
            ->where('post_categories.category_id', $id)
            ->join('posts', 'post_categories.post_id', '=', 'posts.id')
            ->leftJoin('post_medias', 'post_medias.post_id', '=', 'posts.id')
            ->leftJoin('medias', 'medias.id', '=', 'post_medias.media_id')
            ->select('posts.*', 'medias.source')
            ->whereNull('posts.deleted_at')
            ->orderBy('created_at', "desk")
            ->paginate($count);


        return $items;
    }

    public static function getCategoryNews($id, $count)
    {
        $items = DB::table('post_categories')
            ->where('post_categories.category_id',$id)
            ->where('posts.created_at','<',(date('Y-m-d H:i:s')))
//            ->orWhere('posts','posts.created_at','>',date('Y-m-d H:i:s'))
            ->join('posts','post_categories.post_id','=','posts.id')
            ->leftJoin('post_medias','post_medias.post_id','=','posts.id')
            ->leftJoin('medias','medias.id','=','post_medias.media_id')
            ->select('posts.*','medias.source')
            ->orderBy('created_at','desk')
            ->whereNull('posts.deleted_at')
            ->paginate($count);
        return $items;
    }
    public static function getCategoryNews1($id, $count)
    {

        $items1 = DB::table('post_categories')
            ->where('post_categories.category_id', $id)
//            ->orWhere('posts','posts.created_at','<',date('Y-m-d H:i:s'))
            ->where('posts.created_at','>',(date('Y-m-d H:i:s')))
            ->join('posts', 'post_categories.post_id', '=', 'posts.id')
            ->leftJoin('post_medias', 'post_medias.post_id', '=', 'posts.id')
            ->leftJoin('medias', 'medias.id', '=', 'post_medias.media_id')
            ->select('posts.*', 'medias.source')
            ->orderBy('created_at', 'desk')
            ->whereNull('posts.deleted_at')
            ->paginate($count);
        return $items1;
    }

    public static function getSlugName($id)
    {
        // $slugs = DB::table('slugs')->get();
        $slugs = [
            1 => 'Юриспруденция',
            2 => 'Топ-менеджмент',
            3 => 'Бухгалтерия',
            4 => 'Страхование',
            5 => 'IT, телеком',
            6 => 'Продажи и менеджмент',
            7 => 'Строительство',
        ];
    }
}