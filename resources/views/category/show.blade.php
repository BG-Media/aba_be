@extends('layouts.app')
@section('content')
    <div class="col-md-11 col-sm-11">
        <h1 class="list_title">{{$category->title}}</h1>

        <ol class="breadcrumb">
            <li><a href="{{url('/')}}">Главная</a></li>
            <li><a href="#">@if($category->id == 8 ||$category->id == 9) HR @else Об ассоциации @endif</a></li>
            <li class="active">{{$category->title}}</li>
        </ol>
    </div>

    <div class="col-md-12">
        @if($category->id == 3)
                <div class="row content_project">
                    @foreach(Helper::getCategoryItems($category->id) as $item)
                        <div class="col-md-3 col-sm-4 col-xs-6 list_project">
                            <p><a href="{{ url('post',$item->id) }}">{{ $item->title }}</a></p>
                        </div>
                    @endforeach
                </div>
        @elseif( $category->id == 7)
            @section('title')
                Поддержка бизнеса
            @endsection
            <div class="row content_support catalog">
                @foreach(Helper::getCategoryItems($category->id) as $item)
                <div class="col-md-3 col-sm-4 col-xs-6">
                    <div class="row min_block_support">
                        <a href="/"><img src="{{ $item->source }}" class="img-responsive support_img" alt=""></a>

                        <div class="text_support">
                            <p><a href="{{ url('post',$item->id) }}">{{ $item->title }}</a></p>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
            @include('layouts.brand')
        @elseif($category->id == 4)
            <div class="col-md-12 col-sm-12 col-xs-12">
                <h3 class="title_tab">Каталог членов</h3>
                <select name="cat_name" id="catalog" class="select_list">
                    <option value="cat_1">Все члены ассоциации копия</option>
                    <option value="cat_2">Авто, спецтехника</option>
                    <option value="cat_3">Полиграфия</option>
                    <option value="cat_4">Медицина</option>
                    <option value="cat_5">Недвижимость</option>
                    <option value="cat_6">Оборудование</option>
                    <option value="cat_7">Образование</option>
                </select>
                <div class="row">
                    <div class="col-md-3 col-sm-4 col-xs-6 blocks_mambers">
                        <a href="">
                            <div class="img_suuport">
                                <img src="images/slid1.png" class="img-responsive" alt="">
                            </div>
                        </a>
                        <div class="link_support">
                            <p><a href="">ТОО "ALKAZIA" (GENESIS JEWELLERY) </a></p>
                        </div>
                    </div>
                </div>
            </div>
        @elseif($category->id == 5)
            @section('title')Правление@endsection
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="content_support clearfix">
                    @foreach(Helper::getCategoryItems($category->id) as $key=>$item)
                        @if($key==0)
                            <div class="row">
                                <div class="col-md-offset-4 col-md-4 col-sm-offset-4 col-sm-4 col-xs-offset-3 col-xs-6 blocks_directions">
                                   <div class="img_direction">
                                            <img src="{{ $item->source }}" class="img-responsive center-block" alt="">
                                        </div>
                                        <span class="info_direction">
                                            <b>{{ $item->title }}</b> <br>{{ $item->excerpt }}
                                        </span>
                                </div>
                            </div>
                        @else
                            @if($key==1) <div class="row"> @endif
                                <div class="col-md-4 col-sm-3 col-xs-6 blocks_directions">
                                        <div class="img_direction">
                                            <img src="{{ $item->source }}" class="img-responsive center-block" alt="">
                                        </div>
                                        <span class="info_direction">
                                            <b>{{ $item->title }}</b> <br>{{ $item->excerpt }}
                                        </span>
                                </div>
                                @if(count(Helper::getCategoryItems($category->id)) == $key) </div> @endif
                        @endif
                    @endforeach
                </div>
            </div>
        @elseif($category->id == 6)
            @section('title')
                Отзывы
            @endsection
            <div class="col-md-12">
                <div class="content_otzyvy clearfix">
                    @foreach(Helper::getCategoryItems($category->id) as $key=>$item)
                        <div class="col-md-4 col-sm-4 col-xs-6">
                            <div class="img_block">
								<a class="grouped_elements" rel="group1" href="{{$item->source }}">

									<img src="{{$item->source }}" class="img-responsive" alt="">
								</a>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
            </div>
        @elseif($category->id == 8)
            <div class="vacansy clearfix">
                <div class="box">
                    <div class="top">
                        <p>Требуется дизайнер</p>
                    </div>
                    <div class="bottom">
                        <p>Требование:</p>
                        <ul>
                            <li>профессиональное владение программами: Corel Draw. Adobe  Illustrator. Adobe Photoshop.</li>
                            <li>опыт работы  в рекламных агентствах по производству наружной рекламы приветствуется</li>
                            <li>разработка новых креативных идей и способность их воплощать в ограниченные сроки.</li>
                            <li>наличие личного портфолио</li>
                            <li>умение оперативно переключаться между проектами в условиях многозадачности</li>
                            <li>умение работать в команде</li>
                            <li>НЕИССЯКАЕМЫЙ ПОТОК КРЕАТИВНЫХ ИДЕЙ</li>
                        </ul>
                        <p>Контакты:</p>
                        <ul>
                            <li>8 707 742 02 07</li>
                            <li>328 37 26</li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="vacancy">
                <div class="col-md-12 content_news goals_objectives ">
                    @foreach(Helper::getCategoryItems($category->id) as $key=>$item)
                    <p><a href="{{ url('post',$item->id) }}">{{ $item->title }}</a></p>
                    @endforeach
                </div>
            </div>
        @elseif($category->id == 9)
        @section('title')Резюме @endsection
        <div class="col-md-12">
            @include('layouts.rezume')
                <div class="content_otzyvy clearfix">
                    @foreach(Helper::getCategoryItems($category->id) as $key=>$item)
                       {{--{{dd($item)}}--}}
                        <div class="col-md-3 col-sm-3 col-xs-6 rez-{{$item->slug}}">
                            <a href="{{$item->source}}" target="_blank">
                                <div class="rezume_block">
                                    <div class="worker">
                                        <p class="name_worker">{{$item->title}}</p>
                                    </div>
                                    <div class="post_work">
                                        <?
                                        $slugs = [
                                                1 => 'Юриспруденция',
                                                2 => 'Топ-менеджмент',
                                                3 => 'Бухгалтерия',
                                                4 => 'Страхование и банки',
                                                5 => 'IT, телеком',
                                                6 => 'Продажи и менеджмент',
                                                7 => 'Строительство',
                                                8 => 'Медицина',
                                                9 => 'Прочее'
                                        ];
                                        ?>
                                        <span>{{$slugs[($item->slug)]}}</span>
                                    </div>
                                </div>
                            </a>
                        </div>
                    @endforeach
                </div>
            </div>
            @endif
            </div>
@include('layouts.brand')
@stop                                                                                                                                                                                                                  