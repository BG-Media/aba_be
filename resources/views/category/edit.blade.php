@extends('system.system')

@section('breadcrumb')

    <ul class="breadcrumb">
        <li><a href="#">Home</a></li>
        <li><a href="#">>Category list</a></li>
        <li class="active">Category Edit</li>
    </ul>

@endsection

@section('content')
<!-- PAGE CONTENT WRAPPER -->
<div class="page-content-wrap">
    <div class="row">
        <div class="col-md-12">
            <form action="{{ url('category',[$category->id]) }}" method="POST" enctype="multipart/form-data" class="form-horizontal">
                {{ csrf_field() }}
                {{ method_field('PUT') }}
                <div class="panel panel-default">
                    {!! view('system/inputs.title',['name'=>'Edit Category']) !!}
                    {!! view('category.form',compact('category','categories')) !!}
                    {!! view('system/inputs.submit',['name'=>'Edit Category','icon' =>'edit']) !!}
                </div>
            </form>
        </div>
    </div>
</div>
<!-- END PAGE CONTENT WRAPPER -->
@endsection