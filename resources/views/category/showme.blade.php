    {{--{{dd($items->all())}}--}}
@extends('layouts.app')
@section('title')
Новости
@endsection
@section('content')
        <div class="col-md-11 col-sm-11" itemscope ItemType = "http://aba.kz/category/1">
            <h1 class="list_title">Новости</h1>

            <ol class="breadcrumb">
                <li><a href="{{url('/')}}">Главная</a></li>
                <li class="active">Новости</li>
            </ol>
        </div>

        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="grid">
                @for($i=0,$j=0; $i<count($items); $j++,$i++)
                    @if($j==0)
                        <a href="{{url('post',$items[$i]->id)}}">
                        <div class="grid-item grid-item--height2">
                            <img src="{{$items[$i]->source}}" alt="">
                                <div class="inset_text">
                                    <div class="inset_center_text">
                                        <p>{{$items[$i]->title}}</p>
                                        <p class="inset-2">{{$items[$i]->excerpt}}</p>
                                    </div>
                                </div>
                        </div>
                        </a>
                    @elseif($j==1)
                        <a href="{{url('post',$items[$i]->id)}}">
                        <div class="grid-item">
                            <img src="{{$items[$i]->source}}" alt="">
                                <div class="inset_text">
                                    <div class="inset_center_text">
                                        <p>{{$items[$i]->title}}</p>
                                        <p class="inset-2">{{$items[$i]->excerpt}}</p>
                                    </div>
                                </div>
                        </div>
                        </a>
                    @elseif($j==2)
                        <a href="{{url('post',$items[$i]->id)}}">
                        <div class="grid-item grid-item--height2">
                            <img src="{{$items[$i]->source}}" alt="">
                                <div class="inset_text">
                                    <div class="inset_center_text">
                                        <p>{{$items[$i]->title}}</p>
                                        <p class="inset-2">{{$items[$i]->excerpt}}</p>
                                    </div>
                                </div>
                        </div>
                        </a>
                    @elseif($j==3)
                        <a href="{{url('post',$items[$i]->id)}}">
                        <div class="grid-item grid-item--height2">
                            <img src="{{$items[$i]->source}}" alt="">
                                <div class="inset_text">
                                    <div class="inset_center_text">
                                        <p>{{$items[$i]->title}}</p>
                                        <p class="inset-2">{{$items[$i]->excerpt}}</p>
                                    </div>
                                </div>
                        </div>
                        </a>
                    @elseif($j==4)
                        <a href="{{url('post',$items[$i]->id)}}">
                        <div class="grid-item grid-item--height2 / grid-item--height333">
                            <img src="{{$items[$i]->source}}" alt="">
                                <div class="inset_text">
                                    <div class="inset_center_text">
                                        <p>{{$items[$i]->title}}</p>
                                        <p class="inset-2">{{$items[$i]->excerpt}}</p>
                                    </div>
                                </div>
                        </div>
                        </a>
                    @elseif($j==5)
                        <a href="{{url('post',$items[$i]->id)}}">
                        <div class="grid-item">
                            <img src="{{$items[$i]->source}}" alt="">
                                <div class="inset_text">
                                    <div class="inset_center_text">
                                        <p>{{$items[$i]->title}}</p>
                                        <p class="inset-2">{{$items[$i]->excerpt}}</p>
                                    </div>
                                </div>
                        </div>
                        </a>
                    @elseif($j==6)
                        <a href="{{url('post',$items[$i]->id)}}">
                        <div class="grid-item grid-item--height2 / grid-item--height333">
                            <img src="{{$items[$i]->source}}" alt="">
                                <div class="inset_text">
                                    <div class="inset_center_text">
                                        <p>{{$items[$i]->title}}</p>
                                        <p class="inset-2">{{$items[$i]->excerpt}}</p>
                                    </div>
                                </div>
                        </div>
                        </a>
                    @elseif($j==7)
                        <a href="{{url('post',$items[$i]->id)}}">
                        <div class="grid-item grid-item--height2 / grid-item--height333">
                            <img src="{{$items[$i]->source}}" alt="">
                                <div class="inset_text">
                                    <div class="inset_center_text">
                                        <p>{{$items[$i]->title}}</p>
                                        <p class="inset-2">{{$items[$i]->excerpt}}</p>
                                    </div>
                                </div>
                        </div>
                        </a>
                    @elseif($j==8)
                        <a href="{{url('post',$items[$i]->id)}}">
                        <div class="grid-item">
                            <img src="{{$items[$i]->source}}" alt="">
                                <div class="inset_text">
                                    <div class="inset_center_text">
                                        <p>{{$items[$i]->title}}</p>
                                        <p class="inset-2">{{$items[$i]->excerpt}}</p>
                                    </div>
                                </div>
                        </div>
                        </a>
                        <?php $j=-1?>
                    @endif
                @endfor
            </div>

        </div>

        <div class="col-md-12 col-md-12 text-center">
            {{$items->render()}}
        </div>
        @include('layouts.brand')
@stop