{{--{{dd($items)}}--}}
@extends('layouts.app')
@section('title')
    Мероприятия
@endsection
@section('content')
        <div class="col-md-11 col-sm-11" itemscope ItemType = "http://aba.kz/b2b/show">
            <h1 class="list_title">Мероприятия</h1>

            <ol class="breadcrumb">
                <li><a href="{{url('/')}}">Главная</a></li>
                <li class="active">Мероприятия</li>
            </ol>
        </div>

        <div class="col-md-12 link">

            <div class="filter" data-filter=".category-1">Прошедшие</div>
            <div class="filter" data-filter="all">Все</div>
            <div class="filter" data-filter=".category-2">Предстоящие</div>
        </div>
    {{--{{dd(date('Y-m-d H:i:s'))}}--}}
        <div class="col-md-12">
            <div class="row" id="link">
                @for($i=0;$i<count($items1);$i++)
                    <?php
                    $month = [
                            "01"=>"Январь",
                            "02"=>"Февраль",
                            "03"=>"Март",
                            "04"=>"Апрель",
                            "05"=>"Май",
                            "06"=>"Июнь",
                            "07"=>"Июль",
                            "08"=>"Август",
                            "09"=>"Сентябрь",
                            "10"=>"Октябрь",
                            "11"=>"Ноябрь",
                            "12"=>"Декабрь"
                    ];
                    ?>
                    @if(date('Y-m-d H:i:s')>$items1[$i]->created_at)
                    <div class="col-md-4 col-sm-4 col-xs-12 mix category-1">
                        <div class="block_member">
                            <div class="img_member">
                                <a href="{{url('post',$items1[$i]->id)}}"><img src="{{$items1[$i]->source}}" class="img-responsive" alt=""></a>
                            </div>
                            <div class="text_member">
                                <h4 class="text_date">{{$month[date('m', strtotime($items1[$i]->created_at))] . date(' d, Y', strtotime($items1[$i]->created_at))}}<br><span>{{$items1[$i]->slug}}</span></h4>
                                <p class="info_text">{{$items1[$i]->title}}</p>

                                <a href="{{url('post',$items1[$i]->id)}}">
                                    <button type="button" class="more_info">подробнее</button>
                                </a>
                                <span class="it_was">Было</span>
                            </div>
                        </div>
                    </div>
                    @else
                    <div class="col-md-4 col-sm-4  col-xs-12 mix category-2">
                        <div class="block_member">
                            <div class="img_member">
                                <a href="{{url('post',$items1[$i]->id)}}"><img src="{{$items1[$i]->source}}" class="img-responsive" alt=""></a>
                            </div>
                            <div class="text_member">
                                <h4 class="text_date">{{$month[date('m', strtotime($items1[$i]->created_at))] . date(' d, Y', strtotime($items1[$i]->created_at))}}<br><span>{{$items1[$i]->title}}</span></h4>
                                <p class="info_text">{{$items1[$i]->slug}}</p>

                                <a href="{{url('post',$items1[$i]->id)}}">
                                    <button type="button" class="more_info">подробнее</button>
                                </a>
                                <span class="it_was will_be">Будет</span>
                            </div>
                        </div>
                    </div>
                    @endif
                @endfor
            </div>
        </div>

        <div class="col-md-12 col-md-12 text-center">
            {{--{{$items->render()}}--}}
            {{--<a href="#">--}}
                {{--<button class="buttons">ЗАГРУЗИТЬ ЕЩЕ</button>--}}
            {{--</a>--}}
        </div>
    @include('layouts.brand')
@stop
