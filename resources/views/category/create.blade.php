@extends('system.system')

@section('breadcrumb')

    <ul class="breadcrumb">
        <li><a href="#">Home</a></li>
        <li><a href="#">>Category list</a></li>
        <li class="active">Category Create</li>
    </ul>

@endsection

@section('content')
    <!-- PAGE CONTENT WRAPPER -->
    <div class="page-content-wrap">
        <div class="row">
            <div class="col-md-12">
                <form action="{{ url('category') }}" method="POST" enctype="multipart/form-data" class="form-horizontal">
                    {{ csrf_field() }}
                    <div class="panel panel-default">
                        {!! view('system/inputs.title',['name'=>'Add Category']) !!}
                        {!! view('category.form',compact('categories')) !!}
                        {!! view('system/inputs.submit',['name'=>'Add Category','icon' =>'plus']) !!}
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- END PAGE CONTENT WRAPPER -->
@endsection