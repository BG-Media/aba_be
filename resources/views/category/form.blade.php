<div class="panel-body">
    {!! view('errors.validation') !!}
    {!! view('system.inputs.text',['name'=>'title','value' => isset($category->title) ? $category->title : '','label'=>'Title','id'=>'title']) !!}
    {!! view('system.inputs.text',['name'=>'slug','value' => isset($category->slug) ? $category->slug : '','label'=>'Slug','id'=>'slug']) !!}
    {!! view('system.inputs.textarea',['name'=>'description','value' => isset($category->description) ? $category->description : '','label'=>'Description','id'=>'summernote','rows'=>'3','cols'=>'16']) !!}
    {!! view('system.inputs.select',['name'=>'parent_id','value' => isset($category->parent_id) ? $category->parent_id : '','label'=>'Parent Category','id'=>'parent_id','lists' => $categories,'defaultOption'=>'This is a parent Category' ]) !!}
    {!! view('system.inputs.file', ['name'=>'media_id','value' => isset($category->media_id) ? $category->media_id : '','label'=>'Category media','id'=>'post_media']) !!}
</div>