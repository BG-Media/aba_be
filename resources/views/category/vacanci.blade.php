@extends('layouts.app')
@section('content')
    @if($category->id == 8)
    @section('title')
        Вакансии
    @endsection
        <div class="row">
            <div class="col-md-11 col-sm-11">
                <h1 class="list_title">{{$category->title}}</h1>
                <ol class="breadcrumb">
                    <li><a href="{{url('/')}}">Главная</a></li>
                    <li><a href="#">HR</a></li>
                    <li class="active">{{$category->title}}</li>
                </ol>
            </div>
        </div>
        <div class="vacansy clearfix">
            @for($i=0; $i<count($items); $i++)
                <div class="box">
                    <div class="top">
                        <p>{{$items[$i]->title}}</p>
                    </div>
                    <div class="bottom">
                        <p>Требование:</p>
                        <ul>
                            <p>{!!$items[$i]->content!!}</p>
                        </ul>
                    </div>
                </div>
            @endfor
        </div>
    @elseif($category->id == 10)
    @section('title')
        Проекты
    @endsection
        <div class="row">
            <div class="col-md-11 col-sm-11">
                <h1 class="list_title">{{$category->title}}</h1>

                <ol class="breadcrumb">
                    <li><a href="{{url('/')}}">Главная</a></li>
                    <li class="active">Проекты</li>
                </ol>
            </div>
        </div>

        <div class="vacansy clearfix">
            @for($i=0; $i<count($items);$i++)
                <div class="box">
                    <div class="top">
                        <p>{{$items[$i]->title}}</p>
                    </div>
                    <div class="bottom">
                        <h3><b>{{$items[$i]->title}}</b></h3>
                        <p>{!!$items[$i]->content!!}</p>
                        <?
                        $link = $items[$i]->slug;
                        $arrlink = explode(",", $link);
                        $namelink = $items[$i]->excerpt;
                        $arrname = explode(",",$namelink);
                        ?>
                        <p>Прошедшие мероприятия:</p>
                            @foreach($arrlink as $key => $value)
                                <p>{{$key+1}}) <a href="{{$value}}" target="_blank">{{$arrname[$key]}}</a></p>
                            @endforeach
                    </div>
                </div>
            @endfor
        </div>
    @endif
@include('layouts.brand')
@endsection