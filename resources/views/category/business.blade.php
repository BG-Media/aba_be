{{--{{dd($items->all())}}--}}
@extends('layouts.app')
@section('title')
    Поддержка бизнеса
@endsection
@section('content')
    <div class="col-md-11 col-sm-11">
        <h1 class="list_title">{{$category->title}}</h1>

        <ol class="breadcrumb">
            <li><a href="{{url('/')}}">Главная</a></li>
            <li class="active">{{$category->title}}</li>
        </ol>
    </div>

    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="row">
            @for($i=0; $i<count($items1); $i++)
                <div class="col-md-3 col-sm-4 col-xs-6 blocks_mambers" >
                    <a href="{{url('post',$items1[$i]->id)}}">
                        <div class="img_suuport">
                            <img src="{{$items1[$i]->source}}" class="img-responsive" alt="{{$items1[$i]->title}}">
                        </div>
                    </a>
                    <div class="link_support">
                        <p class="title-support"><a href="{{url('post',$items1[$i]->id)}}" >{{$items1[$i]->title}}</a></p>
                    </div>
                </div>
            @endfor
        </div>
    </div>
    @include('layouts.brand')
    @endsection