<div class="panel-body">
{!! view('errors.validation') !!}
{!! view('system.inputs.text',['name'=>'link','value' => isset($banner->link) ? $banner->link : '','label'=>'Ссылка','id'=>'link']) !!}
    <div class="form-group">
        <label class="col-md-3 control-label">Вид</label>
        <div class="col-md-6">
            <select class="form-control select"  name="value">
                <option value="1">Бренды</option>
                <option value="2">Баннеры</option>
                <option value="3">Партнеры</option>
            </select>
        </div>
    </div>
    {!! view('system.inputs.file', ['name'=>'image','value' => isset($banner->image) ? $banner->image : '','label'=>'Картинка','id'=>'images']) !!}
</div>