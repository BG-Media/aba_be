@extends('system.system')

@section('breadcrumb')

    <ul class="breadcrumb">
        <li><a href="#">Home</a></li>
        <li><a href="#">>Advertising list</a></li>
        <li class="active">Advertising Edit</li>
    </ul>

    @endsection

    @section('content')
            <!-- PAGE CONTENT WRAPPER -->
    <div class="page-content-wrap">
        <div class="row">
            <div class="col-md-12">
                <form action="{{ url('banner',[$banner->id]) }}" method="POST" enctype="multipart/form-data" class="form-horizontal">
                    {{ csrf_field() }}
                    {{ method_field('PUT') }}
                    <div class="panel panel-default">
                        {!! view('system/inputs.title',['name'=>'Add Advertising']) !!}
                        {!! view('banner.form',compact('banner')) !!}
                        {!! view('system/inputs.submit',['name'=>'Add Advertising','icon' =>'plus']) !!}
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- END PAGE CONTENT WRAPPER -->
@endsection