@extends('system.system')

@section('breadcrumb')

    <ul class="breadcrumb">
        <li><a href="#">Home</a></li>
        <li><a href="#">>Page list</a></li>
        <li class="active">Page Create</li>
    </ul>

@endsection

@section('content')
    <!-- PAGE CONTENT WRAPPER -->
    <div class="page-content-wrap">
        <div class="row">
            <div class="col-md-12">
                <form action="{{ url('page') }}" method="POST" enctype="multipart/form-data" class="form-horizontal">
                    {{ csrf_field() }}
                    <div class="panel panel-default">
                        {!! view('system/inputs.title',['name'=>'Add Page']) !!}
                        {!! view('page.form',compact('page')) !!}
                        {!! view('system/inputs.submit',['name'=>'Add Page','icon' =>'plus']) !!}
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- END PAGE CONTENT WRAPPER -->
@endsection