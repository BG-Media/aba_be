@extends('system.system')

@section('breadcrumb')

    <ul class="breadcrumb">
        <li><a href="#">Home</a></li>
        <li class="active">Page list</li>
    </ul>

@endsection

@section('title')
     <!-- PAGE TITLE -->
    <div class="page-title">
        <h2><span class="fa fa-arrow-circle-o-left"></span> Pages</h2>
    </div>
    <!-- END PAGE TITLE -->
@endsection

@section('content')

    <div class="page-content-wrap">

        <div class="row">
            <div class="col-md-12">

                <!-- START DEFAULT DATATABLE -->
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            <button onclick="location.href='{{ url('page/create') }}';"  class="btn btn-info">
                                <i class="fa fa-plus"></i> Add new page
                            </button>
                        </h3>
                        <ul class="panel-controls">
                            <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                            <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span></a></li>
                            <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                        </ul>
                    </div>
                    @if (count($pages) > 0)
                    <div class="panel-body">
                        <table class="table datatable">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Title</th>
                                <th>Slug</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($pages as $page)
                                <tr>
                                    <td>
                                        <div>{{ $page->id }}</div>
                                    </td>
                                    <td>
                                        <div>{{ $page->title }}</div>
                                    </td>
                                    <td>
                                        <div>{{ $page->slug }}</div>
                                    </td>
                                    <td>
                                        <a href="/page/{{$page->id}}" target="_blank" class="btn btn-info"><i class="fa fa-btn fa-eye"></i>Show</a>
                                        <a href="/page/{{ $page->id }}/edit" class="btn btn-primary"><i class="fa fa-btn fa-edit"></i>Edit</a>
                                        <form action="{{ url('page/'.$page->id) }}" method="POST" style="display: inline-block">
                                            {{ csrf_field() }}
                                            {{ method_field('DELETE') }}
                                            <button type="submit" id="delete-post-{{ $page->id }}" class="btn btn-danger">
                                                <i class="fa fa-btn fa-trash"></i>Delete
                                            </button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    @endif
                </div>
                <!-- END DEFAULT DATATABLE -->
            </div>
        </div>
    </div>

@endsection

@section('page_plugins')

        <!-- START PAGE PLUGINS -->
<script type='text/javascript' src="{{asset('system/js/plugins/icheck/icheck.min.js')}}"></script>
<script type="text/javascript" src="{{asset('system/js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js')}}"></script>

<script type="text/javascript" src="{{asset('system/js/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<!-- END PAGE PLUGINS -->

@endsection