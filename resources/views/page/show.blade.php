@extends('layouts.app')
@section('content')
    <div class="col-md-11 col-sm-11">
        <h1 class="list_title">{{$page->title}}</h1>
        <ol class="breadcrumb">
            <li><a href="{{url('/')}}">Главная</a></li>
            <li><a href="#">@if ($page->id == 4) Aba @elseif($page->id == 8 )Aba card @elseif($page->id == 9 || 10 || 11 || 12) Спонсорство @else  Об ассоциации@endif</a></li>
            <li class="active">{{$page->title}}</li>
        </ol>
    </div>
@if($page->id == 4)
    @section('title')Контакты@endsection
    <div class="col-md-6 col-sm-6">
        <p class="info_address"><img src="/themes/aba/images/icon_adrress.png" class="icon_address" alt="">050010 Казахстан, Алматы <br>ул. Сатпаева 30а/3(Весновка) <br>оф.125, 3 этаж ЖК "Tengiz Towers", Алматы </p>

        <p class="info_address number_phone"><img src="/themes/aba/images/icon_phone.png" class="icon_address" alt="">+7(707) 700 11 63 <br>+7(778) 377 46 76  </p>

        <p class="info_address email_address"><img src="/themes/aba/images/icon_email.png" class="icon_address" alt="">info@aba.kz </p>

        <h2><a href="" class="bank_details" data-toggle="modal" data-target="#myModal3">БАНКОВСКИЕ РЕКВИЗИТЫ</a></h2>
        <div class="modal fade" id="myModal3" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body clearfix">
                        <p class="title_modal">БАНКОВСКИЕ РЕКВИЗИТЫ</p>

                        <ul class="banks_detal">
                            <li><b>Бенефициар:</b> Объединение юридических лиц «Алматинская Бизнес Ассоциация»</li>
                            <li><b>РНН:</b> 600400637074</li>
                            <li><b>Юридический адрес:</b> 050013, ул.Сатпаева 30а/1 (угол.ул.Шагабутдинова), офис 77, 2 этаж ЖК "Tengiz Towers"</li>
                            <li><b>Фактический адрес:</b> 050010, Казахстан, Алматы, ул. Сатпаева 30а/3 (Весновка) офис 125, 3 этаж</li>
                            <li><b>Банк бенефициара</b>: АО "Казкоммерцбанк", г.Алматы</li>
                            <li><b>ИИК:</b> KZ879261802161616000</li>
                            <li><b>БИК:</b> KZKOKZKX</li>
                            <li><b>БИН:</b> 111240001357</li>
                            <li><b>Кбе</b> 18</li>
                            <li><b>Контактный тел:</b> 8 778 377 4676</li>
                            <li><b>Исполнительный директор:</b> Досанбай Олжас действующий на основании устава.</li>
                        </ul>

                    </div>
                </div>

            </div>
        </div>

    </div>

    <div class="col-md-6 col-sm-6">
        <div class="maps">
            <script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?sid=wjsQZciJan-KVFOhIlVpFxOU6xoDWETQ&amp;width=100%&amp;height=540&amp;lang=ru_RU&amp;sourceType=constructor&amp;scroll=true"></script>
        </div>
    </div>

@else
<div class="col-md-12 @if($page->id ==1 ) @elseif($page->id ==2 || $page->id ==3) content_news goals_objectives status @endif">
    @if($page->id ==1 )
        @section('title')
            О нас
        @endsection
        <div class="col-md-11 col-sm-12 col-xs-12 content_about content_support">
            {!! $page->content !!}
            {{--<img src="{{ asset('themes/aba/images/about.png') }}" class="img-responsive" alt="">--}}
        </div>
        @include('layouts.brand')
    @elseif($page->id ==2 || $page->id ==3)
        <p class="languages"><a href="{{ url('page',2) }}">Қазақша</a> <a href="{{ url('page',3) }}">Русский</a></p>
        <div class="text_status">
            {!! $page->content !!}
        </div>
    @else
        @section('title')
            Об ассоциации / {{$page->title}}
        @endsection
        <div class="text_status">
            {!! $page->content !!}
        </div>
        </div>
        @include('layouts.brand')
    @endif
</div>
@endif
 @stop