<div class="panel-body">
    {!! view('errors.validation') !!}
    {!! view('system.inputs.text',['name'=>'title','value' => isset($page->title) ? $page->title : '','label'=>'Title','id'=>'title']) !!}
    {!! view('system.inputs.text',['name'=>'slug','value' => isset($page->slug) ? $page->slug : '','label'=>'Slug','id'=>'slug']) !!}
    {!! view('system.inputs.textarea',['name'=>'content','value' => isset($page->content) ? $page->content : '','label'=>'Content','id'=>'summernote','rows'=>'3','cols'=>'16']) !!}
    {!! view('system.inputs.file', ['name'=>'media_id','value' => isset($page->media_id) ? $page->media_id : '','label'=>'Post media','id'=>'post_media']) !!}
</div>