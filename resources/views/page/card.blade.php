@extends('layouts.app')
@section('title')
    Держатели card
@endsection
@section('content')
    <div class="col-md-11 col-sm-11">
        <h1 class="list_title">Держатели card</h1>

        <ol class="breadcrumb">
            <li><a href="{{url('/')}}">Главная</a></li>
            <li><a href="#">Aba card</a></li>
            <li class="active">Держатели card</li>
        </ol>
    </div>

    <div class="col-md-12">

        <div class="content_otzyvy clearfix">
            <form action="" method="">
                <div class="col-md-3 col-sm-3 col-xs-6 search_info">
                    <input type="text" placeholder="Поиск по номер карт" class="input_holders" name="number" id="number">
                </div>
                <div class="col-md-3 col-sm-3 col-xs-6 search_info">
                    <input type="text" placeholder="Поиск ФИО" class="input_holders" name="fio" id="fio">
                </div>
                <div class="col-md-3 col-sm-3 col-xs-6 search_info">
                    <input type="text" placeholder="Поиск по названию компании" class="input_holders" name="company" id="company">
                </div>
                {{--<div class="col-md-3 col-sm-3 col-xs-6 search_info">--}}
                    {{--<input type="text" placeholder="Поиск по ИИН" class="input_holders" name="iin" id="iin">--}}
                {{--</div>--}}
            </form>
        </div>

        <div class="info_data">
            <table>
                <thead>
                    <tr>
                        <td id="result">Номер карты</td>
                        <td>ФИО</td>
                        <td>Компания</td>
                        {{--<td>ИИН</td>--}}
                    </tr>
                </thead>
                <tbody>

                </tbody>
            </table>
        </div>

    </div>
    @include('layouts.brand')
@endsection