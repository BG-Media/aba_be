{{--{{dd($banner)}}--}}
<div class="col-md-11 col-sm-11" style="z-index: -1;">
    <h1 class="list_title">Бренды года</h1>
</div>
<div class="col-md-12 col-sm-12 col-xs-12">
    <div class="slider_1">
        @foreach($banner as $item)
            @if($item->value == 1)
            <div><a href="{{$item->link}}"><img src="{{$item->image}}" class="img-responsive" alt=""></a></div>
            @endif
        @endforeach
    </div>
</div>