<div class="col-md-11 col-sm-11">
    <h1 class="list_title">Последние новости</h1>
</div>

<?php
$news  = Helper::getCategoryPagination(1,9);
?>
<div class="col-md-12 col-sm-12 col-xs-12">
    <div class="grid">
        @for ($i=0, $j=0; $i<count($news); $j++, $i++)
            @if($j==0)
            <div class="grid-item grid-item--height2 news_{{$i}}">
                    <img src="{{$news[$i]->source}}" alt="">
                    <a href="{{url('post',$news[$i]->id)}}">
                        <div class="inset_text">
                            <div class="inset_center_text">
                                <p>{{$news[$i]->title}}</p>
                                <p class="inset-2">{{$news[$i]->excerpt}}</p>
                            </div>
                        </div>
                    </a>
                </div>
            @elseif($j==1)
                <div class="grid-item news_{{$i}}">
                    <img src="{{$news[$i]->source}}" alt="">
                    <a href="{{url ('post',$news[$i]->id)}}">
                        <div class="inset_text">
                            <div class="inset_center_text">
                                <p>{{$news[$i]->title}}</p>
                                <p class="inset-2">{{$news[$i]->excerpt}}</p>
                            </div>
                        </div>
                    </a>
                </div>
            @elseif ($j==2)
                <div class="grid-item grid-item--height2 news_{{$i}}">
                    <img src="{{$news[$i]->source}}" alt="">
                    <a href="{{url ('post',$news[$i]->id)}}">
                        <div class="inset_text">
                            <div class="inset_center_text">
                                <p>{{$news[$i]->title}}</p>
                                <p class="inset-2">{{$news[$i]->excerpt}}</p>
                            </div>
                        </div>
                    </a>
                </div>

            @elseif($j==3)
                <div class="grid-item grid-item--height2 news_{{$i}}">
                    <img src="{{$news[$i]->source}}" alt="">
                    <a href="{{url('post',$news[$i]->id)}}">
                        <div class="inset_text">
                            <div class="inset_center_text">
                                <p>{{$news[$i]->title}}</p>
                                <p class="inset-2">{{$news[$i]->excerpt}}</p>
                            </div>
                        </div>
                    </a>
                </div>
            @elseif($j==4)
                <div class="grid-item grid-item--height2 news_{{$i}}">
                    <img src="{{$news[$i]->source}}" alt="">
                    <a href="{{url('post',$news[$i]->id)}}">
                        <div class="inset_text">
                            <div class="inset_center_text">
                                <p>{{$news[$i]->title}}</p>
                                <p class="inset-2">{{$news[$i]->excerpt}}</p>
                            </div>
                        </div>
                    </a>
                </div>
            @elseif($j==5)
                <div class="grid-item news_{{$i}} ">
                    <img src="{{$news[$i]->source}}" alt="">
                    <a href="{{url('post',$news[$i]->id)}}">
                        <div class="inset_text">
                            <div class="inset_center_text">
                                <p>{{$news[$i]->title}}</p>
                                <p class="inset-2">{{$news[$i]->excerpt}}</p>
                            </div>
                        </div>
                    </a>
                </div>
            @elseif($j==6)
                <div class="grid-item grid-item--height2 news_{{$i}}">
                    <img src="{{$news[$i]->source}}" alt="">
                    <a href="{{url('post',$news[$i]->id)}}">
                        <div class="inset_text">
                            <div class="inset_center_text">
                                <p>{{$news[$i]->title}}</p>
                                <p class="inset-2">{{$news[$i]->excerpt}}</p>
                            </div>
                        </div>
                    </a>
                </div>
            @elseif($j==7)
                <div class="grid-item grid-item--height2 news_{{$i}}">
                    <img src="{{$news[$i]->source}}" alt="">
                    <a href="{{url('post',$news[$i]->id)}}">
                        <div class="inset_text">
                            <div class="inset_center_text">
                                <p>{{$news[$i]->title}}</p>
                                <p class="inset-2">{{$news[$i]->excerpt}}</p>
                            </div>
                        </div>
                    </a>
                </div>
            @elseif($j==8)
                <div class="grid-item news_{{$i}}">
                    <img src="{{$news[$i]->source}}" alt="">
                    <a href="{{url('post',$news[$i]->id)}}">
                        <div class="inset_text">
                            <div class="inset_center_text">
                                <p>{{$news[$i]->title}}</p>
                                <p class="inset-2">{{$news[$i]->excerpt}}</p>
                            </div>
                        </div>
                    </a>
                </div>
            @endif
        @endfor
    </div>

</div>