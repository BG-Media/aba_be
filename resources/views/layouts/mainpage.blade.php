<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="Robots" content="INDEX, FOLLOW">
    <meta name="google-site-verification" content="rnxYy0T0Hn1SypHxOTIW_gyQpW_uL_E68SuWUpfGMbk" />
    <meta name="yandex-verification" content="5588e44ee24ac4e8" />
    <meta name="description" content="ABA - cайт предпринимателей, крупнейшей бизнес-ассоциации, некоммерческой организации. Все о развитии предпринимательства. Новости бизнеса. Ведение бизнеса в Казахстане и бизнес-климат. Все о поддержке и защите бизнеса, бизнес в ">
    <meta name="keywords" content="Аба,  бизнес ассоциация, бизнес в Алматы, бизнес алматы, ассоциация , алматы бизнес ассоциациясы , ассоциация бизнес тренеров, ассоциация бизнес красоты, поддержка бизнеса, защита бизнеса, сервисы, закупки, совет по защите прав предпринимателей, совет по противодействию коррупции, бизнес план, бизнес молодость, бизнес с нуля, бизнес жоспар, бизнес молодость алматы, бизес центры алматы,бизнес ангелы алматы, бизнес в казахстане, бизнес для студентов,  аренда бизнеса в алматы, бизнес идеи, новости, аналитика в Казахстане, Алиэкспресс, покупки, инструкции, идеи для бизнеса, ответы на вопросы, форум">

    <title>@yield('title')</title>
    <link href="/themes/aba/images/favicon.ico" rel="shortcut icon" type="image/x-icon" />
    <link rel="stylesheet" href="{{asset('/themes/aba/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('/themes/aba/css/style.css')}}">
    <link rel="stylesheet" href="{{asset('/themes/aba/lib/slick/slick.css')}}">
    <link rel="stylesheet" href="{{asset('/themes/aba/lib/slick/slick-theme.css')}}">
</head>

<body>

<div class="header">

    @include('layouts.menu')

    @include('layouts.header')
</div>

<div class="content">
    @yield('content')
</div>

<div class="footer">
    <div class="container-fluid">
        <div class="row">
            <div class="container">
                <h3>Алматы Бизнес Ассоциациясы</h3>
                <h3>© 2011-2016 Некоммерческая организация</h3>
                <h5 class="link_bg">Сайт <a href="">“Bugin Group”</a> компаниясымен әзірленген</h5>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body clearfix">
                <p class="title_modal">Бесплатная подписка</p>
                <p>Чтобы быть в курсе всех событий ассоциации</p>
                <form action="/subscription" method="post">
                    {{ csrf_field() }}
                    <span><img src="{{asset('/themes/aba/images/massage_modal.png')}}" alt=""></span>
                    <input type="email" placeholder="e-mail" class="input_modal" name="email">
                    <input type="submit" value="Отправить" class="submit_modal">
                </form>
            </div>
        </div>

    </div>
</div>

<div class="modal fade" id="myModal2" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body clearfix">
                <p class="title_modal">Как вступить в Ассоциацию</p>
                <ol>
                    <li><span>1</span>Заполните заявку ниже</li>
                    <li><span>2</span>Мы связываемся с вами и назначаем встречу</li>
                    <li><span>3</span>Выявляем ваши потребности, рассматриваем пути их решения</li>
                    <li><span>4</span>Рассматриваем вашу заявку</li>
                </ol>
                <form action="application" method="post">
                    {{ csrf_field() }}
                    <span><img src="{{asset('/themes/aba/images/user_modal.png')}}" alt=""></span>
                    <input type="text" placeholder="ФИО" class="input_modal" name="name">
                    <span><img src="{{asset('/themes/aba/images/phone_modal.png')}}" alt=""></span>
                    <input type="tel" placeholder="Телефон" class="input_modal" name="phone">
                    <span><img src="{{asset('/themes/aba/images/massage_modal.png')}}" alt=""></span>
                    <input type="email" placeholder="e-mail" class="input_modal" name="email">
                    <span><img src="{{asset('/themes/aba/images/auitcase_modal.png')}}" alt=""></span>
                    <input type="text" placeholder="Название компании" class="input_modal" name="company">
                    <input type="submit" value="Отправить" class="submit_modal">
                </form>
            </div>
        </div>

    </div>
</div>
<div class="icons">
    <div>
        <a href="https://www.facebook.com/almatybusinessassociation/" target="_blank"><img src="{{asset('/themes/aba/images/fb.png')}}" alt=""></a>
    </div>
    <div>
        <a href="https://www.instagram.com/aba_almaty/" target="_blank"><img src="{{asset('/themes/aba/images/ins.png')}}" alt=""></a>
    </div>
    <div>
        <a href="https://www.youtube.com/user/abasocialmarketing" target="_blank"><img src="{{asset('/themes/aba/images/you_tube.png')}}" alt=""></a>
    </div>
</div>
{{--<div id="success" class="modalDialog1">--}}
    {{--<div>--}}
        {{--<a href="#close" title="Закрыть" class="close">&times;</a>--}}
        {{--<h2>Спасибо за заявку</h2>--}}
        {{--<p>Ваш запрос был успешно отправлен.--}}
            {{--Мы свяжемся с вами в ближайшее время. Благодарим за проявленный интерес!</p>--}}
    {{--</div>--}}
{{--</div>--}}
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-85993781-1', 'auto');
  ga('send', 'pageview');

</script>
<!-- Yandex.Metrika counter --> <script type="text/javascript"> (function (d, w, c) { (w[c] = w[c] || []).push(function() { try { w.yaCounter40347385 = new Ya.Metrika({ id:40347385, clickmap:true, trackLinks:true, accurateTrackBounce:true, webvisor:true, trackHash:true }); } catch(e) { } }); var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function () { n.parentNode.insertBefore(s, n); }; s.type = "text/javascript"; s.async = true; s.src = "https://mc.yandex.ru/metrika/watch.js"; if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); } })(document, window, "yandex_metrika_callbacks"); </script> <noscript><div><img src="https://mc.yandex.ru/watch/40347385" style="position:absolute; left:-9999px;" alt="" /></div></noscript> <!-- /Yandex.Metrika counter -->
<script src="{{asset('/themes/aba/js/jquery-1.11.0.js')}}"></script>
<script src="{{asset('/themes/aba/js/bootstrap.min.js')}}"></script>
<script src="{{asset('/themes/aba/lib/slick/slick.min.js')}}"></script>
<script src="{{asset('/themes/aba/lib/slick/jquery-migrate-1.2.1.min.js')}}"></script>
<script src="{{asset('/themes/aba/lib/plita/masonry.pkgd.min.js')}}"></script>
<script>
    $('.slider-reklama').slick({
        arrows:false,
        dots:false,
        fade:true,
        autoplay:true,
        autoplaySpeed:3000
    });
</script>
<script>
    $('.slider_1').slick({
        arrows: true,
        dots: false,
        autoplay: true,
        autoplaySpeed: 2000,
        slidesToShow: 5,
        slidesToScroll: 4,
        responsive: [{
            breakpoint: 992,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 3,
                infinite: true,
                dots: true
            }
        }, {
            breakpoint: 600,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 2
            }
        }, {
            breakpoint: 480,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1
            }
        }]
    });
    $('.slider_2').slick({
        arrows: true,
        dots: false,
        autoplay: true,
        autoplaySpeed: 2000,
        slidesToShow: 5,
        slidesToScroll: 5,
        adaptiveHeight: true,
        responsive: [{
            breakpoint: 992,
            settings: {
                slidesToShow: 4,
                slidesToScroll: 4,
                infinite: true,
                dots: true
            }
        }, {
            breakpoint: 767,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 3
            }
        }, {
            breakpoint: 600,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 2
            }
        }, {
            breakpoint: 480,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1
            }
        }]
    });
    $('.grid').masonry({
        itemSelector: '.grid-item',
        // columnWidth: 371,
        isFitWidth: true
    });

    function openNav() {
        document.getElementById("mySidenav").style.display = "block";
    }

    function closeNav() {
        document.getElementById("mySidenav").style.display = "none";
    }
</script>
</body>

</html>