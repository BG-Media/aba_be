<div class="menu_inside" id="mySidenav">
    <div class="container-fluid">

        <div class="inset_menu_top ">
            <a onclick="closeNav()" class="close_menu"><img src="{{asset('themes/aba/images/x.png')}}" class="icon_inside_menu" alt=""></a>
        </div>

        <div class="row">
            <nav>
                <div class="col-md-12 col-sm-12 col-xs-6 inset_menu_center ">
    
                    <div class="col-md-3 col-sm-3">
                        <h3><a href="#text_inset4" data-toggle="collapse" class="button_click hidden-lg hidden-md hidden-sm">Об ассоциации</a></h3>
                        <h3 class="hidden-xs">Об ассоциации</h3>
                        <ul class="collapse  list_menu " id="text_inset4">
                            <li><a href="{{url('page',1)}}">О нас</a></li>
                            <li><a href="{{url('category',10)}}">Проекты</a></li>
                            <li><a href="{{url('catalog')}}">Каталог членов</a></li>
                            <li><a href="{{url('category',5)}}">Правление</a></li>
                            <li><a href="{{url('page',6)}}">Устав</a></li>
                            <li><a href="{{url('category',6)}}">Отзывы</a></li>
                        </ul>
                    </div>
    
                    <div class="col-md-3 col-sm-3">
                        <h3><a href="{{url('category',1)}}">Новости</a></h3>
                        <h3><a href="{{url('category',2)}}">Мероприятия</a></h3>
                    </div>
                    <div class="col-md-3 col-sm-3">
                        <h3><a href="{{url('b2b/show')}}">B2B</a></h3>
                    </div>
    
                    <div class="col-md-3 col-sm-3">
                        <h3><a href="{{url('category',7)}}">Поддержка бизнеса</a></h3>
                    </div>
    
                </div>
    
                <div class="col-md-12 col-sm-12 col-xs-6 inset_menu_center ">
    
                    <div class="col-md-3 col-sm-3">
                        <h3><a href="#text_inset3" data-toggle="collapse" class="button_click hidden-lg hidden-md hidden-sm">ABA CARD</a></h3>
                        <h3 class="hidden-xs">ABA CARD</h3>
                        <ul class="collapse list_menu " id="text_inset3">
                            <li><a href="{{url('page',7)}}"> О проекте</a></li>
                            <li><a href="{{url('sales')}}">Скидки</a></li>
                            <li><a href="{{url('page',8)}}">Стать участником</a></li>
                            <li><a href="{{url('card',1)}}">Держатели карт </a></li>
                        </ul>
                    </div>
    
                    <div class="col-md-3 col-sm-3">
                        <h3><a href="#text_inset2" data-toggle="collapse" class="button_click hidden-lg hidden-md hidden-sm">Спонсорство</a></h3>
                        <h3 class="hidden-xs">Спонсорство</h3>
                        <ul class="collapse list_menu " id="text_inset2">
                            <li><a href="{{url('page',10)}}">Праздник Наурыз</a></li>
                            <li><a href="{{url('page',9)}}"> Годовое Общее Собрание членов АБА</a></li>
                            <li><a href="{{url('page',11)}}">Выставка членов АБА “ABA BRIDGE”</a></li>
                            <li><a href="{{url('page',13)}}">АБА CARD</a></li>
                            <li><a href="{{url('page',12)}}">Бренды АБА</a></li>
                        </ul>
                    </div>
    
                    <div class="col-md-3 col-sm-3">
                        <h3><a href="#text_inset1" data-toggle="collapse" class="button_click hidden-lg hidden-md hidden-sm">HR </a></h3>
                        <h3 class="hidden-xs">HR</h3>
                        <ul class="collapse list_menu " id="text_inset1">
                            <li><a href="{{url('/category',8)}}">Вакансии </a></li>
                            <li><a href="{{url('/category',9)}}">Резюме</a></li>
                        </ul>
                    </div>
    
                    <div class="col-md-3 col-sm-3">
                        <h3><a href="{{url('page',4)}}">Контакты</a></h3>
                    </div>
    
                </div>
            </nav>
        </div>

    </div>
</div>