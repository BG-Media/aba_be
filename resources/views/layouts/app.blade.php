<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="Robots" content="INDEX, FOLLOW">
    <meta name="description" content="aba.kz - cайт Национальной палаты предпринимателей, крупнейшей бизнес-ассоциации, некоммерческой организации. Все о развитии предпринимательства. Новости бизнеса. Ведение бизнеса в Казахстане и бизнес-климат. Все о поддержке и защите бизнеса, бизнес в ">
    <meta name="keywords" content="Аба,  бизнес ассоциация, алматы, бизнес, бизнес в Алматы, бизнес алматы ассоциация , алматы бизнес ассоциациясы , ассоциация бизнес тренеров, ассоциация бизнес красоты, поддержка бизнеса, защита бизнеса, сервисы, закупки, совет по защите прав предпринимателей, совет по противодействию коррупции, бизнес план, бизнес молодость, бизнес с нуля, бизнес жоспар, бизнес молодость алматы, бизес центры алматы,бизнес ангелы алматы, бизнес в казахстане, бизнес для студентов,  аренда бизнеса в алматы, бизнес идеи, новости, аналитика в Казахстане, Алиэкспресс, покупки, инструкции, идеи для бизнеса, ответы на вопросы, форум">

    <title>@yield('title')</title>
    <link href="/themes/aba/images/favicon.ico" rel="shortcut icon" type="image/x-icon" />
    <link rel="stylesheet" href="{{asset('/themes/aba/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('/themes/aba/css/style.css')}}">
    <link rel="stylesheet" href="{{asset('/themes/aba/lib/fancybox/jquery.fancybox.css')}}">
    <link rel="stylesheet" href="{{asset('/themes/aba/lib/slick/slick.css')}}">
    <link rel="stylesheet" href="{{asset('/themes/aba/lib/slick/slick-theme.css')}}">
    <link rel="stylesheet" href="{{asset('/themes/aba/lib/magnific-popup/magnific-popup.css')}}">
</head>

<body>

<div class="header">
    @include('layouts.menu')
    <div class="container-fluid">
        <div class="row menu header_us_page">

            <div class="col-md-2 col-sm-3 col-xs-4">
                <a href="{{asset('/')}}"><img src="{{asset('/themes/aba/images/logo.png')}}" class="img-responsive logo" alt=""></a>
            </div>
            <div class="col-md-5 col-sm-4 hidden-xs">
                <div class="icon_mob text-center">
                    <a href="http://www.kazka.kz/" target="_blank"><img src="{{asset('/themes/aba/images/kazkka.PNG')}}" class="img-responsive " alt="" ></a>
                    {{--<p>СКАЧАЙТЕ ПРИЛОЖЕНИЕ</p>--}}
                    {{--<a href=""><img src="{{asset('/themes/aba/images/app.png')}}" alt=""></a>--}}
                    {{--<a href=""><img src="{{asset('/themes/aba/images/google_play.png')}}" alt=""></a>--}}
                </div>
            </div>
            <div class=" col-md-5 col-sm-5 col-xs-8">
                <div class="right_block">

                    <div class="block1 block11">
                        <a href="#" data-toggle="modal" data-target="#myModal">
                            <img src="{{asset('/themes/aba/images/icon_masseg.png')}}" class="center-block icon-1" alt="">
                            <p class="text_links">Подписаться на новости</p>
                        </a>
                    </div>

                    <div class="block1 block11">
                        <a href="#" data-toggle="modal" data-target="#myModal2">
                            <img src="{{asset('/themes/aba/images/icon_user.png')}}" class="center-block icon-2" alt="">
                            <p class="text_links">Вступить в ассоциацию</p>
                        </a>
                    </div>

                    <div class="block1">
                        <a href="#">
                            <img src="{{asset('/themes/aba/images/icon_menu.png')}}" class="center-block" onclick="openNav()" alt="">
                        </a>
                        <p style="color: #fff; padding-top: 5px;">Меню</p>
                    </div>

                </div>
            </div>

        </div>
    </div>
</div>

<div class="content content_top">
    <div class="container-fluid">
        <div class="row">
            <div class="container">
               <div class="row">
                   @yield('content')
               </div>

            </div>
        </div>
    </div>
</div>

<div class="footer">
    <div class="container-fluid">
        <div class="row">
            <div class="container">
                <h3>Алматы Бизнес Ассоциациясы</h3>
                <h3>© 2011-2016 Некоммерческая организация</h3>
                <h5 class="link_bg">Сайт <a href="">“Bugin Group”</a> компаниясымен әзірленген</h5>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body clearfix">
                <p class="title_modal">Бесплатная подписка</p>
                <p>Чтобы быть в курсе всех событий ассоциации</p>
                <form action="send_email" method="post">
                    {{ csrf_field() }}
                    <span><img src="{{asset('/themes/aba/images/massage_modal.png')}}" alt=""></span>
                    <input type="email" placeholder="e-mail" class="input_modal" name="email-p">
                    <input type="submit" value="Отправить" class="submit_modal">
                </form>
            </div>
        </div>

    </div>
</div>

<div class="modal fade" id="myModal2" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body clearfix">
                <p class="title_modal">Как вступить в Ассоциацию</p>
                <ol>
                    <li><span>1</span>Заполните заявку ниже</li>
                    <li><span>2</span>Мы связываемся с вами и назначаем встречу</li>
                    <li><span>3</span>Выявляем ваши потребности, рассматриваем пути их решения</li>
                    <li><span>4</span>Рассматриваем вашу заявку</li>
                </ol>
                <form action="send" method="post">
                    {{ csrf_field() }}
                    <span><img src="{{asset('/themes/aba/images/user_modal.png')}}" alt=""></span>
                    <input type="text" placeholder="ФИО" class="input_modal" name="fio">
                    <span><img src="{{asset('/themes/aba/images/phone_modal.png')}}" alt=""></span>
                    <input type="tel" placeholder="Телефон" class="input_modal" name="phone">
                    <span><img src="{{asset('/themes/aba/images/massage_modal.png')}}" alt=""></span>
                    <input type="email" placeholder="e-mail" class="input_modal" name="email-v">
                    <span><img src="{{asset('/themes/aba/images/auitcase_modal.png')}}" alt=""></span>
                    <input type="text" placeholder="Название компании" class="input_modal" name="company">
                    <input type="submit" value="Отправить" class="submit_modal">
                </form>
            </div>
        </div>

    </div>
</div>


<script src="{{asset('/themes/aba/js/jquery-1.11.0.js')}}"></script>
<script src="{{asset('/themes/aba/lib/fancybox/jquery.fancybox.pack.js')}}"></script>
<script src="{{asset('/themes/aba/lib/plita/masonry.pkgd.min.js')}}"></script>
<script src="{{asset('/themes/aba/js/bootstrap.min.js')}}"></script>
<script src="{{asset('/themes/aba/lib/slick/slick.min.js')}}"></script>
<script src="{{asset('/themes/aba/lib/slick/jquery-migrate-1.2.1.min.js')}}"></script>
<script src="{{asset('/themes/aba/lib/mixitup/jquery.mixitup.min.js')}}"></script>
<script src="{{asset('/themes/aba/lib/magnific-popup/jquery.magnific-popup.min.js')}}"></script>

<script>
    var count = $('.title-support ').length;
    for (var k = 0; k < count; k++) {
        var text = $('.title-support a').eq(k).text();
        arr = text.split('');
        var length = arr.length;
        if (length > 70) {
            $('.title-support a').eq(k).text('');
            var string = "";
            for (var i = 0; i < 70; i++) {
                string += arr[i];
            }
            string += '...';
            $('.title-support a').eq(k).text(string);
        }
    }
</script>
<script>
    $(document).ready(function() {
        $('.popup-gallery').magnificPopup({
            delegate: 'a',
            type: 'image',
            tLoading: 'Loading image #%curr%...',
            mainClass: 'mfp-img-mobile',
            gallery: {
                enabled: true,
                navigateByImgClick: false,
                preload: [0, 1] // Will preload 0 - before current, and 1 after the current image
            }
        });
    });

$(document).ready(function() {
	$("a.grouped_elements").fancybox({
		'transitionIn'	:	'elastic',
		'transitionOut'	:	'elastic',
		'speedIn'		:	600,
		'speedOut'		:	200,
		'overlayShow'	:	false
	});

});

    $('.slider_1').slick({
        arrows: false,
        dots: true,
        autoplay: true,
        autoplaySpeed: 2000,
        slidesToShow: 5,
        slidesToScroll: 4,
        responsive: [{
            breakpoint: 992,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 3,
                infinite: true,
                dots: true
            }
        }, {
            breakpoint: 600,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 2
            }
        }, {
            breakpoint: 480,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1
            }
        }]
    });
    $('.slider_2').slick({
        arrows: true,
        dots: false,
        autoplay: true,
        autoplaySpeed: 1000,
        slidesToShow: 5,
        slidesToScroll: 5,
        adaptiveHeight: true,
        arrows: true,
        responsive: [{
            breakpoint: 992,
            settings: {
                slidesToShow: 4,
                slidesToScroll: 4,
                infinite: true,
                dots: true
            }
        }, {
            breakpoint: 767,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 3
            }
        }, {
            breakpoint: 600,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 2
            }
        }, {
            breakpoint: 480,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1
            }
        }]
    });

    $('.grid').masonry({
        itemSelector: '.grid-item',
        // columnWidth: 371,
        isFitWidth: true
    });
    $(function() {

        $('#link').mixItUp();

    });

</script>
<script>
    var black = false;
    var title = 'temp';
    var first = true;
    $('.top').on('click', function() {

        $('.top').css("background-color","#fffFFF");

        $(this).css("background-color","#02253a");
        parent_box = $(this).closest('.box');
        parent_box.siblings().find('.bottom').slideUp();
        parent_box.find('.bottom').slideToggle(100, 'swing');

        var bottom = $(this).parents('.box').find('.bottom');
        if(title == $(this).text()) {
            $(this).css("background-color","#fffFFF");
        }

        title = $(this).text();
    });

    function openNav() {
        document.getElementById("mySidenav").style.display = "block";
    }

    function closeNav() {
        document.getElementById("mySidenav").style.display = "none";
    };
</script>
<script>

    $('.slider-for').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: true,
        fade: true,
        asNavFor: '.slider-nav',
        autoplay: true
    });
    $('.slider-nav').slick({
        slidesToShow: 4,
        slidesToScroll: 1,
        asNavFor: '.slider-for',
        dots: false,
        centerMode: false,
        focusOnSelect: false,
        autoplay: true,
        focusOnSelect: true,
        arrows: true,
        responsive: [{
            breakpoint: 992,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 1,
                infinite: true
            }
        }, {
            breakpoint: 728,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 1,
                infinite: true
            }
        }
            // You can unslick at a given breakpoint now by adding:
            // settings: "unslick"
            // instead of a settings object
        ]
    });

    function openNav() {
        document.getElementById("mySidenav").style.display = "block";
    }

    function closeNav() {
        document.getElementById("mySidenav").style.display = "none";
    };
</script>
<script>
    var count = $('.info_text').length;
    for (var k = 0; k < count; k++) {
        var text = $('.info_text').eq(k).text();
        arr = text.split('');
        var length = arr.length;
        if (length > 250) {
            $('.info_text').eq(k).text('');
            var string = "";
            for (var i = 0; i < 250; i++) {
                string += arr[i];
            }
            string += '...';
            $('.info_text').eq(k).text(string);
        }
    };
</script>
<script>
    $('#number').on('keyup',function(){
        $value = $(this).val();
        $.ajax({
            type :'get',
            url : '{{URL::to('getcard')}}',
            data : {'number':$value},
            success: function(data)
            {
                $('tbody').html(data);
            }
        });
    });
    $('#fio').on('keyup',function(){
        $value = $(this).val();
        $.ajax({
            type :'get',
            url : '{{URL::to('getcard')}}',
            data : {'fio':$value},
            success: function(data)
            {
                $('tbody').html(data);
            }
        });
    });
    $('#company').on('keyup',function(){
        $value = $(this).val();
        $.ajax({
            type :'get',
            url : '{{URL::to('getcard')}}',
            data : {'company':$value},
            success: function(data)
            {
                $('tbody').html(data);
            }
        });
    });
    $('#iin').on('keyup',function(){
        $value = $(this).val();
        $.ajax({
            type :'get',
            url : '{{URL::to('getcard')}}',
            data : {'iin':$value},
            success: function(data)
            {
                $('tbody').html(data);
            }
        });
    });
</script>
{{--******************************b2b script********************************--}}
<script>
    $('#select_list').on('change',function(){
        $value = $(this).val();
        if ($value == 1){
            $('.b2b1').css('display','block');
            $('.b2b0').css('display','block');

            $('.b2b0').css('display','none');
        }else if($value == 0){
            $('.b2b1').css('display','block');
            $('.b2b0').css('display','block');

            $('.b2b1').css('display','none');
        }else{
            $('.b2b1').css('display','block');
            $('.b2b0').css('display','block');
        }
    });
</script>
{{--*****************************end b2b script******************************--}}
{{--***********************sales script*********************************--}}
<script>
    $('#value').on('change',function(){
        $value = $(this).val();
        if ($value == 1){

            $('.gr-1').css('display','block');
            $('.gr-2').css('display','block');
            $('.gr-3').css('display','block');
            $('.gr-4').css('display','block');
            $('.gr-5').css('display','block');
            $('.gr-6').css('display','block');
            $('.gr-7').css('display','block');

            $('.gr-2').css('display','none');
            $('.gr-3').css('display','none');
            $('.gr-4').css('display','none');
            $('.gr-5').css('display','none');
            $('.gr-6').css('display','none');
            $('.gr-7').css('display','none');
        }else if ($value == 2){

            $('.gr-1').css('display','block');
            $('.gr-2').css('display','block');
            $('.gr-3').css('display','block');
            $('.gr-4').css('display','block');
            $('.gr-5').css('display','block');
            $('.gr-6').css('display','block');
            $('.gr-7').css('display','block');

            $('.gr-3').css('display','none');
            $('.gr-1').css('display','none');
            $('.gr-4').css('display','none');
            $('.gr-5').css('display','none');
            $('.gr-6').css('display','none');
            $('.gr-7').css('display','none');
        }else if ($value == 3){

            $('.gr-1').css('display','block');
            $('.gr-2').css('display','block');
            $('.gr-3').css('display','block');
            $('.gr-4').css('display','block');
            $('.gr-5').css('display','block');
            $('.gr-6').css('display','block');
            $('.gr-7').css('display','block');

            $('.gr-2').css('display','none');
            $('.gr-1').css('display','none');
            $('.gr-4').css('display','none');
            $('.gr-5').css('display','none');
            $('.gr-6').css('display','none');
            $('.gr-7').css('display','none');
        }else if ($value == 4){

            $('.gr-1').css('display','block');
            $('.gr-2').css('display','block');
            $('.gr-3').css('display','block');
            $('.gr-4').css('display','block');
            $('.gr-5').css('display','block');
            $('.gr-6').css('display','block');
            $('.gr-7').css('display','block');

            $('.gr-2').css('display','none');
            $('.gr-3').css('display','none');
            $('.gr-1').css('display','none');
            $('.gr-5').css('display','none');
            $('.gr-6').css('display','none');
            $('.gr-7').css('display','none');
        }else if ($value == 5){

            $('.gr-1').css('display','block');
            $('.gr-2').css('display','block');
            $('.gr-3').css('display','block');
            $('.gr-4').css('display','block');
            $('.gr-5').css('display','block');
            $('.gr-6').css('display','block');
            $('.gr-7').css('display','block');

            $('.gr-2').css('display','none');
            $('.gr-3').css('display','none');
            $('.gr-4').css('display','none');
            $('.gr-1').css('display','none');
            $('.gr-6').css('display','none');
            $('.gr-7').css('display','none');
        }else if ($value == 6){

            $('.gr-1').css('display','block');
            $('.gr-2').css('display','block');
            $('.gr-3').css('display','block');
            $('.gr-4').css('display','block');
            $('.gr-5').css('display','block');
            $('.gr-6').css('display','block');
            $('.gr-7').css('display','block');

            $('.gr-2').css('display','none');
            $('.gr-3').css('display','none');
            $('.gr-4').css('display','none');
            $('.gr-5').css('display','none');
            $('.gr-1').css('display','none');
            $('.gr-7').css('display','none');
        }else if ($value == 7){

            $('.gr-1').css('display','block');
            $('.gr-2').css('display','block');
            $('.gr-3').css('display','block');
            $('.gr-4').css('display','block');
            $('.gr-5').css('display','block');
            $('.gr-6').css('display','block');
            $('.gr-7').css('display','block');

            $('.gr-2').css('display','none');
            $('.gr-3').css('display','none');
            $('.gr-4').css('display','none');
            $('.gr-5').css('display','none');
            $('.gr-1').css('display','none');
            $('.gr-6').css('display','none');
        } else{
            $('.gr-1').css('display','block');
            $('.gr-2').css('display','block');
            $('.gr-3').css('display','block');
            $('.gr-4').css('display','block');
            $('.gr-5').css('display','block');
            $('.gr-6').css('display','block');
            $('.gr-7').css('display','blcok');
        }
    });
</script>
{{--********************************end sales script***************************************--}}
{{--*********************************start catalog script**********************************--}}
<script>
    $('#value-cat').on('change',function() {
        $catalog = $(this).val();
        if ($catalog == 1){
            $('.cat-1').css('display','block');
            $('.cat-2').css('display','block');
            $('.cat-3').css('display','block');
            $('.cat-4').css('display','block');
            $('.cat-5').css('display','block');
            $('.cat-6').css('display','block');
            $('.cat-7').css('display','block');
            $('.cat-8').css('display','block');
            $('.cat-9').css('display','block');
            $('.cat-10').css('display','block');
            $('.cat-11').css('display','block');
            $('.cat-12').css('display','block');
            $('.cat-13').css('display','block');
            $('.cat-14').css('display','block');
            $('.cat-15').css('display','block');
            $('.cat-16').css('display','block');
            $('.cat-17').css('display','block');
            $('.cat-18').css('display','block');
            $('.cat-19').css('display','block');

            $('.cat-2').css('display','none');
            $('.cat-3').css('display','none');
            $('.cat-4').css('display','none');
            $('.cat-5').css('display','none');
            $('.cat-6').css('display','none');
            $('.cat-7').css('display','none');
            $('.cat-8').css('display','none');
            $('.cat-9').css('display','none');
            $('.cat-10').css('display','none');
            $('.cat-11').css('display','none');
            $('.cat-12').css('display','none');
            $('.cat-13').css('display','none');
            $('.cat-14').css('display','none');
            $('.cat-15').css('display','none');
            $('.cat-16').css('display','none');
            $('.cat-17').css('display','none');
            $('.cat-18').css('display','none');
            $('.cat-19').css('display','none');
        }else if ($catalog == 2){
            $('.cat-1').css('display','block');
            $('.cat-2').css('display','block');
            $('.cat-3').css('display','block');
            $('.cat-4').css('display','block');
            $('.cat-5').css('display','block');
            $('.cat-6').css('display','block');
            $('.cat-7').css('display','block');
            $('.cat-8').css('display','block');
            $('.cat-9').css('display','block');
            $('.cat-10').css('display','block');
            $('.cat-11').css('display','block');
            $('.cat-12').css('display','block');
            $('.cat-13').css('display','block');
            $('.cat-14').css('display','block');
            $('.cat-15').css('display','block');
            $('.cat-16').css('display','block');
            $('.cat-17').css('display','block');
            $('.cat-18').css('display','block');
            $('.cat-19').css('display','block');

            $('.cat-1').css('display','none');
            $('.cat-3').css('display','none');
            $('.cat-4').css('display','none');
            $('.cat-5').css('display','none');
            $('.cat-6').css('display','none');
            $('.cat-7').css('display','none');
            $('.cat-8').css('display','none');
            $('.cat-9').css('display','none');
            $('.cat-10').css('display','none');
            $('.cat-11').css('display','none');
            $('.cat-12').css('display','none');
            $('.cat-13').css('display','none');
            $('.cat-14').css('display','none');
            $('.cat-15').css('display','none');
            $('.cat-16').css('display','none');
            $('.cat-17').css('display','none');
            $('.cat-18').css('display','none');
            $('.cat-19').css('display','none');
        }else if ($catalog == 3){
            $('.cat-1').css('display','block');
            $('.cat-2').css('display','block');
            $('.cat-3').css('display','block');
            $('.cat-4').css('display','block');
            $('.cat-5').css('display','block');
            $('.cat-6').css('display','block');
            $('.cat-7').css('display','block');
            $('.cat-8').css('display','block');
            $('.cat-9').css('display','block');
            $('.cat-10').css('display','block');
            $('.cat-11').css('display','block');
            $('.cat-12').css('display','block');
            $('.cat-13').css('display','block');
            $('.cat-14').css('display','block');
            $('.cat-15').css('display','block');
            $('.cat-16').css('display','block');
            $('.cat-17').css('display','block');
            $('.cat-18').css('display','block');
            $('.cat-19').css('display','block');

            $('.cat-1').css('display','none');
            $('.cat-2').css('display','none');
            $('.cat-4').css('display','none');
            $('.cat-5').css('display','none');
            $('.cat-6').css('display','none');
            $('.cat-7').css('display','none');
            $('.cat-8').css('display','none');
            $('.cat-9').css('display','none');
            $('.cat-10').css('display','none');
            $('.cat-11').css('display','none');
            $('.cat-12').css('display','none');
            $('.cat-13').css('display','none');
            $('.cat-14').css('display','none');
            $('.cat-15').css('display','none');
            $('.cat-16').css('display','none');
            $('.cat-17').css('display','none');
            $('.cat-18').css('display','none');
            $('.cat-19').css('display','none');
        }else if ($catalog == 4){
            $('.cat-1').css('display','block');
            $('.cat-2').css('display','block');
            $('.cat-3').css('display','block');
            $('.cat-4').css('display','block');
            $('.cat-5').css('display','block');
            $('.cat-6').css('display','block');
            $('.cat-7').css('display','block');
            $('.cat-8').css('display','block');
            $('.cat-9').css('display','block');
            $('.cat-10').css('display','block');
            $('.cat-11').css('display','block');
            $('.cat-12').css('display','block');
            $('.cat-13').css('display','block');
            $('.cat-14').css('display','block');
            $('.cat-15').css('display','block');
            $('.cat-16').css('display','block');
            $('.cat-17').css('display','block');
            $('.cat-18').css('display','block');
            $('.cat-19').css('display','block');

            $('.cat-1').css('display','none');
            $('.cat-3').css('display','none');
            $('.cat-2').css('display','none');
            $('.cat-5').css('display','none');
            $('.cat-6').css('display','none');
            $('.cat-7').css('display','none');
            $('.cat-8').css('display','none');
            $('.cat-9').css('display','none');
            $('.cat-10').css('display','none');
            $('.cat-11').css('display','none');
            $('.cat-12').css('display','none');
            $('.cat-13').css('display','none');
            $('.cat-14').css('display','none');
            $('.cat-15').css('display','none');
            $('.cat-16').css('display','none');
            $('.cat-17').css('display','none');
            $('.cat-18').css('display','none');
            $('.cat-19').css('display','none');
        }else if ($catalog == 5){
            $('.cat-1').css('display','block');
            $('.cat-2').css('display','block');
            $('.cat-3').css('display','block');
            $('.cat-4').css('display','block');
            $('.cat-5').css('display','block');
            $('.cat-6').css('display','block');
            $('.cat-7').css('display','block');
            $('.cat-8').css('display','block');
            $('.cat-9').css('display','block');
            $('.cat-10').css('display','block');
            $('.cat-11').css('display','block');
            $('.cat-12').css('display','block');
            $('.cat-13').css('display','block');
            $('.cat-14').css('display','block');
            $('.cat-15').css('display','block');
            $('.cat-16').css('display','block');
            $('.cat-17').css('display','block');
            $('.cat-18').css('display','block');
            $('.cat-19').css('display','block');

            $('.cat-1').css('display','none');
            $('.cat-3').css('display','none');
            $('.cat-4').css('display','none');
            $('.cat-2').css('display','none');
            $('.cat-6').css('display','none');
            $('.cat-7').css('display','none');
            $('.cat-8').css('display','none');
            $('.cat-9').css('display','none');
            $('.cat-10').css('display','none');
            $('.cat-11').css('display','none');
            $('.cat-12').css('display','none');
            $('.cat-13').css('display','none');
            $('.cat-14').css('display','none');
            $('.cat-15').css('display','none');
            $('.cat-16').css('display','none');
            $('.cat-17').css('display','none');
            $('.cat-18').css('display','none');
            $('.cat-19').css('display','none');
        }else if ($catalog == 6){
            $('.cat-1').css('display','block');
            $('.cat-2').css('display','block');
            $('.cat-3').css('display','block');
            $('.cat-4').css('display','block');
            $('.cat-5').css('display','block');
            $('.cat-6').css('display','block');
            $('.cat-7').css('display','block');
            $('.cat-8').css('display','block');
            $('.cat-9').css('display','block');
            $('.cat-10').css('display','block');
            $('.cat-11').css('display','block');
            $('.cat-12').css('display','block');
            $('.cat-13').css('display','block');
            $('.cat-14').css('display','block');
            $('.cat-15').css('display','block');
            $('.cat-16').css('display','block');
            $('.cat-17').css('display','block');
            $('.cat-18').css('display','block');
            $('.cat-19').css('display','block');

            $('.cat-1').css('display','none');
            $('.cat-3').css('display','none');
            $('.cat-4').css('display','none');
            $('.cat-5').css('display','none');
            $('.cat-2').css('display','none');
            $('.cat-7').css('display','none');
            $('.cat-8').css('display','none');
            $('.cat-9').css('display','none');
            $('.cat-10').css('display','none');
            $('.cat-11').css('display','none');
            $('.cat-12').css('display','none');
            $('.cat-13').css('display','none');
            $('.cat-14').css('display','none');
            $('.cat-15').css('display','none');
            $('.cat-16').css('display','none');
            $('.cat-17').css('display','none');
            $('.cat-18').css('display','none');
            $('.cat-19').css('display','none');
        }else if ($catalog == 7){
            $('.cat-1').css('display','block');
            $('.cat-2').css('display','block');
            $('.cat-3').css('display','block');
            $('.cat-4').css('display','block');
            $('.cat-5').css('display','block');
            $('.cat-6').css('display','block');
            $('.cat-7').css('display','block');
            $('.cat-8').css('display','block');
            $('.cat-9').css('display','block');
            $('.cat-10').css('display','block');
            $('.cat-11').css('display','block');
            $('.cat-12').css('display','block');
            $('.cat-13').css('display','block');
            $('.cat-14').css('display','block');
            $('.cat-15').css('display','block');
            $('.cat-16').css('display','block');
            $('.cat-17').css('display','block');
            $('.cat-18').css('display','block');
            $('.cat-19').css('display','block');

            $('.cat-1').css('display','none');
            $('.cat-3').css('display','none');
            $('.cat-4').css('display','none');
            $('.cat-5').css('display','none');
            $('.cat-2').css('display','none');
            $('.cat-6').css('display','none');
            $('.cat-8').css('display','none');
            $('.cat-9').css('display','none');
            $('.cat-10').css('display','none');
            $('.cat-11').css('display','none');
            $('.cat-12').css('display','none');
            $('.cat-13').css('display','none');
            $('.cat-14').css('display','none');
            $('.cat-15').css('display','none');
            $('.cat-16').css('display','none');
            $('.cat-17').css('display','none');
            $('.cat-18').css('display','none');
            $('.cat-19').css('display','none');
        }else if ($catalog == 8){
            $('.cat-1').css('display','block');
            $('.cat-2').css('display','block');
            $('.cat-3').css('display','block');
            $('.cat-4').css('display','block');
            $('.cat-5').css('display','block');
            $('.cat-6').css('display','block');
            $('.cat-7').css('display','block');
            $('.cat-8').css('display','block');
            $('.cat-9').css('display','block');
            $('.cat-10').css('display','block');
            $('.cat-11').css('display','block');
            $('.cat-12').css('display','block');
            $('.cat-13').css('display','block');
            $('.cat-14').css('display','block');
            $('.cat-15').css('display','block');
            $('.cat-16').css('display','block');
            $('.cat-17').css('display','block');
            $('.cat-18').css('display','block');
            $('.cat-19').css('display','block');

            $('.cat-1').css('display','none');
            $('.cat-3').css('display','none');
            $('.cat-4').css('display','none');
            $('.cat-5').css('display','none');
            $('.cat-6').css('display','none');
            $('.cat-7').css('display','none');
            $('.cat-2').css('display','none');
            $('.cat-9').css('display','none');
            $('.cat-10').css('display','none');
            $('.cat-11').css('display','none');
            $('.cat-12').css('display','none');
            $('.cat-13').css('display','none');
            $('.cat-14').css('display','none');
            $('.cat-15').css('display','none');
            $('.cat-16').css('display','none');
            $('.cat-17').css('display','none');
            $('.cat-18').css('display','none');
            $('.cat-19').css('display','none');
        }else if ($catalog == 9){
            $('.cat-1').css('display','block');
            $('.cat-2').css('display','block');
            $('.cat-3').css('display','block');
            $('.cat-4').css('display','block');
            $('.cat-5').css('display','block');
            $('.cat-6').css('display','block');
            $('.cat-7').css('display','block');
            $('.cat-8').css('display','block');
            $('.cat-9').css('display','block');
            $('.cat-10').css('display','block');
            $('.cat-11').css('display','block');
            $('.cat-12').css('display','block');
            $('.cat-13').css('display','block');
            $('.cat-14').css('display','block');
            $('.cat-15').css('display','block');
            $('.cat-16').css('display','block');
            $('.cat-17').css('display','block');
            $('.cat-18').css('display','block');
            $('.cat-19').css('display','block');

            $('.cat-1').css('display','none');
            $('.cat-3').css('display','none');
            $('.cat-4').css('display','none');
            $('.cat-5').css('display','none');
            $('.cat-6').css('display','none');
            $('.cat-7').css('display','none');
            $('.cat-8').css('display','none');
            $('.cat-2').css('display','none');
            $('.cat-10').css('display','none');
            $('.cat-11').css('display','none');
            $('.cat-12').css('display','none');
            $('.cat-13').css('display','none');
            $('.cat-14').css('display','none');
            $('.cat-15').css('display','none');
            $('.cat-16').css('display','none');
            $('.cat-17').css('display','none');
            $('.cat-18').css('display','none');
            $('.cat-19').css('display','none');
        }else if ($catalog == 10){
            $('.cat-1').css('display','block');
            $('.cat-2').css('display','block');
            $('.cat-3').css('display','block');
            $('.cat-4').css('display','block');
            $('.cat-5').css('display','block');
            $('.cat-6').css('display','block');
            $('.cat-7').css('display','block');
            $('.cat-8').css('display','block');
            $('.cat-9').css('display','block');
            $('.cat-10').css('display','block');
            $('.cat-11').css('display','block');
            $('.cat-12').css('display','block');
            $('.cat-13').css('display','block');
            $('.cat-14').css('display','block');
            $('.cat-15').css('display','block');
            $('.cat-16').css('display','block');
            $('.cat-17').css('display','block');
            $('.cat-18').css('display','block');
            $('.cat-19').css('display','block');

            $('.cat-1').css('display','none');
            $('.cat-3').css('display','none');
            $('.cat-4').css('display','none');
            $('.cat-5').css('display','none');
            $('.cat-6').css('display','none');
            $('.cat-7').css('display','none');
            $('.cat-8').css('display','none');
            $('.cat-9').css('display','none');
            $('.cat-2').css('display','none');
            $('.cat-11').css('display','none');
            $('.cat-12').css('display','none');
            $('.cat-13').css('display','none');
            $('.cat-14').css('display','none');
            $('.cat-15').css('display','none');
            $('.cat-16').css('display','none');
            $('.cat-17').css('display','none');
            $('.cat-18').css('display','none');
            $('.cat-19').css('display','none');
        }else if ($catalog == 11){
            $('.cat-1').css('display','block');
            $('.cat-2').css('display','block');
            $('.cat-3').css('display','block');
            $('.cat-4').css('display','block');
            $('.cat-5').css('display','block');
            $('.cat-6').css('display','block');
            $('.cat-7').css('display','block');
            $('.cat-8').css('display','block');
            $('.cat-9').css('display','block');
            $('.cat-10').css('display','block');
            $('.cat-11').css('display','block');
            $('.cat-12').css('display','block');
            $('.cat-13').css('display','block');
            $('.cat-14').css('display','block');
            $('.cat-15').css('display','block');
            $('.cat-16').css('display','block');
            $('.cat-17').css('display','block');
            $('.cat-18').css('display','block');
            $('.cat-19').css('display','block');

            $('.cat-1').css('display','none');
            $('.cat-3').css('display','none');
            $('.cat-4').css('display','none');
            $('.cat-5').css('display','none');
            $('.cat-6').css('display','none');
            $('.cat-7').css('display','none');
            $('.cat-8').css('display','none');
            $('.cat-9').css('display','none');
            $('.cat-10').css('display','none');
            $('.cat-2').css('display','none');
            $('.cat-12').css('display','none');
            $('.cat-13').css('display','none');
            $('.cat-14').css('display','none');
            $('.cat-15').css('display','none');
            $('.cat-16').css('display','none');
            $('.cat-17').css('display','none');
            $('.cat-18').css('display','none');
            $('.cat-19').css('display','none');
        }else if ($catalog == 12){
            $('.cat-1').css('display','block');
            $('.cat-2').css('display','block');
            $('.cat-3').css('display','block');
            $('.cat-4').css('display','block');
            $('.cat-5').css('display','block');
            $('.cat-6').css('display','block');
            $('.cat-7').css('display','block');
            $('.cat-8').css('display','block');
            $('.cat-9').css('display','block');
            $('.cat-10').css('display','block');
            $('.cat-11').css('display','block');
            $('.cat-12').css('display','block');
            $('.cat-13').css('display','block');
            $('.cat-14').css('display','block');
            $('.cat-15').css('display','block');
            $('.cat-16').css('display','block');
            $('.cat-17').css('display','block');
            $('.cat-18').css('display','block');
            $('.cat-19').css('display','block');

            $('.cat-1').css('display','none');
            $('.cat-3').css('display','none');
            $('.cat-4').css('display','none');
            $('.cat-5').css('display','none');
            $('.cat-6').css('display','none');
            $('.cat-7').css('display','none');
            $('.cat-8').css('display','none');
            $('.cat-9').css('display','none');
            $('.cat-10').css('display','none');
            $('.cat-11').css('display','none');
            $('.cat-2').css('display','none');
            $('.cat-13').css('display','none');
            $('.cat-14').css('display','none');
            $('.cat-15').css('display','none');
            $('.cat-16').css('display','none');
            $('.cat-17').css('display','none');
            $('.cat-18').css('display','none');
            $('.cat-19').css('display','none');
        }else if ($catalog == 13){
            $('.cat-1').css('display','block');
            $('.cat-2').css('display','block');
            $('.cat-3').css('display','block');
            $('.cat-4').css('display','block');
            $('.cat-5').css('display','block');
            $('.cat-6').css('display','block');
            $('.cat-7').css('display','block');
            $('.cat-8').css('display','block');
            $('.cat-9').css('display','block');
            $('.cat-10').css('display','block');
            $('.cat-11').css('display','block');
            $('.cat-12').css('display','block');
            $('.cat-13').css('display','block');
            $('.cat-14').css('display','block');
            $('.cat-15').css('display','block');
            $('.cat-16').css('display','block');
            $('.cat-17').css('display','block');
            $('.cat-18').css('display','block');
            $('.cat-19').css('display','block');

            $('.cat-1').css('display','none');
            $('.cat-3').css('display','none');
            $('.cat-4').css('display','none');
            $('.cat-5').css('display','none');
            $('.cat-6').css('display','none');
            $('.cat-7').css('display','none');
            $('.cat-8').css('display','none');
            $('.cat-9').css('display','none');
            $('.cat-10').css('display','none');
            $('.cat-11').css('display','none');
            $('.cat-12').css('display','none');
            $('.cat-2').css('display','none');
            $('.cat-14').css('display','none');
            $('.cat-15').css('display','none');
            $('.cat-16').css('display','none');
            $('.cat-17').css('display','none');
            $('.cat-18').css('display','none');
            $('.cat-19').css('display','none');
        }else if ($catalog == 14){
            $('.cat-1').css('display','block');
            $('.cat-2').css('display','block');
            $('.cat-3').css('display','block');
            $('.cat-4').css('display','block');
            $('.cat-5').css('display','block');
            $('.cat-6').css('display','block');
            $('.cat-7').css('display','block');
            $('.cat-8').css('display','block');
            $('.cat-9').css('display','block');
            $('.cat-10').css('display','block');
            $('.cat-11').css('display','block');
            $('.cat-12').css('display','block');
            $('.cat-13').css('display','block');
            $('.cat-14').css('display','block');
            $('.cat-15').css('display','block');
            $('.cat-16').css('display','block');
            $('.cat-17').css('display','block');
            $('.cat-18').css('display','block');
            $('.cat-19').css('display','block');

            $('.cat-1').css('display','none');
            $('.cat-3').css('display','none');
            $('.cat-4').css('display','none');
            $('.cat-5').css('display','none');
            $('.cat-6').css('display','none');
            $('.cat-7').css('display','none');
            $('.cat-8').css('display','none');
            $('.cat-9').css('display','none');
            $('.cat-10').css('display','none');
            $('.cat-11').css('display','none');
            $('.cat-12').css('display','none');
            $('.cat-13').css('display','none');
            $('.cat-2').css('display','none');
            $('.cat-15').css('display','none');
            $('.cat-16').css('display','none');
            $('.cat-17').css('display','none');
            $('.cat-18').css('display','none');
            $('.cat-19').css('display','none');
        }else if ($catalog == 15){
            $('.cat-1').css('display','block');
            $('.cat-2').css('display','block');
            $('.cat-3').css('display','block');
            $('.cat-4').css('display','block');
            $('.cat-5').css('display','block');
            $('.cat-6').css('display','block');
            $('.cat-7').css('display','block');
            $('.cat-8').css('display','block');
            $('.cat-9').css('display','block');
            $('.cat-10').css('display','block');
            $('.cat-11').css('display','block');
            $('.cat-12').css('display','block');
            $('.cat-13').css('display','block');
            $('.cat-14').css('display','block');
            $('.cat-15').css('display','block');
            $('.cat-16').css('display','block');
            $('.cat-17').css('display','block');
            $('.cat-18').css('display','block');
            $('.cat-19').css('display','block');

            $('.cat-1').css('display','none');
            $('.cat-3').css('display','none');
            $('.cat-4').css('display','none');
            $('.cat-5').css('display','none');
            $('.cat-6').css('display','none');
            $('.cat-7').css('display','none');
            $('.cat-8').css('display','none');
            $('.cat-9').css('display','none');
            $('.cat-10').css('display','none');
            $('.cat-11').css('display','none');
            $('.cat-12').css('display','none');
            $('.cat-13').css('display','none');
            $('.cat-14').css('display','none');
            $('.cat-2').css('display','none');
            $('.cat-16').css('display','none');
            $('.cat-17').css('display','none');
            $('.cat-18').css('display','none');
            $('.cat-19').css('display','none');
        }else if ($catalog == 16){
            $('.cat-1').css('display','block');
            $('.cat-2').css('display','block');
            $('.cat-3').css('display','block');
            $('.cat-4').css('display','block');
            $('.cat-5').css('display','block');
            $('.cat-6').css('display','block');
            $('.cat-7').css('display','block');
            $('.cat-8').css('display','block');
            $('.cat-9').css('display','block');
            $('.cat-10').css('display','block');
            $('.cat-11').css('display','block');
            $('.cat-12').css('display','block');
            $('.cat-13').css('display','block');
            $('.cat-14').css('display','block');
            $('.cat-15').css('display','block');
            $('.cat-16').css('display','block');
            $('.cat-17').css('display','block');
            $('.cat-18').css('display','block');
            $('.cat-19').css('display','block');

            $('.cat-1').css('display','none');
            $('.cat-3').css('display','none');
            $('.cat-4').css('display','none');
            $('.cat-5').css('display','none');
            $('.cat-6').css('display','none');
            $('.cat-7').css('display','none');
            $('.cat-8').css('display','none');
            $('.cat-9').css('display','none');
            $('.cat-10').css('display','none');
            $('.cat-11').css('display','none');
            $('.cat-12').css('display','none');
            $('.cat-13').css('display','none');
            $('.cat-14').css('display','none');
            $('.cat-15').css('display','none');
            $('.cat-2').css('display','none');
            $('.cat-17').css('display','none');
            $('.cat-18').css('display','none');
            $('.cat-19').css('display','none');
        }else if ($catalog == 17){
            $('.cat-1').css('display','block');
            $('.cat-2').css('display','block');
            $('.cat-3').css('display','block');
            $('.cat-4').css('display','block');
            $('.cat-5').css('display','block');
            $('.cat-6').css('display','block');
            $('.cat-7').css('display','block');
            $('.cat-8').css('display','block');
            $('.cat-9').css('display','block');
            $('.cat-10').css('display','block');
            $('.cat-11').css('display','block');
            $('.cat-12').css('display','block');
            $('.cat-13').css('display','block');
            $('.cat-14').css('display','block');
            $('.cat-15').css('display','block');
            $('.cat-16').css('display','block');
            $('.cat-17').css('display','block');
            $('.cat-18').css('display','block');
            $('.cat-19').css('display','block');

            $('.cat-1').css('display','none');
            $('.cat-3').css('display','none');
            $('.cat-4').css('display','none');
            $('.cat-5').css('display','none');
            $('.cat-6').css('display','none');
            $('.cat-7').css('display','none');
            $('.cat-8').css('display','none');
            $('.cat-9').css('display','none');
            $('.cat-10').css('display','none');
            $('.cat-11').css('display','none');
            $('.cat-12').css('display','none');
            $('.cat-13').css('display','none');
            $('.cat-14').css('display','none');
            $('.cat-15').css('display','none');
            $('.cat-16').css('display','none');
            $('.cat-2').css('display','none');
            $('.cat-18').css('display','none');
            $('.cat-19').css('display','none');
        }else if ($catalog == 18){
            $('.cat-1').css('display','block');
            $('.cat-2').css('display','block');
            $('.cat-3').css('display','block');
            $('.cat-4').css('display','block');
            $('.cat-5').css('display','block');
            $('.cat-6').css('display','block');
            $('.cat-7').css('display','block');
            $('.cat-8').css('display','block');
            $('.cat-9').css('display','block');
            $('.cat-10').css('display','block');
            $('.cat-11').css('display','block');
            $('.cat-12').css('display','block');
            $('.cat-13').css('display','block');
            $('.cat-14').css('display','block');
            $('.cat-15').css('display','block');
            $('.cat-16').css('display','block');
            $('.cat-17').css('display','block');
            $('.cat-18').css('display','block');
            $('.cat-19').css('display','block');

            $('.cat-1').css('display','none');
            $('.cat-3').css('display','none');
            $('.cat-4').css('display','none');
            $('.cat-5').css('display','none');
            $('.cat-6').css('display','none');
            $('.cat-7').css('display','none');
            $('.cat-8').css('display','none');
            $('.cat-9').css('display','none');
            $('.cat-10').css('display','none');
            $('.cat-11').css('display','none');
            $('.cat-12').css('display','none');
            $('.cat-13').css('display','none');
            $('.cat-14').css('display','none');
            $('.cat-15').css('display','none');
            $('.cat-16').css('display','none');
            $('.cat-17').css('display','none');
            $('.cat-2').css('display','none');
            $('.cat-19').css('display','none');
        }else if ($catalog == 19){
            $('.cat-1').css('display','block');
            $('.cat-2').css('display','block');
            $('.cat-3').css('display','block');
            $('.cat-4').css('display','block');
            $('.cat-5').css('display','block');
            $('.cat-6').css('display','block');
            $('.cat-7').css('display','block');
            $('.cat-8').css('display','block');
            $('.cat-9').css('display','block');
            $('.cat-10').css('display','block');
            $('.cat-11').css('display','block');
            $('.cat-12').css('display','block');
            $('.cat-13').css('display','block');
            $('.cat-14').css('display','block');
            $('.cat-15').css('display','block');
            $('.cat-16').css('display','block');
            $('.cat-17').css('display','block');
            $('.cat-18').css('display','block');
            $('.cat-19').css('display','block');

            $('.cat-1').css('display','none');
            $('.cat-3').css('display','none');
            $('.cat-4').css('display','none');
            $('.cat-5').css('display','none');
            $('.cat-6').css('display','none');
            $('.cat-7').css('display','none');
            $('.cat-8').css('display','none');
            $('.cat-9').css('display','none');
            $('.cat-10').css('display','none');
            $('.cat-11').css('display','none');
            $('.cat-12').css('display','none');
            $('.cat-13').css('display','none');
            $('.cat-14').css('display','none');
            $('.cat-15').css('display','none');
            $('.cat-16').css('display','none');
            $('.cat-17').css('display','none');
            $('.cat-18').css('display','none');
            $('.cat-2').css('display','none');
        }else{
            $('.cat-1').css('display','block');
            $('.cat-2').css('display','block');
            $('.cat-3').css('display','block');
            $('.cat-4').css('display','block');
            $('.cat-5').css('display','block');
            $('.cat-6').css('display','block');
            $('.cat-7').css('display','block');
            $('.cat-8').css('display','block');
            $('.cat-9').css('display','block');
            $('.cat-10').css('display','block');
            $('.cat-11').css('display','block');
            $('.cat-12').css('display','block');
            $('.cat-13').css('display','block');
            $('.cat-14').css('display','block');
            $('.cat-15').css('display','block');
            $('.cat-16').css('display','block');
            $('.cat-17').css('display','block');
            $('.cat-18').css('display','block');
            $('.cat-19').css('display','block');
        }

    });
</script>
{{--************************** end catalog script******************************************--}}
{{--*********************************start rezume script************************************--}}
<script>
    $('#rezume').on('change',function(){
        $rezume = $(this).val();
        if ($rezume == 1){
            $('.rez-1').css('display','block');
            $('.rez-2').css('display','block');
            $('.rez-3').css('display','block');
            $('.rez-4').css('display','block');
            $('.rez-5').css('display','block');
            $('.rez-6').css('display','block');
            $('.rez-7').css('display','block');
            $('.rez-8').css('display','block');
            $('.rez-9').css('display','block');

            $('.rez-2').css('display','none');
            $('.rez-3').css('display','none');
            $('.rez-4').css('display','none');
            $('.rez-5').css('display','none');
            $('.rez-6').css('display','none');
            $('.rez-7').css('display','none');
            $('.rez-8').css('display','none');
            $('.rez-9').css('display','none');
        }else if ($rezume == 2){

            $('.rez-1').css('display','block');
            $('.rez-2').css('display','block');
            $('.rez-3').css('display','block');
            $('.rez-4').css('display','block');
            $('.rez-5').css('display','block');
            $('.rez-6').css('display','block');
            $('.rez-7').css('display','block');
            $('.rez-8').css('display','block');
            $('.rez-9').css('display','block');

            $('.rez-3').css('display','none');
            $('.rez-1').css('display','none');
            $('.rez-4').css('display','none');
            $('.rez-5').css('display','none');
            $('.rez-6').css('display','none');
            $('.rez-7').css('display','none');
            $('.rez-8').css('display','none');
            $('.rez-9').css('display','none');
        }else if ($rezume == 3){

            $('.rez-1').css('display','block');
            $('.rez-2').css('display','block');
            $('.rez-3').css('display','block');
            $('.rez-4').css('display','block');
            $('.rez-5').css('display','block');
            $('.rez-6').css('display','block');
            $('.rez-7').css('display','block');
            $('.rez-8').css('display','block');
            $('.rez-9').css('display','block');

            $('.rez-2').css('display','none');
            $('.rez-1').css('display','none');
            $('.rez-4').css('display','none');
            $('.rez-5').css('display','none');
            $('.rez-6').css('display','none');
            $('.rez-7').css('display','none');
            $('.rez-8').css('display','none');
            $('.rez-9').css('display','none');
        }else if ($rezume == 4){

            $('.rez-1').css('display','block');
            $('.rez-2').css('display','block');
            $('.rez-3').css('display','block');
            $('.rez-4').css('display','block');
            $('.rez-5').css('display','block');
            $('.rez-6').css('display','block');
            $('.rez-7').css('display','block');
            $('.rez-8').css('display','block');
            $('.rez-9').css('display','block');

            $('.rez-2').css('display','none');
            $('.rez-3').css('display','none');
            $('.rez-1').css('display','none');
            $('.rez-5').css('display','none');
            $('.rez-6').css('display','none');
            $('.rez-7').css('display','none');
            $('.rez-8').css('display','none');
            $('.rez-9').css('display','none');
        }else if ($rezume == 5){

            $('.rez-1').css('display','block');
            $('.rez-2').css('display','block');
            $('.rez-3').css('display','block');
            $('.rez-4').css('display','block');
            $('.rez-5').css('display','block');
            $('.rez-6').css('display','block');
            $('.rez-7').css('display','block');
            $('.rez-8').css('display','block');
            $('.rez-9').css('display','block');

            $('.rez-2').css('display','none');
            $('.rez-3').css('display','none');
            $('.rez-4').css('display','none');
            $('.rez-1').css('display','none');
            $('.rez-6').css('display','none');
            $('.rez-7').css('display','none');
            $('.rez-8').css('display','none');
            $('.rez-9').css('display','none');
        }else if ($rezume == 6){

            $('.rez-1').css('display','block');
            $('.rez-2').css('display','block');
            $('.rez-3').css('display','block');
            $('.rez-4').css('display','block');
            $('.rez-5').css('display','block');
            $('.rez-6').css('display','block');
            $('.rez-7').css('display','block');
            $('.rez-8').css('display','block');
            $('.rez-9').css('display','block');

            $('.rez-2').css('display','none');
            $('.rez-3').css('display','none');
            $('.rez-4').css('display','none');
            $('.rez-5').css('display','none');
            $('.rez-1').css('display','none');
            $('.rez-7').css('display','none');
            $('.rez-8').css('display','none');
            $('.rez-9').css('display','none');
        }else if ($rezume == 7){

            $('.rez-1').css('display','block');
            $('.rez-2').css('display','block');
            $('.rez-3').css('display','block');
            $('.rez-4').css('display','block');
            $('.rez-5').css('display','block');
            $('.rez-6').css('display','block');
            $('.rez-7').css('display','block');
            $('.rez-8').css('display','block');
            $('.rez-9').css('display','block');

            $('.rez-2').css('display','none');
            $('.rez-3').css('display','none');
            $('.rez-4').css('display','none');
            $('.rez-5').css('display','none');
            $('.rez-1').css('display','none');
            $('.rez-6').css('display','none');
            $('.rez-8').css('display','none');
            $('.rez-9').css('display','none');
        }else if ($rezume == 8){

            $('.rez-1').css('display','block');
            $('.rez-2').css('display','block');
            $('.rez-3').css('display','block');
            $('.rez-4').css('display','block');
            $('.rez-5').css('display','block');
            $('.rez-6').css('display','block');
            $('.rez-7').css('display','block');
            $('.rez-8').css('display','block');
            $('.rez-9').css('display','block');

            $('.rez-2').css('display','none');
            $('.rez-3').css('display','none');
            $('.rez-4').css('display','none');
            $('.rez-5').css('display','none');
            $('.rez-1').css('display','none');
            $('.rez-6').css('display','none');
            $('.rez-7').css('display','none');
            $('.rez-9').css('display','none');
        }  else if ($rezume == 9){

            $('.rez-1').css('display','block');
            $('.rez-2').css('display','block');
            $('.rez-3').css('display','block');
            $('.rez-4').css('display','block');
            $('.rez-5').css('display','block');
            $('.rez-6').css('display','block');
            $('.rez-7').css('display','block');
            $('.rez-8').css('display','block');
            $('.rez-9').css('display','block');

            $('.rez-2').css('display','none');
            $('.rez-3').css('display','none');
            $('.rez-4').css('display','none');
            $('.rez-5').css('display','none');
            $('.rez-1').css('display','none');
            $('.rez-6').css('display','none');
            $('.rez-8').css('display','none');
            $('.rez-7').css('display','none');
        } else{
            $('.rez-1').css('display','block');
            $('.rez-2').css('display','block');
            $('.rez-3').css('display','block');
            $('.rez-4').css('display','block');
            $('.rez-5').css('display','block');
            $('.rez-6').css('display','block');
            $('.rez-7').css('display','block');
            $('.rez-8').css('display','block');
            $('.rez-9').css('display','block');
        }
    });
</script>
{{--*****************************************end rezume script******************************--}}
</body>

</html>