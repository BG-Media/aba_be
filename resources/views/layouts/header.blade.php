<div class="container-fluid">
    <div class="row menu">

        <div class="col-md-2 col-sm-3 col-xs-4">
            <a href="{{asset('/')}}"><img src="{{asset('/themes/aba/images/logo.png')}}" class="img-responsive logo" alt=""></a>
        </div>
        <div class="col-md-5 col-sm-4 hidden-xs">
            <div class="icon_mob text-center">
                <a href="http://www.kazka.kz/" target="_blank"><img src="{{asset('/themes/aba/images/kazkka.PNG')}}" class="img-responsive " alt="" ></a>
                {{--<p>СКАЧАЙТЕ ПРИЛОЖЕНИЕ</p>--}}
                {{--<a href=""><img src="{{asset('/themes/aba/images/app.png')}}" alt=""></a>--}}
                {{--<a href=""><img src="{{asset('/themes/aba/images/google_play.png')}}" alt=""></a>--}}
            </div>
        </div>
        <div class=" col-md-5 col-sm-5 col-xs-8">
            <div class="right_block">

                <div class="block1 block11">
                    <a href="#" data-toggle="modal" data-target="#myModal">
                        <img src="{{asset('/themes/aba/images/icon_masseg.png')}}" class="center-block icon-1" alt="">
                        <p class="text_links">Подписаться на новости</p>
                    </a>
                </div>

                <div class="block1 block11">
                    <a href="#" data-toggle="modal" data-target="#myModal2">
                        <img src="{{asset('/themes/aba/images/icon_user.png')}}" class="center-block icon-2" alt="">
                        <p class="text_links">Вступить в ассоциацию</p>
                    </a>
                </div>

                <div class="block1">
                    <a href="#">
                        <img src="{{asset('/themes/aba/images/icon_menu.png')}}" class="center-block" onclick="openNav()" alt="">
                    </a>
                    <p style="color: #fff; padding-top: 5px;">Меню</p>
                </div>

            </div>
        </div>
        <div class="col-xs-12 hidden-md hidden-lg hidden-sm">
            <div class="icon_mob text-center">
                {{--<p>СКАЧАЙТЕ ПРИЛОЖЕНИЕ</p>--}}
                {{--<a href=""><img src="{{asset('/themes/aba/images/app.png')}}" alt=""></a>--}}
                {{--<a href=""><img src="{{asset('/themes/aba/images/google_play.png')}}" alt=""></a>--}}
            </div>
        </div>

    </div>
</div>