
<?php
$items = Helper::getCategoryNews(2,3);
$items1 = Helper::getCategoryNews1(2,3);
?>
{{--{{dd($items1)}}--}}
{{--{{dd($items)}}--}}
<h1 class="title_1">Календарь мероприятий </h1>

<div class="buttons1 nav nav-tabs">
    <span class="tab_buttons1"><a  href="#a" data-toggle="tab">Прошедшие</a></span>
    <span class="tab_buttons2"><a class="active" href="#b" data-toggle="tab">Предстоящие</a></span>
</div>
<div class="row tab-content">
    <div class="tab-pane active" id="b">




        <?php
        $month = [
                "01"=>"Январь",
                "02"=>"Февраль",
                "03"=>"Март",
                "04"=>"Апрель",
                "05"=>"Май",
                "06"=>"Июнь",
                "07"=>"Июль",
                "08"=>"Август",
                "09"=>"Сентябрь",
                "10"=>"Октябрь",
                "11"=>"Ноябрь",
                "12"=>"Декабрь"
        ];
        ?>
        @foreach($items1 as $item1)
            @if(date('Y-m-d H:i:s') < $item1->created_at)
                <div class="col-md-4 col-sm-4">
                    <div class="block_member">
                        <div class="img_member">
                            <a href="{{url('post',$item1->id)}}"><img src="{{$item1->source}}" class="img-responsive" alt=""></a>
                        </div>
                        <div class="text_member">
                            <h4 class="text_date">{{$month[date('m', strtotime($item1->created_at))] . date(' d, Y', strtotime($item1->created_at))}}<br><span>{{$item1->slug}}</span></h4>
                            <p class="info_text">{{$item1->title}}</p>

                            <a href="{{url('post',$item1->id)}}">
                                <button type="button" class="more_info">подробнее</button>
                            </a>
                            <span class="it_was will_be">Будет</span>
                        </div>
                    </div>
                </div>
            @endif
        @endforeach
    </div>

    <div class="tab-pane" id="a">
        @foreach($items as $item)
            @if(date('Y-m-d H:i:s')>$item->created_at)
                <div class="col-md-4 col-sm-4">
                    <div class="block_member">
                        <div class="img_member">
                            <a href="{{url('post',$item->id)}}"><img src="{{$item->source}}" class="img-responsive" alt=""></a>
                        </div>
                        <div class="text_member">
                            <h4 class="text_date">{{$month[date('m', strtotime($item->created_at))] . date(' d, Y', strtotime($item->created_at))}}<br><span>{{$item->slug}}</span></h4>
                            <p class="info_text">{{$item->title}}</p>

                            <a href="{{url('post',$item->id)}}">
                                <button type="button" class="more_info">подробнее</button>
                            </a>
                            <span class="it_was">Было</span>
                        </div>
                    </div>
                </div>
            @endif
        @endforeach

    </div>

</div>