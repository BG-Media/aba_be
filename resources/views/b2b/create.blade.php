{{--{{dd($post)}}--}}
{{--{{dd($pictures[0]->source)}}--}}
@extends('layouts.app')
@section('title')
    B2B
@endsection
@section('content')
    {{--@if (count($errors) > 0)--}}
        {{--<div class="alert alert-danger">--}}
            {{--<ul>--}}
                {{--@foreach ($errors->all() as $error)--}}
                    {{--<li>{{ $error }}</li>--}}
                {{--@endforeach--}}
            {{--</ul>--}}
        {{--</div>--}}
    {{--@endif--}}
        <div class="col-md-11 col-sm-11 col-xs-12">
            <h1 class="list_title">B2B</h1>

            <ol class="breadcrumb">
                <li><a href="{{url('/')}}">Главная</a></li>
                <li><a href="#">B2B</a></li>
                <li class="active"> Добавить объявление</li>
            </ol>
        </div>

        <div class="col-md-12 col-sm-12 col-xs-12">
            <h3 class="title_tab clearfix"> Добавить объявление</h3>
            <div class="clearfix">
                <div class="add-advert">
                    <form action="{{ url('b2b') }}" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <input type="text" placeholder="Название вашей организации" name="title">
                        @if ($errors->has('title')) <p class="help-block" style="color: red">Поле оргиназации обязательно для заполнения.</p> @endif
                        <input type="text" placeholder="Электронный адрес" name="email">
                        @if ($errors->has('email')) <p class="help-block" style="color: red">Поле электронный адрес обязательно для заполнения.</p> @endif
                        <input type="text" placeholder="Номер телефона" name="phone">
                        @if ($errors->has('phone')) <p class="help-block" style="color: red">Поле номер телефона обязательно для заполнения.</p> @endif
                        <select name="type" id="">
                            <option value="0">Купи/Продай</option>
                            <option value="1">Поиск партнеров</option>
                        </select>
                        <input type="text" placeholder="Заголовок объявления" name="title_content" >
                        @if ($errors->has('title_content')) <p class="help-block" style="color: red">Поле заголовок объявления обязательно для заполнения.</p> @endif
                        <textarea id="" rows="10" placeholder="Объявление полностью" name="description"></textarea>
                        @if ($errors->has('description')) <p class="help-block" style="color: red">Поле  объявления обязательно для заполнения.</p> @endif
                        <div>
                        <div class="add-file">
                            Добавить логотип
                            <input type="file" value="Добавить логотип" name="image_1">
                        </div>
                        <div class="add-file">
                            Добавить файл
                            <input type="file" value="Добавить файл" name="file">
                        </div>
                            </div>
                        @if ($errors->has('file')) <p class="help-block" style="color: red">Поле  файл обязательно для заполнения.</p> @endif
                        @if ($errors->has('image_2')) <p class="help-block" style="color: red">Поле  фото обязательно для заполнения.</p> @endif
                        @if ($errors->has('image_1')) <p class="help-block" style="color: red">Поле  логотип обязательно для заполнения.</p> @endif
                        <input type="submit" value="Опубликовать">
                    </form>
                </div>
            </div>
        </div>
@endsection