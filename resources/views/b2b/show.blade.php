{{--{{dd($content)->get()}}--}}
@extends('layouts.app')
@section('title')
    B2B
@endsection
@section('content')
        <div class="col-md-11 col-sm-11" itemscope ItemType = "http://aba.kz/b2b/show">
            <h1 class="list_title">B2B</h1>

            <ol class="breadcrumb">
                <li><a href="{{url('/')}}">Главная</a></li>
                <li class="active">B2B</li>
            </ol>
        </div>

        <div class="col-md-12 col-sm-12 col-xs-12">
            <a href="{{url('/b2b/create')}}"><button class="add_advert">+Добавить объявление</button></a>
            <h3 class="title_tab">Объявления</h3>

            <select name="" id="select_list" class="select_list">
                <option value="2">Все объявления</option>
                <option value="0">Купи / продай</option>
                <option value="1">Поиск партнеров</option>
            </select>

            <div class="row">
            @foreach($content as $item)
                @if($item->is_admin == 1)
                @if($item->type == 0)
                    <div class="col-md-3 col-sm-4 col-xs-6 blocks_mambers b2b{{$item->type}}">
                        <a href="" data-toggle="modal" data-target="#Modal{{$item->id}}">
                            <div class="img_suuport">
                                <img src="{{$item->image_1}}" class="img-responsive" alt="">
                                <span class="sale" style="width:135px;">Купи/Продай</span>
                            </div>
                        </a>
                        <div class="link_support">
                            <p><a href="" data-toggle="modal" data-target="#Modal{{$item->id}}">{{$item->title}}</a></p>
                        </div>
                    </div>
                    <div class="modal fade" id="Modal{{$item->id}}" role="dialog">
                        <div class="content-modal modal-dialog">

                            <!-- Modal content-->
                            <div class="modal-content" style="background-image:url(/public/themes/aba/images/red1.png);">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                </div>
                                <div class="modal-body clearfix">
                                    <h3>{{$item->title_content}}</h3>

                                    <div class="content-b2b">
                                        <p>{{$item->description}}</p>
                                        {{--<p>Создание впервые в Казахстане фабрики по пошиву кожаных сумок и эксклюзивных сумок;</p>--}}
                                        {{--<p>Внесение своего вклада  в  проект Патриотического акта «Мәнгілік ел» с популяризацией орнаментов казахского народного ремесла  в повседневных сумках, сувенирах, детских игрушках;</p>--}}

                                        {{--<h3>Автор идеи и основатель Еликбаева Сымбат Толеубаевна</h3>--}}
                                        <img src="{{$item->image_1}}" class="img-responsive" alt="">
                                        @if(($item->file))
                                            <a href="{{$item->file}}">Прикрепленный файл</a>
                                        @endif
                                        <p>По всем вопросам обращайтесь: <br>{{$item->phone}}<br>{{$item->email}}</p>
                                    </div>
                                </div>

                            </div>

                        </div>
                    </div>
                @elseif($item->type == 1)
                    <div class="col-md-3 col-sm-4 col-xs-6 blocks_mambers b2b{{$item->type}}">
                        <a href="" data-toggle="modal" data-target="#Modal{{$item->id}}">
                            <div class="img_suuport">
                                <img src="{{$item->image_1}}" class="img-responsive" alt="">
                                <span class="sale" style="width:135px; background:#02253a">Партнеры</span>
                            </div>
                        </a>
                        <div class="link_support">
                            <p><a href="" data-toggle="modal" data-target="#Modal{{$item->id}}">{{$item->title}}</a></p>
                        </div>
                    </div>
                    <div class="modal fade" id="Modal{{$item->id}}" role="dialog">
                        <div class="content-modal modal-dialog">

                            <!-- Modal content-->
                            <div class="modal-content" style="background-image:url(/public/themes/aba/images/black1.png);">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                </div>
                                <div class="modal-body clearfix">
                                    <h3>{{$item->title_content}}</h3>

                                    <div class="content-b2b">
                                        <p>{{$item->description}}</p>
                                        {{--<p>Создание впервые в Казахстане фабрики по пошиву кожаных сумок и эксклюзивных сумок;</p>--}}
                                        {{--<p>Внесение своего вклада  в  проект Патриотического акта «Мәнгілік ел» с популяризацией орнаментов казахского народного ремесла  в повседневных сумках, сувенирах, детских игрушках;</p>--}}

                                        {{--<h3>Автор идеи и основатель Еликбаева Сымбат Толеубаевна</h3>--}}
                                        <img src="{{$item->image_1}}" class="img-responsive" style="padding-bottom: 10px" alt="">
                                        @if(($item->file))
                                            <a href="{{$item->file}}" target="_blank" >Скачать прикрепленный файл</a>
                                        @endif
                                        <p>По всем вопросам обращайтесь: <br>{{$item->phone}}<br>{{$item->email}}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
            @endif
        @endforeach
            </div>
        </div>

@endsection