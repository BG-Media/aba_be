<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Алматы Бизнес Ассоциациясы</title>
</head>
<body>
<h1>Новости АБА</h1>
<h2>{{$data->title}}</h2>
<img src="{{$data->image}}" alt="">
<p>{!! $data->content !!}</p>
</body>
</html>