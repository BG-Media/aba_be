{{--{{dd($banner->all())}}--}}
@extends('layouts.mainpage')
@section('title')
Алматы Бизнес Ассоциациясы, Ассоциация Предпринимателей города Алматы «АБА» |Алматинская ассоциация поддержки предпринимателей «АБА» содействует в обеспечении защиты прав и интересов субъектов малого и среднего бизнеса, в развитии предпринимательства в производственной и сельскохозяйственной сферах, в диверсификации производств малого и среднего бизнеса и увеличении доли малого предпринимательства в структуре валового регионального продукта.
@endsection
@section('content')
    <div class="main_page">
        <div class="container-fluid">

            <div class="row rows_banner">
                <div class="container">
                    <h1 class="main_title">АБА-ЭТО</h1>

                    <div class="row top_block">
                        <div class="col-md-2 col-sm-3 col-xs-6 blocks_top_content">
                            <img src="{{asset('/themes/aba/images/img1.png')}}" class="img-responsive center-block img_servic" alt="">
                            <p class="text1_servic">200+</p>
                            <p class="text2_servic">Членов ассоциации</p>
                        </div>
                        <div class="col-md-2 col-sm-3 col-xs-6 blocks_top_content">
                            <img src="{{asset('/themes/aba/images/img2.png')}}" class="img-responsive center-block img_servic" alt="">
                            <p class="text1_servic">5+</p>
                            <p class="text2_servic">Лет на рынке</p>
                        </div>
                        <div class="col-md-2 col-sm-3 col-xs-6 blocks_top_content">
                            <img src="{{asset('/themes/aba/images/img3.png')}}" class="img-responsive center-block img_servic" alt="">
                            <p class="text1_servic">50+</p>
                            <p class="text2_servic">Бизнес мероприятий ежегодно</p>
                        </div>
                        <div class="col-md-2 col-sm-3 col-xs-6 blocks_top_content">
                            <img src="{{asset('/themes/aba/images/img4.png')}}" class="img-responsive center-block img_servic" alt="">
                            <p class="text1_servic">30+ </p>
                            <p class="text2_servic">Партнеров по всему миру</p>
                        </div>
                        <div class="col-md-2 col-sm-3 col-xs-6 blocks_top_content div-class">
                            <img src="{{asset('/themes/aba/images/img5.png')}}" class="img-responsive center-block img_servic" alt="">
                            <p class="text1_servic">1000+ </p>
                            <p class="text2_servic">Нетворкинговая сеть из компаний по Казахстану</p>
                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="container">
                <div class="row">
                    @include('layouts.brand')
                    @include('layouts.news')
                </div>

            </div>
        </div>
        <div class="row slider-reklama">
            @for($i=0; $i<count($banner);$i++)
                @if($banner[$i]->value == 2)
                    <div><a href="{{$banner[$i]->link}}" target="_blank"><img src="{{$banner[$i]->image}}" class="img-responsive center-block img_reclam" alt="" ></a></div>
                @endif
            @endfor
        </div>
        <div class="row bottom_content">
            <div class="container">
                @include('layouts.calendar')
            </div>

        </div>

        <div class="row">
            <div class="container">

                <div class="col-md-11 col-sm-11">
                    <h1 class="list_title">Наши партнеры</h1>
                </div>

                <div class="col-md-12 slider_2">
                    @for($i=0;$i<count($banner);$i++)
                        @if($banner[$i]->value == 3)
                            <div><a href="{{$banner[$i]->link}}" target="_blank"><img src="{{$banner[$i]->image}}" class="img-responsive" alt=""></a></div>
                        @endif
                    @endfor
                </div>
            </div>
        </div>
    </div>
@endsection