@extends('layouts.app')
@section('title')
    Каталог членов
@endsection
@section('content')
    <div class="col-md-11 col-sm-11"  itemscope ItemType = "http://aba.kz/catalog">
        <h1 class="list_title">Каталог членов</h1>

        <ol class="breadcrumb">
            <li><a href="{{url('/')}}">Главная</a></li>
            <li class="active">Каталог членов</li>
        </ol>
    </div>

    <div class="col-md-12 col-sm-12 col-xs-12 suk">
        {{--<h3 class="title_tab">Каталог членов</h3>--}}

        <select name="value-cat" id="value-cat" class="select_list ">
            <option value="0">Все члены ассоциации</option>
            <option value="1"> Авто, Спецтехника, автозапчасти</option>
            <option value="2">Издательства, полиграфия, типографии</option>
            <option value="3">Медицина</option>
            <option value="4"> Недвижимость  </option>
            <option value="5">Оборудование</option>
            <option value="6">Образование</option>
            <option value="7">Строительство и проектирование</option>
            <option value="8">Строительные материалы: производство, продажа</option>
            <option value="9"> Электротовары, бытовая техника</option>
            <option value="10"> Мебель, интерьер, фурнитура</option>
            <option value="11">IT услуги</option>
            <option value="12">Бухгалтерские и Юридические услуги</option>
            <option value="13">Одежда, обувь, галантерея</option>
            <option value="14">Рестораны, кафе, гостиницы, кейтеринг</option>
            <option value="15">Транспорт и перевозки</option>
            <option value="16">Телекоммуникации и связь</option>
            <option value="17">Продукты питания</option>
            <option value="18">Ювелирные изделия</option>
            <option value="19">Прочее услуги</option>
        </select>
        <div class="row">
            @foreach($store as $item)
                <div class="col-md-3 col-sm-4 col-xs-6 blocks_mambers cat-{{$item->value_cat}}">
                    <a href="/catalog/{{$item->id}}">
                        <div class="img_suuport">
                            <img src="{{$item->images}}" class="img-responsive" alt="">
                        </div>
                    </a>
                    <div class="link_support">
                        <p><a href="/catalog/{{$item->id}}">{{$item->title}}</a></p>
                    </div>

                </div>
            @endforeach
        </div>
    </div>
    @include('layouts.brand')
@stop
