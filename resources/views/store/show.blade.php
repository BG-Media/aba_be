{{--{{dd($store->all())}}--}}
@extends('layouts.app')
@section('title')
    @if ($store->role == 2)
        {{$store->title}}
    @else
        Скидка
    @endif
@endsection
@section('content')
    <div class="col-md-11 col-sm-11">
        <h1 class="list_title">
            @if ($store->role == 2)
                Каталог членов
            @else
                Скидка
            @endif
        </h1>

        <ol class="breadcrumb">
            <li><a href="{{url('/')}}">Главная</a></li>
            <li><a href="
                            @if ($store->role == 2)
                                {{url('/catalog')}}
                            @else
                                {{url('/sales')}}
                            @endif
                        ">
                    @if ($store->role == 2)
                        Каталог членов
                    @else
                        Скидка
                    @endif
                </a></li>
            <li class="active">{{$store->title}}</li>
        </ol>
    </div>

    <div class="col-md-12 col-sm-12 col-xs-12 ">
        <h3 class="title_tab clearfix">{{$store->title}}</h3>
        <div class="clearfix">
            <div class="add-advert">
                <div class="row">
                    <div class="col-md-4 col-sm-5">
                        <div class="blocks_mambers">
                            <a href="">
                                <div class="img_suuport">
                                    <img src="{{$store->images}}" class="img-responsive" alt="">
                                </div>
                            </a>
                            <div class="link_support ">
                                <p><a href="">{{$store->title}}</a></p>
                            </div>

                            <div class="info-address">
                                <p style="background-image: url(/public/themes/aba/images/i-phone.png);">{{$store->phone}}</p>
                                <p style="background-image: url(/public/themes/aba/images/i-address.png);">{{$store->address}}</p>
                                <p style="background-image: url(/public/themes/aba/images/i-email.png);">{{$store->mail}}</p>
                                <a href="<? $even =($store->site);
                                if (preg_match('/^(https?)(:\/\/)/', $even)){
                                    echo $even;
                                }else{
                                    echo 'http://'.$even;
                                }
                                ?>
                                " target="_blank"><p style="background-image: url(/public/themes/aba/images/icon-web.png);">{{$store->site}}</p></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8 col-sm-7">
                        <div class="text-catalog-member content_support">
                            <p>{!!$store->description!!}</p>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
@stop
