@extends('system.system')
@section('breadcrumb')

    <ul class="breadcrumb">
        <li><a href="#">Home</a></li>
        <li><a href="#">>Store list</a></li>
        <li class="active">Store Create</li>
    </ul>

@endsection

@section('content')
    <!-- PAGE CONTENT WRAPPER -->
    <div class="page-content-wrap">
        <div class="row">
            <div class="col-md-12">
                <form action="{{ url('store') }}" method="POST" enctype="multipart/form-data" class="form-horizontal">
                    {{ csrf_field() }}
                    <div class="panel panel-default">
                        {!! view('system/inputs.title',['name'=>'Add Store']) !!}
                        {!! view('store.form',compact('post','categories')) !!}
                        {!! view('system/inputs.submit',['name'=>'Add Store','icon' =>'plus']) !!}
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- END PAGE CONTENT WRAPPER -->
@endsection

@section('page_plugins')

        <!-- START PAGE PLUGINS -->
    <script type='text/javascript' src="{{asset('system/js/plugins/icheck/icheck.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('system/js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js')}}"></script>

    <script type="text/javascript" src="{{asset('system/js/plugins/bootstrap/bootstrap-datepicker.js')}}"></script>
    <script type="text/javascript" src="{{asset('system/js/plugins/bootstrap/bootstrap-timepicker.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('system/js/plugins/bootstrap/bootstrap-colorpicker.js')}}"></script>
    <script type="text/javascript" src="{{asset('system/js/plugins/bootstrap/bootstrap-file-input.js')}}"></script>
    <script type="text/javascript" src="{{asset('system/js/plugins/bootstrap/bootstrap-select.js')}}"></script>
    <script type="text/javascript" src="{{asset('system/js/plugins/tagsinput/jquery.tagsinput.min.js')}}"></script>

    <!-- END PAGE PLUGINS -->

@endsection