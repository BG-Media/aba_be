<div class="panel-body">

{!! view('errors.validation') !!}
{!! view('system.inputs.text',['name'=>'title','value' => isset($store->title) ? $store->title : '','label'=>'Title','id'=>'title']) !!}
{!! view('system.inputs.text',['name'=>'sales','value' => isset($store->sales) ? $store->sales : '','label'=>'Sales','id'=>'sales']) !!}
    <div class="form-group">
        <label class="col-md-3 control-label">Value</label>
        <div class="col-md-6">
            @include('layouts.sales')
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-3 control-label">View</label>
        <div class="col-md-6">
            <select class="form-control select " data-style="btn" name="role">
                <option value="1">Sales</option>
                <option value="2">Catalog</option>
            </select>
        </div>
    </div>
    {!! view('system.inputs.file', ['name'=>'images','value' => isset($store->images) ? $store->images : '','label'=>'Post media','id'=>'images']) !!}
        <h5 style="text-align: center;">Форма для подробно добавление каталогов и скидки</h5>
    {!! view('system.inputs.textarea',['name'=>'description','value' => isset($store->description) ? $store->description : '','label'=>'Description','id'=>'summernote','rows'=>'3','cols'=>'16']) !!}
    {!! view('system.inputs.text',['name'=>'phone','value' => isset($store->phone) ? $store->phone : '','label'=>'Phone','id'=>'slug']) !!}
    {!! view('system.inputs.text',['name'=>'address','value' => isset($store->address) ? $store->address : '','label'=>'Address','id'=>'Address']) !!}
    {!! view('system.inputs.text',['name'=>'mail','value' => isset($store->mail) ? $store->mail : '','label'=>'Mail','id'=>'Mail']) !!}
    {!! view('system.inputs.text',['name'=>'site','value' => isset($store->site) ? $store->site : '','label'=>'Site','id'=>'Site']) !!}

</div>