@extends('layouts.app')
@section('title')
    Скидки
@endsection
@section('content')
    <div class="col-md-11 col-sm-11">
        <h1 class="list_title">Скидки</h1>

        <ol class="breadcrumb">
            <li><a href="{{url('/')}}">Главная</a></li>
            <li><a href="#">Об ассоциации</a></li>
            <li class="active">Скидки</li>
        </ol>
    </div>

    <div class="col-md-12 col-sm-12 col-xs-12">
        <select name="value" id="value" class="select_list">
            <option value="0">Все члены ассоциации</option>
            <option value="1">Здоровье</option>
            <option value="2">Услуги</option>
            <option value="3">Еда</option>
            <option value="4">Товары</option>
            <option value="5">Красота</option>
            <option value="6">Отдых</option>
            <option value="7">Одежда</option>
        </select>
        <div class="row" id = "tbody">
            @foreach($store as $item)
                <div class="col-md-3 col-sm-4 col-xs-6 blocks_mambers gr-{{$item->value}}">
                    <a href="/sales/{{$item->id}}">
                        <div class="img_suuport">
                            <img src="{{$item->images}}" class="img-responsive" alt="">
                            <span class="sale">{{$item->sales}}%</span>
                        </div>
                    </a>
                    <div class="link_support">
                        <p><a href="/sales/{{$item->id}}">{{$item->title}}</a></p>
                    </div>

                </div>
            @endforeach
        </div>
    </div>
    @include('layouts.brand')
@stop
