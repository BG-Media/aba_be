<div class="panel-footer">
    <button class="btn btn-default">Clear Form</button>
    <button type="submit" class="btn btn-default pull-right">
        <i class="fa fa-{{ $icon }}"></i> {{ $name }}
    </button>
</div>