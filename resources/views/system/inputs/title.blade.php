<div class="panel-heading">
    <h3 class="panel-title">{{ $name }}</h3>
    <ul class="panel-controls">
        <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
    </ul>
</div>