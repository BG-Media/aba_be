<div class="form-group">
    <label class="col-md-3 col-xs-12 control-label">{{ $label }}</label>
    <div class="col-md-6 col-xs-12">
        <textarea name="{{ $name }}" id="{{ $id }}" class="form-control" rows="{{ $rows }}" cols="{{ $cols }}">{{ old($name, isset($value) ? $value : '') }}</textarea>
        @if(isset($hint))
        <span class="help-block">Default textarea field</span>
        @endif
    </div>
</div>