<div class="form-group">
    <label class="col-md-3 control-label">{{ $label }}</label>
    <div class="col-md-6 col-xs-12">
        <div class="input-group">
            <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
            <input type="text" name="{{ $name }}" value="{{ old($name, isset($value) ? $value : '') }}" id="{{ $id }}" class="form-control"/>
        </div>
        @if(isset($hint))
            <span class="help-block">Default textarea field</span>
        @endif
    </div>
</div>