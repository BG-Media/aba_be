<div class="form-group">
    <label class="col-md-3 col-xs-12 control-label">{{ $label }}</label>
    <div class="col-md-6 col-xs-12">
        <input type="file" class="fileinput btn-primary" name="{{ $name }}" id="{{ $id }}" {{ isset($multiple) ? $multiple:'' }} title="Browse file"/>
        @if(isset($hint))
            <span class="help-block">Default textarea field</span>
        @endif
        @if(isset($value))
            <img src="{{$value}}" style="width: 280px;height: 200px;object-fit: contain;float: right;">
        @endif
    </div>
</div>
