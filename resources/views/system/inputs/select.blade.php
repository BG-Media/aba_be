<div class="form-group">
    <label class="col-md-3 control-label">{{ $label }}</label>
    <div class="col-md-6 col-xs-12">
        <select class="form-control select" data-live-search="true" name="{{ $name }}" id="{{ $id }}">
            <option value="0">{{ $defaultOption }}</option>
            @forelse($lists as $key=>$list)
                <option value="{{ $list->id }}" @if(old($name,$value) == $list->id) selected @endif>{{ $list->title }}</option>
            @empty
                <option value="">No list</option>
            @endforelse
        </select>
    </div>
</div>

@push('scripts')
<script type="text/javascript" src="{{ asset('/system/js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('/system/js/plugins/bootstrap/bootstrap-select.js') }}"></script>
@endpush
