<ul class="x-navigation">
    <li class="xn-logo">
        <a href="html/index.html">bcode</a>
        <a href="#" class="x-navigation-control"></a>
    </li>
    <li class="xn-profile">
        <a href="#" class="profile-mini">
            <img src="{{asset('system/assets/images/users/avatar.jpg')}}" alt="John Doe"/>
        </a>
        <div class="profile">
            <div class="profile-image">
                <img src="{{asset('system/assets/images/users/avatar.jpg')}}" alt="John Doe"/>
            </div>
            <div class="profile-data">
                <div class="profile-data-name">John Doe</div>
                <div class="profile-data-title">Web Developer/Designer</div>
            </div>
            <div class="profile-controls">
                <a href="pages-profile.html" class="profile-control-left"><span class="fa fa-info"></span></a>
                <a href="pages-messages.html" class="profile-control-right"><span class="fa fa-envelope"></span></a>
            </div>
        </div>
    </li>
    {{--<li class="active">--}}
        {{--<a href="{{ url('dashboard') }}"><span class="fa fa-desktop"></span> <span class="xn-text">Dashboard</span></a>--}}
    {{--</li>--}}
    <li class="xn-title">Basic modules</li>
    <li>
        <a href="{{ url('page') }}"><span class="fa fa-files-o"></span> <span class="xn-text">Страницы</span></a>
    </li>
    <li>
        <a href="{{ url('post') }}"><span class="fa fa-file-text-o"></span> <span class="xn-text">Посты</span></a>
    </li>
    <li>
        <a href="{{ url('category') }}"><span class="fa fa-folder-open"></span> <span class="xn-text">Категории</span></a>
    </li>
    <li>
        <a href="{{ url('card') }}"><span class="fa fa-comment-o"></span> <span class="xn-text">Карты</span></a>
    </li>
    <li>
        <a href="{{ url('store') }}"><span class="fa fa-picture-o"></span> <span class="xn-text">Скидки,Каталог</span></a>
    </li>
    <li>
        <a href="{{ url('banner') }}"><span class="fa fa-users"></span> <span class="xn-text">Баннер,Партнеры,Бренд</span></a>
    </li>
    <li>
        <a href="{{ url('b2b') }}"><span class="fa fa-users"></span> <span class="xn-text">B2B</span></a>
    </li>
    <li>
        <a href="{{ url('subscription') }}"><span class="fa fa-users"></span> <span class="xn-text">Подписка</span></a>
    </li>
    <li>
        <a href="{{ url('application') }}"><span class="fa fa-users"></span> <span class="xn-text">Заявки</span></a>
    </li>
    <li>
        <a href="{{ url('user') }}"><span class="fa fa-users"></span> <span class="xn-text">Пользователии</span></a>
    </li>
    <li class="xn-title">Components</li>
    <li>
        <a href="{{ url('about-system') }}"><span class="fa fa-key"></span> <span class="xn-text">Выход из системы</span></a>
    </li>
</ul>