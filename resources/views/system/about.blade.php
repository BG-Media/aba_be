@extends('system.system')

@section('breadcrumb')
        <!-- START BREADCRUMB -->
<ul class="breadcrumb">
    <li><a href="#">Home</a></li>
    <li class="active">About system</li>
</ul>
<!-- END BREADCRUMB -->
@endsection

@section('content')
    <div class="page-content-wrap">

        <!-- START PANELS WITH CONTROLS -->
        <div class="row">
            <div class="col-md-12">

                <!-- START PANEL WITH STATIC CONTROLS -->
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">About system</h3>
                        <ul class="panel-controls">
                            <li><a href="#" class="panel-fullscreen"><span class="fa fa-expand"></span></a></li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="fa fa-cog"></span></a>
                                <ul class="dropdown-menu">
                                    <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span> Collapse</a></li>
                                    <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span> Refresh</a></li>
                                </ul>
                            </li>
                            <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                        </ul>
                    </div>
                    <div class="panel-body">
                         Version 1.0<br>
                         Batyrqul Maqsat and Adilet Maksatkaliuly <br>supported by BUGIN GROUP
                    </div>
                    <div class="panel-footer"></div>
                </div>
                <!-- END PANEL WITH STATIC CONTROLS -->

            </div>
        </div>
        <!-- END PANELS WITH CONTROLS -->

    </div>
@endsection