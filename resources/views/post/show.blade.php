{{--{{dd($post)}}--}}
@extends('layouts.app')
@section('title')
@endsection
@section('content')
    <h1 class="title_page">{{ $post->title }}</h1>
    <ol class="breadcrumb">
        <li><a href="/">Главная</a></li>
        <li><a href="/">Об ассоциации </a></li>
        <li class="active">{{ $post->title }}</li>
    </ol>

    <div class="col-md-12  content_news goals_objectives">
        {!! $post->content !!}
    </div>
@stop
