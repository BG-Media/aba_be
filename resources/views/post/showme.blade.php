{{--{{dd($post)}}--}}
{{--{{dd($pictures[0]->source)}}--}}
@extends('layouts.app')
@section('title')
{{$post->title}}
@endsection
@section('content')
    <div class="col-md-11 col-sm-11">
        <h1 class="list_title">{{$post->title}}</h1>

        <ol class="breadcrumb">
            <li><a href="{{url('/')}}">Главная</a></li>
            <li><a href="{{url('category',1)}}">новости</a></li>
            <li class="active">{{$post->title}}</li>
        </ol>
    </div>
    <div class="col-md-12 text_news ">
        <p>{!!$post->content!!}</p>
        </div>
    @if(isset($pictures))
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="slider-nav popup-gallery">
            @foreach($pictures as $item)
                <div>
                    <a class="gallery" rel="group"  href="{{$item->source}}"><img src="{{$item->source}}" class="img-responsive" alt="{{$post->title}}"></a>
                </div>
            @endforeach
        </div>
    </div>
    @endif

    <div class="col-md-12 share-pad">
        {{--<p class="text_news">{!!$post->content!!}</p>--}}

        <div class="share">
            <div class="shar"><span class="sharies">Поделиться </span></div>
            <div class="share">
                <script src="//yastatic.net/es5-shims/0.0.2/es5-shims.min.js"></script>
                <script src="//yastatic.net/share2/share.js"></script>
                <div class="ya-share2" data-services="vkontakte,facebook,odnoklassniki,moimir,twitter,whatsapp,telegram"></div>
            </div>
            {{--<a href=""><img src="{{asset('/themes/aba/images/icon_vk.png')}}" alt=""></a>--}}
            {{--<a href=""><img src="{{asset('/themes/aba/images/icon_fb.png')}}" alt=""></a>--}}
            {{--<a href=""><img src="{{asset('/themes/aba/images/icon_tw.png')}}" alt=""></a>--}}
            {{--<a href=""><img src="{{asset('/themes/aba/images/icon_go.png')}}" alt=""></a>--}}
        </div>
    </div>
   @endsection