@extends('system.system')

@section('breadcrumb')

    <ul class="breadcrumb">
        <li><a href="#">Home</a></li>
        <li><a href="#">>Post list</a></li>
        <li class="active">Post Create</li>
    </ul>

@endsection

@section('content')
    <!-- PAGE CONTENT WRAPPER -->
    <div class="page-content-wrap">
        <div class="row">
            <div class="col-md-12">
                <form action="{{ url('post') }}" method="POST" enctype="multipart/form-data" class="form-horizontal">
                    {{ csrf_field() }}
                    <div class="panel panel-default">
                        {!! view('system/inputs.title',['name'=>'Add Post']) !!}
                        <div class="panel-body">
							{!! view('errors.validation') !!}
							{!! view('system.inputs.text',['name'=>'title','value' => isset($post->title) ? $post->title : '','label'=>'Имя пользователя','id'=>'title']) !!}
							<input type="hidden" name="category_id" value="9"/>
							<div class="form-group">
								<label class="col-md-3 control-label">Slug</label>
								<div class="col-md-6 col-xs-12">
									<select class="form-control select" data-live-search="true" name="slug" id="slug">
										<option value="0">Выбрать</option>
											<option value="1">Юриспруденция</option>
											<option value="2">Топ-менеджмент</option>
											<option value="3">Бухгалтерия</option>
											<option value="4">Страхование и банки</option>
											<option value="5">IT, телеком</option>
											<option value="6">Продажи и менеджмент</option>
											<option value="7">Строительство</option>
											<option value="8">Медицина</option>
											<option value="9">Прочее</option>
									</select>
								</div>
							</div>
							{!! view('system.inputs.date',['name'=>'created_at','value' => isset($post->created_at) ? $post->created_at : '','label'=>'Date','id'=>'date']) !!}
							<div class="form-group">
								<label class="col-md-3 control-label">24 hour mode</label>
								<div class="col-md-5">
									<div class="input-group bootstrap-timepicker">
										<input type="text" class="form-control timepicker24" name="time"/>
										<span class="input-group-addon"><span class="glyphicon glyphicon-time"></span></span>
									</div>
								</div>
							</div>
							{!! view('system.inputs.file', ['name'=>'media_id','value' => isset($post->media_id) ? $post->media_id : '','label'=>'Post media','id'=>'post_media']) !!}

							
						</div>
                        {!! view('system/inputs.submit',['name'=>'Add Post','icon' =>'plus']) !!}
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- END PAGE CONTENT WRAPPER -->
@endsection

@section('page_plugins')

        <!-- START PAGE PLUGINS -->
    <script type='text/javascript' src="{{asset('system/js/plugins/icheck/icheck.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('system/js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js')}}"></script>

    <script type="text/javascript" src="{{asset('system/js/plugins/bootstrap/bootstrap-datepicker.js')}}"></script>
    <script type="text/javascript" src="{{asset('system/js/plugins/bootstrap/bootstrap-timepicker.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('system/js/plugins/bootstrap/bootstrap-colorpicker.js')}}"></script>
    <script type="text/javascript" src="{{asset('system/js/plugins/bootstrap/bootstrap-file-input.js')}}"></script>
    <script type="text/javascript" src="{{asset('system/js/plugins/bootstrap/bootstrap-select.js')}}"></script>
    <script type="text/javascript" src="{{asset('system/js/plugins/tagsinput/jquery.tagsinput.min.js')}}"></script>

    <!-- END PAGE PLUGINS -->

@endsection