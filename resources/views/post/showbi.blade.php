{{--{{dd($post)}}--}}
@extends('layouts.app')
@section('title')
     {{$post->title}}
@endsection
@section('content')
    <div class="col-md-11 col-sm-11">
        <h1 class="list_title">{{$post->title}}</h1>

        <ol class="breadcrumb">
            <li><a href="{{url('/')}}">Главная</a></li>
            <li class="active"> Мероприятия</li>
        </ol>
    </div>

    <div class="col-md-12">
        <div class="row">

            <div class="col-md-5 col-sm-6 mamber_block">
                <div class="mamber_img">
                    <img src="{{$post->media_id}}" class="img_mamber" alt="">
                </div>
                @if($post->created_at >date('Y-m-d H:i:s'))
                    <span class="it_was will_be">Будет</span>
                    @else
                    <span class="it_was">Было</span>
                @endif


                <div class="bottom_info">
                    <?php
                    $month = [
                            "01"=>"Январь",
                            "02"=>"Февраль",
                            "03"=>"Март",
                            "04"=>"Апрель",
                            "05"=>"Май",
                            "06"=>"Июнь",
                            "07"=>"Июль",
                            "08"=>"Август",
                            "09"=>"Сентябрь",
                            "10"=>"Октябрь",
                            "11"=>"Ноябрь",
                            "12"=>"Декабрь"
                    ];
                    ?>
                    <span><img src="{{asset('/themes/aba/images/calendar.png')}}" alt="">{{date('d',strtotime($post->created_at))}} {{$month[date('m', strtotime($post->created_at))]}}</span>
                    <span><img src="{{asset('/themes/aba/images/clock.png')}}" alt="">{{$post->time}}</span>
                </div>

                <div class="bottom_info">
                    <span><img src="{{asset('/themes/aba/images/map_icon.png')}}" alt="">{{$post->slug}}</span>
                </div>
                @if(isset($pictures[0]->source))
                <p class="down_link"><a href="{{$pictures[0]->source}}">Скачать программу тренинга>>>></a></p>
                @endif
                <p class="info_mamber">{{$post->excerpt}}</p>
            </div>

            <div class="col-md-7 col-sm-6 text_mamber">
                <h3 class="title_mamber">{{$post->title}}</h3>
                <p>{!! $post->content !!}</p>
                {{--<h4 class="inside_title"><b>Уважаемые члены и друзья АБА!</b></h4>--}}
                {{--<p class="text_content_mamber">ТОО “BPMI Kazakhstan” приглашает Вас на однодневный семинар легендарного эксперта в области психологии успеха – Брайана Трейси «Достижение Максимума», который состоится 21 июня, во Дворце Республики, в Алматы.</p>--}}
                {{--<p class="text_content_mamber">Семинар от ведущего специалиста по вопросам развития потенциальных возможностей человека, одного из самых влиятельных преподавателей в бизнес сфере, чье имя известно во всем Мире, чьи техники и ключи для управления и развития бизнеса используются день ото дня и востребованы во всем мировом масштабе. </p>--}}
                {{--<h4 class="inside_title"><b>! Для всех членов Ассоциации действует скидка 10% !</b></h4>--}}
            </div>

        </div>
    </div>

@endsection