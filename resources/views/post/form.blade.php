<div class="panel-body">
    {!! view('errors.validation') !!}
    {!! view('system.inputs.text',['name'=>'title','value' => isset($post->title) ? $post->title : '','label'=>'Заголовок','id'=>'title']) !!}
    {!! view('system.inputs.text',['name'=>'slug','value' => isset($post->slug) ? $post->slug : '','label'=>'Slug (ПРОЕКТ < через запятую >)','id'=>'slug']) !!}
    {!! view('system.inputs.textarea',['name'=>'excerpt','value' => isset($post->excerpt) ? $post->excerpt : '','label'=>'Краткое описание контента','id'=>'excerpt','rows'=>'3','cols'=>'3']) !!}
    {!! view('system.inputs.textarea',['name'=>'content','value' => isset($post->content) ? $post->content : '','label'=>'Контент','id'=>'summernote','rows'=>'3','cols'=>'16']) !!}
    {!! view('system.inputs.select',['name'=>'category_id','value' => isset($post->category_id) ? $post->category_id : '','label'=>'Категория поста','id'=>'category_id','lists' => $categories,'defaultOption'=>'Select post category' ]) !!}
    {!! view('system.inputs.date',['name'=>'created_at','value' => isset($post->created_at) ? $post->created_at : '','label'=>'Дата','id'=>'date']) !!}
    <div class="form-group">
        <label class="col-md-3 control-label">Время: начало - окончание</label>
        <div class="col-md-5">
            <div class="input-group bootstrap-timepicker">
                <input type="text" class="form-control " name="time"/>
                <span class="input-group-addon"><span class="glyphicon glyphicon-time"></span></span>
            </div>
        </div>
    </div>
    {!! view('system.inputs.file', ['name'=>'media_id','value' => isset($post->media_id) ? $post->media_id : '','label'=>'Фото поста','id'=>'post_media']) !!}

    <h2 style="text-align: center;">Картинка для слайдера</h2>
    @if(isset($pictures))
        @foreach($pictures as $item)
            {!! view('system.inputs.file', ['name'=>'image[]','value' => $item->source,'label'=>'Slider media','id'=>'post_media','max'=>'30' ]) !!}
            <input type="hidden" value="{{$item->source}}" name="sources[]" >
        @endforeach
    @else
        {!! view('system.inputs.file', ['name'=>'image[]','label'=>'Фото для слайдера' ,'id'=>'post_media','multiple'=>'multiple']) !!}
        {{--{!! view('system.inputs.file', ['name'=>'image[]','label'=>'Slide media 1','id'=>'post_media']) !!}--}}
        {{--{!! view('system.inputs.file', ['name'=>'image[]','label'=>'Slide media 1','id'=>'post_media']) !!}--}}
        {{--{!! view('system.inputs.file', ['name'=>'image[]','label'=>'Slide media 1','id'=>'post_media']) !!}--}}
        {{--{!! view('system.inputs.file', ['name'=>'image[]','label'=>'Slide media 1','id'=>'post_media']) !!}--}}
    @endif
    {{--{!! view('system.inputs.file', ['name'=>'image[]','label'=>'Slide media 2','id'=>'post_media']) !!}--}}
    {{--{!! view('system.inputs.file', ['name'=>'image[]','label'=>'Slide media 3','id'=>'post_media']) !!}--}}
    {{--{!! view('system.inputs.file', ['name'=>'image[]','label'=>'Slide media 4','id'=>'post_media']) !!}--}}
    {{--{!! view('system.inputs.file', ['name'=>'image[]','label'=>'Slide media 5','id'=>'post_media']) !!}--}}
    {{--{!! view('system.inputs.file', ['name'=>'image[]','label'=>'Slide media 6','id'=>'post_media']) !!}--}}
    {{--{!! view('system.inputs.file', ['name'=>'image[]','label'=>'Slide media 7','id'=>'post_media']) !!}--}}
    {{--{!! view('system.inputs.file', ['name'=>'image[]','label'=>'Slide media 8','id'=>'post_media']) !!}--}}
</div>