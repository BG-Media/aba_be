{{--{{dd($post)}}--}}
@extends('layouts.app')
@section('title')
    {{$post->title}}
@endsection
@section('content')
    <div class="col-md-11 col-sm-11 col-xs-12">
        <h1 class="list_title">{{$post->title}}</h1>

        <ol class="breadcrumb">
            <li><a href="{{url('/')}}">Главная</a></li>
            <li><a href="{{url('category',$post->category_id)}}">Поддержка бизнеса  </a></li>
            <li class="active">{{$post->title}}</li>
        </ol>
    </div>

    <div class="col-md-12 col-sm-12 col-xs-12">
        {{--<h3 class="title_tab clearfix">{{$post->title}}</h3>--}}
        <div class="content_support clearfix">
            <p>{!!$post->content!!}</p>
            {{--<p>Подробнее на сайте: <a href="{{$post->excerpt}}" class="link_damu">{{$post->excerpt}}</a></p>--}}
        </div>
    </div>
@endsection