@extends('system.system')

        <!-- START BREADCRUMB -->
@section('breadcrumb')
    <ul class="breadcrumb">
        <li><a href="#">Home</a></li>
        <li><a href="#">Profile</a></li>
        <li class="active">Edit</li>
    </ul>
    @endsection
            <!-- END BREADCRUMB -->

    @section('content')
            <!-- PAGE CONTENT WRAPPER -->
    <div class="page-content-wrap">

        <div class="row">
            <div class="col-md-12">
                <form method="post" action="/user/{{$user->id}}" class="form-horizontal">
                    {{ csrf_field() }}
                    {{ method_field('PUT') }}
                    <div class="panel panel-default tabs">
                        <div class="panel-body tab-content">
                            <div class="tab-pane active" id="tab-first">
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed dictum dolor sem, quis pharetra dui ultricies vel. Cras non pulvinar tellus, vel bibendum magna. Morbi tellus nulla, cursus non nisi sed, porttitor dignissim erat. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc facilisis commodo lectus. Vivamus vel tincidunt enim, non vulputate ipsum. Ut pellentesque consectetur arcu sit amet scelerisque. Fusce commodo leo eros, ut eleifend massa congue at.</p>

                                <div class="form-group">
                                    <label class="col-md-3 col-xs-12 control-label">Name</label>
                                    <div class="col-md-6 col-xs-12">
                                        <input name="name" type="text" class="form-control" value="{{$user->name}}"/>
                                        @if ($errors->has('name'))
                                            <span class="help-block">
                                            <strong>{{ $errors->first('name') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-3 col-xs-12 control-label">Email</label>
                                    <div class="col-md-6 col-xs-12">
                                        <input type="email" name="email" class="form-control" value="{{$user->email}}" disabled/>
                                        @if ($errors->has('email'))
                                            <span class="help-block">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-3 col-xs-12 control-label">Role</label>
                                    <div class="col-md-2">
                                        <select name="role" class="form-control select">
                                            @foreach($roles as $role)
                                                @if($role->id == $role_id)
                                                    <option selected="selected">{{$role->role}}</option>
                                                @else
                                                    <option>{{$role->role}}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-3 col-xs-12 control-label">Password</label>
                                    <div class="col-md-6 col-xs-12">
                                        <input type="password" name="password" class="form-control" value=""/>
                                        @if ($errors->has('password'))
                                            <span class="help-block">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-3 col-xs-12 control-label">Confirm Password</label>
                                    <div class="col-md-6 col-xs-12">
                                        <input type="password" name="password_confirmation" class="form-control" value=""/>
                                        @if ($errors->has('password_confirmation'))
                                            <span class="help-block">
                                            <strong>{{ $errors->first('password_confirmation') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="panel-footer">
                                    <button class="btn btn-primary pull-right" type="submit" >Save Changes <span class="fa fa-floppy-o fa-right"></span></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>

            </div>

        </div>

    </div>

    <!-- END PAGE CONTENT WRAPPER -->
    @endsection

            <!-- THIS PAGE PLUGINS -->
@section('page_plugins')

    <script type='text/javascript' src="{{asset('system/js/plugins/icheck/icheck.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('system/js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js')}}"></script>

    <script type="text/javascript" src="{{asset('system/js/plugins/bootstrap/bootstrap-datepicker.js')}}"></script>
    <script type="text/javascript" src="{{asset('system/js/plugins/bootstrap/bootstrap-file-input.js')}}"></script>
    <script type="text/javascript" src="{{asset('system/js/plugins/bootstrap/bootstrap-select.js')}}"></script>
    <script type="text/javascript" src="{{asset('system/js/plugins/tagsinput/jquery.tagsinput.min.js')}}"></script>

    @endsection
            <!-- END THIS PAGE PLUGINS -->






