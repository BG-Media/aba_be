@extends('system.system')

        <!-- START BREADCRUMB -->
@section('breadcrumb')
    <ul class="breadcrumb">
        <li><a href="#">Home</a></li>
        <li><a href="#">Profile</a></li>
        <li class="active">Create</li>
    </ul>
    @endsection
            <!-- END BREADCRUMB -->

    @section('content')
            <!-- PAGE CONTENT WRAPPER -->
    <div class="page-content-wrap">
        <div class="row">
            <div class="col-md-12">
                <form action="{{ url('role') }}" method="POST" enctype="multipart/form-data" class="form-horizontal">
                    {{ csrf_field() }}
                    <div class="panel panel-default">
                        {!! view('system/inputs.title',['name'=>'Add new user']) !!}
                        {!! view('errors.validation') !!}
                        <div class="panel-body">
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Role name:</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                        <input type="text" name="role" value="" class="form-control" required/>
                                    </div>
                                </div>
                            </div>
                            <div class="panel-footer">
                                <button class="btn btn-default" type="reset">Clear Form</button>
                                <button type="submit" class="btn btn-default pull-right">
                                    <i class="fa fa-plus"></i> Add role
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- END PAGE CONTENT WRAPPER -->
    @endsection

            <!-- THIS PAGE PLUGINS -->
@section('page_plugins')

    <script type='text/javascript' src="{{asset('system/js/plugins/icheck/icheck.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('system/js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js')}}"></script>

    <script type="text/javascript" src="{{asset('system/js/plugins/bootstrap/bootstrap-datepicker.js')}}"></script>
    <script type="text/javascript" src="{{asset('system/js/plugins/bootstrap/bootstrap-file-input.js')}}"></script>
    <script type="text/javascript" src="{{asset('system/js/plugins/bootstrap/bootstrap-select.js')}}"></script>
    <script type="text/javascript" src="{{asset('system/js/plugins/tagsinput/jquery.tagsinput.min.js')}}"></script>

    @endsection
            <!-- END THIS PAGE PLUGINS -->






