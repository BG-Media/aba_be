@extends('system.system')

<!-- START BREADCRUMB -->
@section('breadcrumb')
<ul class="breadcrumb">
    <li><a href="#">Home</a></li>
    <li class="active">Profile</li>
</ul>
@endsection
<!-- END BREADCRUMB -->

@section('content')
<!-- PAGE CONTENT WRAPPER -->
<div class="page-content-wrap">

    <div class="row">
        <div class="col-md-12">
                <div class="panel panel-default tabs">
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="active"><a href="#tab-first" role="tab" data-toggle="tab">Personal Data</a></li>
                        <li><a href="#tab-second" role="tab" data-toggle="tab">All users</a></li>
                        <li><a href="#tab-third" role="tab" data-toggle="tab">Roles</a></li>
                    </ul>
                    <div class="panel-body tab-content">
                        <div class="tab-pane active" id="tab-first">
                        <form method="post" action="/user/{{Auth::user()->id}}" class="form-horizontal">
                            {{ csrf_field() }}
                            {{ method_field('PUT') }}
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed dictum dolor sem, quis pharetra dui ultricies vel. Cras non pulvinar tellus, vel bibendum magna. Morbi tellus nulla, cursus non nisi sed, porttitor dignissim erat. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc facilisis commodo lectus. Vivamus vel tincidunt enim, non vulputate ipsum. Ut pellentesque consectetur arcu sit amet scelerisque. Fusce commodo leo eros, ut eleifend massa congue at.</p>

                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Name</label>
                                <div class="col-md-6 col-xs-12">
                                    <input name="name" type="text" class="form-control" value="{{Auth::user()->name}}"/>
                                    @if ($errors->has('name'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('name') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Email</label>
                                <div class="col-md-6 col-xs-12">
                                    <input type="email" name="email" class="form-control" value="{{Auth::user()->email}}" disabled/>
                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Role</label>
                                <div class="col-md-2">
                                    <select name="role1" class="form-control select" disabled>
                                        <option selected="selected">{{$role->role}}</option>
                                    </select>
                                    <input type="hidden" name="role" value="{{$role->role}}">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Password</label>
                                <div class="col-md-6 col-xs-12">
                                    <input type="password" name="password" class="form-control" value=""/>
                                    @if ($errors->has('password'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Confirm Password</label>
                                <div class="col-md-6 col-xs-12">
                                    <input type="password" name="password_confirmation" class="form-control" value=""/>
                                    @if ($errors->has('password_confirmation'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('password_confirmation') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="panel-footer">
                                <button class="btn btn-primary pull-right" type="submit" >Save Changes <span class="fa fa-floppy-o fa-right"></span></button>
                            </div>

                        </form  >

                        </div>
                        <div class="tab-pane" id="tab-second">
                            <p>Donec tristique eu sem et aliquam. Proin sodales elementum urna et euismod. Quisque nisl nisl, venenatis eget dignissim et, adipiscing eu tellus. Sed nulla massa, luctus id orci sed, elementum consequat est. Proin dictum odio quis diam gravida facilisis. Sed pharetra dolor a tempor tristique. Sed semper sed urna ac dignissim. Aenean fermentum leo at posuere mattis. Etiam vitae quam in magna viverra dictum. Curabitur feugiat ligula in dui luctus, sed aliquet neque posuere.</p>

                            <!-- START DEFAULT DATATABLE -->
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title">
                                        <button onclick="window.location.href='/user/create';"  class="btn btn-info">
                                            <i class="fa fa-plus"></i> Add new user
                                        </button>
                                    </h3>
                                    <ul class="panel-controls">
                                        <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                                        <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span></a></li>
                                        <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                                    </ul>
                                </div>
                                <div class="panel-body">
                                    <table class="table datatable">
                                        <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Email</th>
                                            <th>Role</th>
                                            <th>Actions</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @for ($i=0; $i<count($users);$i++)
                                            <tr>
                                                <td>
                                                    <div>{{ $users[$i]->name }}</div>
                                                </td>
                                                <td>
                                                    <div>{{ $users[$i]->email }}</div>
                                                </td>
                                                <td>
                                                    <div>{{ $roles[$i] }}</div>
                                                </td>
                                                <td>
                                                    @if($roles[$i] == 'admin')
                                                        <a href="/user/{{ $users[$i]->id }}/edit" class="btn btn-primary" disabled=""><i class="fa fa-btn fa-edit"></i>Edit</a>
                                                        <button type="submit" id="delete-post-{{ $users[$i]->id }}" class="btn btn-danger" disabled>
                                                            <i class="fa fa-btn fa-trash"></i>Delete
                                                        </button>
                                                    @else
                                                    <a href="/user/{{ $users[$i]->id }}/edit" class="btn btn-primary"><i class="fa fa-btn fa-edit"></i>Edit</a>
                                                    <form action="{{ url('user/'.$users[$i]->id) }}" method="POST" style="display: inline-block">
                                                        {{ csrf_field() }}
                                                        {{ method_field('DELETE') }}
                                                        <button type="submit" id="delete-user-{{ $users[$i]->id }}" class="btn btn-danger">
                                                            <i class="fa fa-btn fa-trash"></i>Delete
                                                        </button>
                                                    </form>
                                                    @endif
                                                </td>
                                            </tr>
                                        @endfor
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- END DEFAULT DATATABLE -->

                        </div>
                        <div class="tab-pane" id="tab-third">
                            <p>This is non libero bibendum, scelerisque arcu id, placerat nunc. Integer ullamcorper rutrum dui eget porta. Fusce enim dui, pulvinar a augue nec, dapibus hendrerit mauris. Praesent efficitur, elit non convallis faucibus, enim sapien suscipit mi, sit amet fringilla felis arcu id sem. Phasellus semper felis in odio convallis, et venenatis nisl posuere. Morbi non aliquet magna, a consectetur risus. Vivamus quis tellus eros. Nulla sagittis nisi sit amet orci consectetur laoreet.</p>

                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title">
                                        <button onclick="window.location.href='/role/create';"  class="btn btn-info">
                                            <i class="fa fa-plus"></i> Add new role
                                        </button>
                                    </h3>
                                    <ul class="panel-controls">
                                        <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                                        <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span></a></li>
                                        <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                                    </ul>
                                </div>
                                <div class="panel-body">
                                    <table class="table datatable">
                                        <thead>
                                        <tr>
                                            <th>Role</th>
                                            <th>Actions</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach ($all_roles as $role)
                                            <tr>
                                                <td>
                                                    <div>{{ $role->role }}</div>
                                                </td>
                                                <td>
                                                    @if($role->id == 1 || $role->id == 2)
                                                        <a href="" class="btn btn-primary" disabled><i class="fa fa-btn fa-edit"></i>Edit</a>
                                                        <button type="submit" id="delete-user-{{ $role->id }}" class="btn btn-danger" disabled>
                                                            <i class="fa fa-btn fa-trash"></i>Delete
                                                        </button>
                                                    @else
                                                        <a href="/role/{{ $role->id }}/edit" class="btn btn-primary"><i class="fa fa-btn fa-edit"></i>Edit</a>
                                                        <form action="{{ url('role/'.$role->id) }}" method="POST" style="display: inline-block">
                                                            {{ csrf_field() }}
                                                            {{ method_field('DELETE') }}
                                                            <button type="submit" id="delete-user-{{ $role->id }}" class="btn btn-danger">
                                                                <i class="fa fa-btn fa-trash"></i>Delete
                                                            </button>
                                                        </form>
                                                    @endif
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>

        </div>
    </div>

</div>
<!-- END PAGE CONTENT WRAPPER -->
@endsection

<!-- THIS PAGE PLUGINS -->
@section('page_plugins')

<script type='text/javascript' src="{{asset('system/js/plugins/icheck/icheck.min.js')}}"></script>
<script type="text/javascript" src="{{asset('system/js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js')}}"></script>

<script type="text/javascript" src="{{asset('system/js/plugins/bootstrap/bootstrap-datepicker.js')}}"></script>
<script type="text/javascript" src="{{asset('system/js/plugins/bootstrap/bootstrap-file-input.js')}}"></script>
<script type="text/javascript" src="{{asset('system/js/plugins/bootstrap/bootstrap-select.js')}}"></script>
<script type="text/javascript" src="{{asset('system/js/plugins/tagsinput/jquery.tagsinput.min.js')}}"></script>

<script type="text/javascript" src="{{asset('system/js/plugins/datatables/jquery.dataTables.min.js')}}"></script>

@endsection
<!-- END THIS PAGE PLUGINS -->






