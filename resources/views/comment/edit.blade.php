@extends('system.system')

@section('breadcrumb')

<ul class="breadcrumb">
    <li><a href="#">Home</a></li>
    <li><a href="#">>Comment list</a></li>
    <li class="active">Comment Edit</li>
</ul>

@endsection

@section('content')
<!-- PAGE CONTENT WRAPPER -->
<div class="page-content-wrap">
    <div class="row">
        <div class="col-md-12">
            <form action="{{ url('comment',[$comment->id]) }}" method="POST" enctype="multipart/form-data" class="form-horizontal">
                {{ csrf_field() }}
                {{ method_field('PUT') }}
                <div class="panel panel-default">
                    {!! view('system/inputs.title',['name'=>'Edit Comment']) !!}
                    {!! view('comment.form',compact('comment')) !!}
                    {!! view('system/inputs.submit',['name'=>'Edit Comment','icon' =>'edit']) !!}
                </div>
            </form>
        </div>
    </div>
</div>
<!-- END PAGE CONTENT WRAPPER -->
@endsection