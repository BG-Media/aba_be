<div class="panel-body">
    {!! view('errors.validation') !!}
    {!! view('system.inputs.textarea',['name'=>'content','value' => isset($comment->content) ? $comment->content : '','label'=>'Content','id'=>'content','rows'=>'3','cols'=>'16']) !!}
</div>