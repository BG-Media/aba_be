@extends('system.system')

@section('breadcrumb')

    <ul class="breadcrumb">
        <li><a href="#">Home</a></li>
        <li><a href="#">>Comment list</a></li>
        <li class="active">Comment Create</li>
    </ul>

@endsection

@section('content')
    <!-- PAGE CONTENT WRAPPER -->
    <div class="page-content-wrap">
        <div class="row">
            <div class="col-md-12">
                <form action="{{ url('comment') }}" method="POST" enctype="multipart/form-data" class="form-horizontal">
                    {{ csrf_field() }}
                    <div class="panel panel-default">
                        {!! view('system/inputs.title',['name'=>'Edit Comment']) !!}
                        {!! view('comment.form',compact('comment')) !!}
                        {!! view('system/inputs.submit',['name'=>'Add Comment','icon' =>'plus']) !!}
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- END PAGE CONTENT WRAPPER -->
@endsection